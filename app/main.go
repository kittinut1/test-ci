package main

import (
	"botio-voucher/app/config"
	"botio-voucher/app/pageconfig"
	"botio-voucher/app/paymentgateway"
	"botio-voucher/app/transaction"

	"botio-voucher/app/fbapis"
	"botio-voucher/app/feedDataToConsumer"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/network/fasthttp_client"
	"botio-voucher/app/processVoucher"
	"botio-voucher/app/voucher"
	"context"
	"strings"
	"time"

	"botio-voucher/app/authService"

	"botio-voucher/app/storage"
	"botio-voucher/app/storage/mongo"
	"bytes"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/buaazp/fasthttprouter"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/valyala/fasthttp"
)

var (
	dbRedis   *storage.Redis
	dbMongo   *mongo.Mongo
	consumer  *kafka.Consumer
	consumers []*kafka.Consumer
	producer  *kafka.Producer
)

func init() {
	if config.AppEnvironment != "test" {
		dbRedis = getRedisConnection()
		dbMongo = getMongoConnection()
		// createTopic(config.KafkaTopics)
		// producer = createProducer()
		// consumers = createConsumers(config.NumberOfMessagingConsumers + config.NumberOfRetryConsumers + config.NumbetOfFeedConsumers)
	}
}

func main() {
	defer dbRedis.Close()
	defer dbMongo.Client.Disconnect(context.TODO())

	client := fasthttp_client.New()
	k := paymentgateway.New(client)
	fb := fbapis.New(client)
	as := authService.New(client)
	vouchers := voucher.New(dbMongo)
	pageconfigs := pageconfig.New(dbMongo)
	transaction := transaction.New(dbMongo)
	pv := processVoucher.New(vouchers, pageconfigs, transaction, as, fb, k, dbRedis)

	fpc := feedDataToConsumer.New(pv, consumers, producer)
	fpc.Listeners()

	router := fasthttprouter.New()
	router.NotFound = CORS(func(ctx *fasthttp.RequestCtx) {
		logger.Info("NotFound")
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		return
	})
	router.MethodNotAllowed = CORS(func(ctx *fasthttp.RequestCtx) {
		logger.Info("MethodNotAllowed")
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		return
	})
	router.OPTIONS("/:any", CORS(func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Set("Access-Control-Max-Age", "5000")
		ctx.Response.Header.Set("Content-Type", "text/plain charset=UTF-8")
		ctx.Response.Header.Set("Content-Length", "0")
		ctx.SetStatusCode(204)
		return
	}))
	// for kubernetes
	router.GET("/healthz", health)

	defaultpath := "/voucher/v1.1"

	// to check server status
	router.GET(defaultpath+"/status", healthCheckHandler)

	// Campaing Endpoints
	router.GET(defaultpath+"/pages/:page-id/campaigns/:campaign-id", basicAuth(fpc.PV.GetCampaign))     // GET Campaign
	router.GET(defaultpath+"/pages/:page-id/stat", basicAuth(fpc.PV.PublicList))                        // GET Statistic
	router.GET(defaultpath+"/pages/:page-id/campaigns", basicAuth(fpc.PV.ListCampaign))                 // GET List Campaign
	router.GET(defaultpath+"/pages/:page-id/campaigns/:campaign-id/json", basicAuth(fpc.PV.ExportData)) // GET Export json
	router.POST(defaultpath+"/pages/:page-id/campaigns", basicAuth(fpc.PV.Create))                      // POST Create Campaign
	router.POST(defaultpath+"/pages/:page-id/campaigns/:campaign-id", basicAuth(fpc.PV.EditCampaign))   // POST Edit Campaign

	// Page Config Campaigns
	router.GET(defaultpath+"/pages", basicAuth(fpc.PV.AllPageConfigs))                               // GET List all pages with their configs
	router.GET(defaultpath+"/pages/:page-id", basicAuth(fpc.PV.GetPageConfig))                       // GET Pageconfig
	router.POST(defaultpath+"/pages/:page-id", basicAuth(fpc.PV.Setting))                            // POST  Update pageconfig, 1) consent message 2) passwords
	router.POST(defaultpath+"/pages/:page-id/messagephotos", basicAuth(fpc.PV.UploadImageToMessage)) // POST upload photo

	router.GET(defaultpath+"/pages/:page-id/payment/ksher", basicAuth(fpc.PV.GetKsherInfo))
	router.POST(defaultpath+"/pages/:page-id/payment/ksher", basicAuth(fpc.PV.UploadPrivateKey))

	// facebook webhook hanlders
	router.POST(defaultpath+"/feed", fpc.PV.WebhookHandler)      // POST handle feed webhook
	router.POST(defaultpath+"/messaging", fpc.PV.WebhookHandler) // POST  handle message webhook

	// Redemption APIS
	router.POST(defaultpath+"/pages/:page-id/campaigns/:campaign-id/vouchers/:voucher-id", fpc.PV.RedeemVoucher) // POST redeem couponcdoe
	router.GET(defaultpath+"/pages/:page-id/campaigns/:campaign-id/vouchers/:voucher-id", fpc.PV.Info)    // GET get campaign info
	// Subscription APIS
	router.POST(defaultpath+"/subscriptions", basicAuth(fpc.PV.CreateSubscription)) // POST Create subscription
	router.POST(defaultpath+"/subscriptions/payment", fpc.PV.KsherNotification)     // POST payment notification from ksher
	// router.GET(defaultpath+"/susbcription/checkpayment", fpc.PV.CheckPayment)

	// ksher api

	srv := &fasthttp.Server{
		Handler: Recover(CORS(router.Handler)),
	}
	port := ":" + os.Getenv("PORT")

	go func() {
		if err := srv.ListenAndServe(port); err != nil {
			fmt.Println("ERROR : Voucher is listening on port =", port, ", error =", err)
		}
	}()

	fmt.Println("Voucher is ready to listen and serve on port =", port)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	switch <-sigChan {
	case os.Interrupt:
		fmt.Println("\n Got SIGINT...")
	case syscall.SIGTERM:
		fmt.Println("\n Got SIGTERM...")
	}
	fmt.Println("The service is shutting down...in 1 Minute")
	time.Sleep(10 * time.Second)
	os.Exit(0)
}

var (
	corsAllowHeaders     = "Content-Type,Bearer,content-type,Origin,Accept,Access-Control-Allow-Headers,Access-Control-Allow-Origin,Authorization,X-Requested-With"
	corsAllowMethods     = "HEAD,GET,POST,PUT,DELETE,OPTIONS"
	corsAllowOrigin      = "*"
	corsAllowCredentials = "true"
)

func CORS(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {

		ctx.Response.Header.Set("Access-Control-Allow-Credentials", corsAllowCredentials)
		ctx.Response.Header.Set("Access-Control-Allow-Headers", corsAllowHeaders)
		ctx.Response.Header.Set("Access-Control-Allow-Methods", corsAllowMethods)
		// ctx.Response.Header.Set("Access-Control-Allow-Origin", corsAllowOrigin)

		next(ctx)
		ctx.SetContentType("application/json")
	}
}
func restrictedAPI(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		h(ctx)
	})
}

func basicAuth(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		auth := ctx.Request.Header.Peek("Authorization")
		if bytes.HasPrefix(auth, config.BasicAuthPrefix) {
			pair := bytes.SplitN(auth, []byte(" "), 2)
			if len(pair) == 2 {
				// Delegate request to the given handle
				claims := model.MyCustomClaims{}

				token, err := jwt.ParseWithClaims(string(pair[1]), &claims, func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return []byte(config.TokenSecret), nil
				})
				if err != nil {
					logger.Error(err)
					ctx.Error("Unauthorized Access", fasthttp.StatusForbidden)
					return
				}
				if !token.Valid {
					logger.Error(err)
					ctx.Error("Unauthorized Access", fasthttp.StatusForbidden)
					return
				}
				ctx.Request.Header.Set("user-id", claims.UserID)
				// ctx.Request.Header.Set("access-token", claims.AccessToken)
				// ctx.Request.Header.Set("ServiceID", "21212")
				h(ctx)
				return
			}
		}
		ctx.Error("Unauthorized Access", fasthttp.StatusForbidden)
	})
}

func createProducer() *kafka.Producer {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": config.BootstrapServers,
		"sasl.mechanisms":   "PLAIN",
		"security.protocol": "SASL_SSL",
		"sasl.username":     config.CcloudAPIKey,
		"sasl.password":     config.CcloudAPISecret,
	})
	if err != nil {

		fmt.Printf("Failed to create producer: %s", err)
		os.Exit(1)
	}
	fmt.Println("Producer intialized")
	return producer
}

func createTopic(topics []string) {
	adminClient, err := kafka.NewAdminClient(&kafka.ConfigMap{
		"bootstrap.servers":       config.BootstrapServers,
		"broker.version.fallback": "0.10.0.0",
		"api.version.fallback.ms": 0,
		"sasl.mechanisms":         "PLAIN",
		"security.protocol":       "SASL_SSL",
		"sasl.username":           config.CcloudAPIKey,
		"sasl.password":           config.CcloudAPISecret,
	})

	if err != nil {
		fmt.Printf("Failed to create Admin client: %s\n", err)
		os.Exit(1)
	}
	defer adminClient.Close()

	// Contexts are used to abort or limit the amount of time
	// the Admin call blocks waiting for a result.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Create topics on cluster.
	// Set Admin options to wait for the operation to finish (or at most 60s)
	maxDuration, err := time.ParseDuration("60s")
	if err != nil {
		panic("time.ParseDuration(60s)")
	}
	topicSpecifications := []kafka.TopicSpecification{}
	for i := range topics {
		topicSpecifications = append(topicSpecifications, kafka.TopicSpecification{
			Topic:             topics[i],
			NumPartitions:     6,
			ReplicationFactor: 3,
		})
	}
	results, err := adminClient.CreateTopics(ctx,
		topicSpecifications,
		kafka.SetAdminOperationTimeout(maxDuration))

	if err != nil {
		fmt.Printf("Problem during the topic creation: %v\n", err)
		os.Exit(1)
	}
	// Check for specific topic errors
	for _, result := range results {
		if result.Error.Code() != kafka.ErrNoError &&
			result.Error.Code() != kafka.ErrTopicAlreadyExists {
			fmt.Printf("Topic creation failed for %s: %v",
				result.Topic, result.Error.String())
			os.Exit(1)
		}
	}

}

func createConsumers(num int) (kC []*kafka.Consumer) {
	for i := 0; i < num; i++ {
		kC = append(kC, createConsumer())
	}
	return
}

func createConsumer() *kafka.Consumer {
	gid := "voucher"
	if config.AppEnvironment != "prod" {
		gid = "voucher-dev"
	}
	logger.Info("Group ID: ", gid)
	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":  config.BootstrapServers,
		"sasl.mechanisms":    "PLAIN",
		"security.protocol":  "SASL_SSL",
		"sasl.username":      config.CcloudAPIKey,
		"sasl.password":      config.CcloudAPISecret,
		"session.timeout.ms": 6000,
		"group.id":           gid,
	})

	if err != nil {
		panic(fmt.Sprintf("Failed to create consumer: %s", err))
	}
	fmt.Println("Consumer intialized")
	return consumer
}

func getRedisConnection() *storage.Redis {
	redisURL := config.RedisHost + ":" + config.RedisPort
	if config.AppEnvironment == "prod" {

	}
	redis, err := storage.NewConn(redisURL, config.RedisPassword)
	if err != nil {
		fmt.Println("Connect to redis failed.")
		fmt.Println("The service is shutting down...")
		os.Exit(1)
	}
	fmt.Println("Connect to redis '" + redisURL + "' successfully.")
	return redis
}

func getMongoConnection() *mongo.Mongo {
	mongoURL := "mongodb://" + config.MongoHost + ":" + config.MongoPort
	if config.AppEnvironment == "prod" {
		logger.Info("************* Production **************")
		mongoURL = fmt.Sprintf("%s", strings.TrimSpace(config.MongoDBURL))
	}
	m, err := mongo.New(mongoURL, uint64(config.MaxConnectionPool))
	if err != nil {
		fmt.Println("Connect to mongodb  failed.", err)
		fmt.Println("The service is shutting down...")
		os.Exit(1)
	}
	fmt.Println("Connect to mongo '" + config.MongoHost + ":" + config.MongoPort + "' successfully.")
	return m
}

func health(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(fasthttp.StatusOK)
	return
}

func healthCheckHandler(ctx *fasthttp.RequestCtx) {

	err := dbMongo.Client.Ping(context.TODO(), nil)
	if err != nil {
		dbMongo = getMongoConnection()
		ctx.SetBodyString(`{"message": {"mongo":"mongodb is down" }`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	ctx.SuccessString("application/json", `{"status":{"message":"Voucher status OK","mongo":"fine","redis":"`+fmt.Sprintf("%v", dbRedis.Ping())+`"}}`)
}
