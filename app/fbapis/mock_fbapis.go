package fbapis

import (
	"botio-voucher/app/network"

	"github.com/valyala/fasthttp"
)

type MockFBAPI struct {
	Client network.Client
}

func NewMock(client network.Client) *MockFBAPI {
	return &MockFBAPI{
		client,
	}
}
func (f *MockFBAPI) BatchRequest(body interface{}, pageAccessToken string) (res network.Response) {
	return
}

func (f *MockFBAPI) GetPages2(userID, userToken string) (res network.Response) {

	return
}

func (f *MockFBAPI) GenericElement(title, imageUrl, subtitle, imageRedirectUrl string, buttons []map[string]interface{}) map[string]interface{} {
	return map[string]interface{}{
		"title":     title,
		"image_url": imageUrl,
		"subtitle":  subtitle,
		"default_action": map[string]interface{}{
			"type":                 "web_url",
			"url":                  imageRedirectUrl,
			"webview_height_ratio": "tall",
		},
		"buttons": buttons,
	}
}
func (f *MockFBAPI) GenericTemplate(el1, el2 []map[string]interface{}) map[string]interface{} {
	return map[string]interface{}{}
}
func (f *MockFBAPI) TextQuickReply(text string, quickreplies []map[string]interface{}) map[string]interface{} {
	return map[string]interface{}{}
}
func (f *MockFBAPI) Like(pageAccessToken string, objectID string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}
func (f *MockFBAPI) HideComment(pageAccessToken string, objectID string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}
func (f *MockFBAPI) CreatePost(pageID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["id"] = "132312413431"
	return
}

func (f *MockFBAPI) PublishPost(postID, pageAccessToken string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}
func (f *MockFBAPI) SubscribeWebhook(pageID, pageAccessToken string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}

func (f *MockFBAPI) UploadVideo(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response) {
	return
}

func (f *MockFBAPI) UpdatePost(postID, pageAccessToken string, message string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}
func (f *MockFBAPI) UpdatePost2(postID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}
func (f *MockFBAPI) GetImageFromPhotoID(pageAccessToken, photoid string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["images"] = []interface{}{
		0: map[string]interface{}{
			"image": map[string]interface{}{
				"height": 392,
				"src":    "URL",
				"width":  720,
			},
		},
	}
	return
}
func (f *MockFBAPI) GetPageAccessToken(pageID, userToken string) (res network.Response) {
	res.Response = map[string]interface{}{"access_token": "232123132wedfcsdfvfdcx"}
	return
}

func (f *MockFBAPI) Comment(postID, pageAccessToken, message string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}

func (f *MockFBAPI) ReplyToComment(commentID, pageAccessToken, message string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}

func (f *MockFBAPI) UploadPhoto(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["id"] = "132312413431"
	return
}

func (f *MockFBAPI) UploadFileFb(filename, pageID, pageAccessToken string) (map[string]interface{}, error) {

	return map[string]interface{}{
		"id": "edwdwd",
	}, nil
}

func (f *MockFBAPI) UploadPagePhotos(pageID, pageAccessToken, url string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["id"] = "132312413431"
	return
}

func (f *MockFBAPI) GetPhotos(postID, pageAccessToken string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["full_picture"] = "URL"
	return
}

func (f *MockFBAPI) SendPrivateMessage(commentID string, message map[string]interface{}, pageAccessToken string) (res network.Response) {
	body := map[string]interface{}{
		"recipient": map[string]interface{}{
			"comment_id": commentID,
		},
		"message": message,
	}
	return f.SendMessage(pageAccessToken, body)
}
func (f *MockFBAPI) DeletePost(pageAccessToken, postid string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["success"] = true
	return
}

func (f *MockFBAPI) SendMessage(pageAccessToken string, body interface{}) (res network.Response) {
	return
}

func (f *MockFBAPI) GetPostAttachments(pageAccessToken, postid string) (res network.Response) {
	res.Response = map[string]interface{}{}
	res.Response["data"] = []interface{}{
		0: map[string]interface{}{
			"description": "DESCRIPTION",
			"media": map[string]interface{}{
				"image": map[string]interface{}{
					"height": 392,
					"src":    "URL",
					"width":  720,
				},
			},
			"target": map[string]interface{}{
				"id":  postid,
				"url": "URL",
			},
		},
	}
	return
}

func (f *MockFBAPI) Text(text string) map[string]interface{} {
	return map[string]interface{}{"text": text}
}

func (f *MockFBAPI) PayloadButton(title, payload string) map[string]interface{} {
	return map[string]interface{}{
		"type":    "postback",
		"payload": payload,
		"title":   title,
	}
}
func (f *MockFBAPI) WebUrlButton(title, url string) map[string]interface{} {
	return map[string]interface{}{
		"title":                title,
		"type":                 "web_url",
		"url":                  url,
		"webview_height_ratio": "full",
	}
}

func (f *MockFBAPI) QuickReply(title, payload string) map[string]interface{} {
	return map[string]interface{}{
		"content_type": "text",
		"title":        title,
		"payload":      payload,
	}
}
func (f *MockFBAPI) TextButton(text string, buttons, quickreplies []map[string]interface{}) map[string]interface{} {
	res := map[string]interface{}{
		"attachment": map[string]interface{}{
			"type": "template",
			"payload": map[string]interface{}{
				"template_type": "button",
				"text":          text,
				"buttons":       buttons,
			},
		},
		"quick_replies": quickreplies,
	}
	if len(quickreplies) == 0 {
		delete(res, "quick_replies")
	}
	return res
}
