package fbapis

func (f *FbAPI) Text(text string) map[string]interface{} {
	return map[string]interface{}{"text": text}
}

func (f *FbAPI) PayloadButton(title, payload string) map[string]interface{} {
	return map[string]interface{}{
		"type":    "postback",
		"payload": payload,
		"title":   title,
	}
}
func (f *FbAPI) WebURLButton(title, url string) map[string]interface{} {
	return map[string]interface{}{
		"title":                title,
		"type":                 "web_url",
		"url":                  url,
		"webview_height_ratio": "full",
	}
}

func (f *FbAPI) QuickReply(title, payload string) map[string]interface{} {
	return map[string]interface{}{
		"content_type": "text",
		"title":        title,
		"payload":      payload,
	}
}
func (f *FbAPI) TextButton(text string, buttons, quickreplies []map[string]interface{}) map[string]interface{} {
	res := map[string]interface{}{
		"attachment": map[string]interface{}{
			"type": "template",
			"payload": map[string]interface{}{
				"template_type": "button",
				"text":          text,
				"buttons":       buttons,
			},
		},
		"quick_replies": quickreplies,
	}
	if len(quickreplies) == 0 {
		delete(res, "quick_replies")
	}
	return res
}

func (f *FbAPI) TextQuickReply(text string, quickreplies []map[string]interface{}) map[string]interface{} {
	res := map[string]interface{}{
		"text":          text,
		"quick_replies": quickreplies,
	}
	return res
}
