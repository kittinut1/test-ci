package fbapis

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/network"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	"github.com/valyala/fasthttp"
)

type ExternalAPI interface {
	TextButton(text string, buttons, quickreplies []map[string]interface{}) map[string]interface{}
	WebURLButton(title, url string) map[string]interface{}
	PayloadButton(title, payload string) map[string]interface{}
	TextQuickReply(text string, quickreplies []map[string]interface{}) map[string]interface{}
	CreatePost(pageID, pageAccessToken string, post map[string]interface{}) (res network.Response)
	PublishPost(postID, pageAccessToken string) (res network.Response)
	SubscribeWebhook(pageID, pageAccessToken string) (res network.Response)
	UploadVideo(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response)
	UpdatePost(postID, pageAccessToken string, message string) (res network.Response)
	UpdatePost2(postID, pageAccessToken string, post map[string]interface{}) (res network.Response)
	GetImageFromPhotoID(pageAccessToken, photoid string) (res network.Response)
	GetPageAccessToken(pageID, userToken string) (res network.Response)
	Comment(postID, pageAccessToken, message string) (res network.Response)
	ReplyToComment(commentID, pageAccessToken, message string) (res network.Response)
	UploadPhoto(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response)
	UploadFileFb(filename, pageID, pageAccessToken string) (map[string]interface{}, error)
	UploadPagePhotos(pageID, pageAccessToken, url string) (res network.Response)
	GetPhotos(postID, pageAccessToken string) (res network.Response)
	SendPrivateMessage(commentID, messageTag string, message map[string]interface{}, pageAccessToken string) (res network.Response)
	DeletePost(pageAccessToken, postid string) (res network.Response)
	SendMessage(pageAccessToken string, body interface{}) (res network.Response)
	GetPostAttachments(pageAccessToken, postid string) (res network.Response)
	Text(text string) map[string]interface{}
	Like(pageAccessToken string, objectID string) (res network.Response)
	HideComment(pageAccessToken string, objectID string) (res network.Response)
	QuickReply(title, payload string) map[string]interface{}
	GenericTemplate(el []map[string]interface{}, qr []map[string]interface{}) map[string]interface{}
	GenericElement(title, imageURL, subtitle, imageRedirectURL string, buttons []map[string]interface{}) map[string]interface{}
	BatchRequest(body interface{}, pageAccessToken string) network.Response
	GetPages2(userID, userToken string) (res network.Response)
	ImageButton(photoid []string, buttons []map[string]interface{}) map[string]interface{}
	AttachmentUploadAPI(header string, pageAccessToken string) (res network.Response)
	AttachmentUploadAPI2(req *fasthttp.Request, pageAccessToken string) (res network.Response)
	PageInfo(pageAccessToken string) (res network.Response)
}

type FbAPI struct {
	client network.Client
}

func (f *FbAPI) PageInfo(pageAccessToken string) (res network.Response) {
	url := config.FacebookAPIEndpoint + "me?fields=id,name&access_token=" + pageAccessToken
	res = f.client.Get(url)
	checkFBErrors(&res)
	return
}

func New(c network.Client) *FbAPI {
	return &FbAPI{
		client: c,
	}
}

func (f *FbAPI) AttachmentUploadAPI2(req *fasthttp.Request, pageAccessToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + "me/message_attachments?access_token=" + pageAccessToken
	res = f.client.Redirect(req, uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) AttachmentUploadAPI(path string, pageAccessToken string) (res network.Response) {
	var err error
	url := config.FacebookAPIEndpoint + "me/message_attachments?access_token=" + pageAccessToken
	values := map[string]io.Reader{
		"message": strings.NewReader("{\"attachment\":{\"type\":\"file\", \"payload\":{\"is_reusable\":true}}}"),
	}
	values["filedata"], err = mustOpen(path) // lets assume its this file
	if err != nil {
		res.Err = err
		return
	}
	b := new(bytes.Buffer)
	w := multipart.NewWriter(b)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}

		// Add an image file
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				res.Err = err
				return
			}
		} else {
			// Add other fields

			if fw, err = w.CreateFormField(key); err != nil {
				res.Err = err
				return
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			res.Err = err
			return
		}

	}
	err = w.Close()
	if err != nil {
		res.Err = err
		return
	}
	client := http.Client{}

	req, err := http.NewRequest("POST", url, b)

	if err != nil {
		res.Err = err
		return
	}
	req.Header.Set("Content-Type", w.FormDataContentType())

	response, err := client.Do(req)
	if err != nil {
		res.Err = err
		return
	}

	// Check the response
	body, _ := ioutil.ReadAll(response.Body)
	response.Body.Close()
	fmt.Println(string(body))
	err = json.Unmarshal(body, &res.Response)
	if response.StatusCode != http.StatusOK {
		res.Err = fmt.Errorf("Response code :%v  , error: %s", response.StatusCode, string(body))
		return
	}
	logger.Info("HAHAH RES ", res)
	return

}
func mustOpen(f string) (*os.File, error) {
	return os.Open(f)
}

func (f *FbAPI) ImageButton(photoid []string, buttons []map[string]interface{}) map[string]interface{} {
	el := []map[string]interface{}{}
	for i := range photoid {
		el = append(el, map[string]interface{}{
			"media_type": "image",
			"url":        "https://www.facebook.com/photo.php?fbid=" + photoid[i],
		})
		if len(buttons) > 0 {
			el[i]["buttons"] = buttons
		}
	}
	return map[string]interface{}{
		"attachment": map[string]interface{}{
			"type": "template",
			"payload": map[string]interface{}{
				"template_type": "media",
				"elements":      el,
			},
		},
	}
}

func (f *FbAPI) BatchRequest(body interface{}, pageAccessToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + "me?include_headers=false&access_token=" + pageAccessToken
	res = f.client.Post(body, uri)
	// checkFBErrors(&res)
	return
}

func (f *FbAPI) GetPages2(userID, userToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + "me/accounts?fields=id,name,access_token,picture.type(large)&access_token=" + userToken
	res = f.client.Get(uri)
	fmt.Println(res)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) GenericElement(title, imageUrl, subtitle, imageactionurl string, buttons []map[string]interface{}) (res map[string]interface{}) {
	res = map[string]interface{}{
		"title":     title,
		"image_url": imageUrl,
		"subtitle":  subtitle,
		"default_action": map[string]interface{}{
			"type":                 "web_url",
			"url":                  imageactionurl,
			"webview_height_ratio": "tall",
		},
	}
	if len(buttons) > 0 {
		res["buttons"] = buttons
	}
	return
}
func (f *FbAPI) GenericTemplate(el []map[string]interface{}, qr []map[string]interface{}) (res map[string]interface{}) {

	res = map[string]interface{}{
		"attachment": map[string]interface{}{
			"type": "template",
			"payload": map[string]interface{}{
				"template_type":      "generic",
				"image_aspect_ratio": "square",
				"elements":           el,
			},
		},
	}
	if len(qr) > 0 {
		res["quick_replies"] = qr
	}
	return
}

func (f *FbAPI) HideComment(pageAccessToken string, objectID string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + objectID + "?access_token=" + pageAccessToken
	params := map[string]interface{}{
		"is_hidden": true,
	}
	res = f.client.Post(params, uri)

	checkFBErrors(&res)

	return
}

func (f *FbAPI) Like(pageAccessToken string, objectID string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + objectID + "/likes?access_token=" + pageAccessToken
	res = f.client.Post(nil, uri)

	checkFBErrors(&res)

	return
}

func (f *FbAPI) CreatePost(pageID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	uri := config.FacebookAPIEndpoint + pageID + "/feed?access_token=" + pageAccessToken
	res = f.client.Post(post, uri)

	checkFBErrors(&res)
	return
}

func (f *FbAPI) PublishPost(postID, pageAccessToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postID + "?is_published=true&access_token=" + pageAccessToken
	res = f.client.Post(nil, uri)

	checkFBErrors(&res)
	return
}
func (f *FbAPI) SubscribeWebhook(pageID, pageAccessToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + pageID + "/subscribed_apps?access_token=" + pageAccessToken + "&subscribed_fields=" + "feed,messages,messaging_postbacks,message_deliveries,message_reads,message_deliveries,messaging_referrals"
	res = f.client.Post(nil, uri)

	checkFBErrors(&res)
	return
}

func (f *FbAPI) UploadVideo(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response) {
	uri := config.FacebookVideoAPIEndpoint + pageID + "/videos?access_token=" + pageAccessToken
	res = f.client.Redirect(req, uri)
	checkFBErrors(&res)

	return
}

func (f *FbAPI) UpdatePost(postID, pageAccessToken string, message string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postID + "?access_token=" + pageAccessToken
	data := map[string]interface{}{
		"message": message,
	}
	res = f.client.PostJSON(data, uri)

	checkFBErrors(&res)
	return
}
func (f *FbAPI) UpdatePost2(postID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postID + "?access_token=" + pageAccessToken
	logger.Info(post)
	res = f.client.PostJSON(post, uri)

	checkFBErrors(&res)
	return
}
func (f *FbAPI) GetImageFromPhotoID(pageAccessToken, photoid string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + photoid + "?fields=images&access_token=" + pageAccessToken
	res = f.client.Get(uri)
	checkFBErrors(&res)
	if val, ok := res.Response["images"].([]interface{}); ok {
		if val2, ok := val[0].(map[string]interface{}); ok {
			if src, ok := val2["source"].(string); ok {
				res.Response = map[string]interface{}{
					"src": src,
				}
			}
		}
	}
	return
}
func (f *FbAPI) GetPageAccessToken(pageID, userToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + pageID + "?fields=name,access_token&access_token=" + userToken
	res = f.client.Get(uri)
	checkFBErrors(&res)

	return
}

func (f *FbAPI) Comment(postID, pageAccessToken, message string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postID + "/comments?access_token=" + pageAccessToken
	data := map[string]interface{}{
		"message": message,
	}
	res = f.client.PostJSON(data, uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) ReplyToComment(commentID, pageAccessToken, message string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + commentID + "/comments?access_token=" + pageAccessToken
	data := map[string]interface{}{
		"message": message,
	}
	res = f.client.PostJSON(data, uri)
	checkFBErrors(&res)

	return
}

func (f *FbAPI) UploadPhoto(pageID, pageAccessToken string, req *fasthttp.Request) (res network.Response) {
	uri := config.FacebookAPIEndpoint + pageID + "/photos?access_token=" + pageAccessToken
	res = f.client.Redirect(req, uri)
	checkFBErrors(&res)

	return
}

func (f *FbAPI) UploadFileFb(filename, pageID, pageAccessToken string) (map[string]interface{}, error) {
	var result map[string]interface{}
	file, err := os.Open(filename)
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}
	defer file.Close()

	var requestBody bytes.Buffer

	w := multipart.NewWriter(&requestBody)

	fw, err := w.CreateFormFile("source", "abc.jpg")
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}

	few, err := w.CreateFormField("published")
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}
	_, err = few.Write([]byte("false"))
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}
	w.Close()

	uri := config.FacebookAPIEndpoint + pageID + "/photos?access_token=" + pageAccessToken
	req, err := http.NewRequest("POST", uri, &requestBody)
	if err != nil {
		return result, fmt.Errorf("Cannot Read file")
	}

	req.Header.Set("Content-Type", w.FormDataContentType())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Info(err)
		return result, fmt.Errorf("fb Api Error")
	}

	json.NewDecoder(resp.Body).Decode(&result)

	if resp.StatusCode != 200 {
		res := network.Response{
			Response: result,
			Err:      fmt.Errorf("RESponse Status Code: %v", resp.StatusCode),
		}
		checkFBErrors(&res)
		result = res.Response
	}

	return result, err
}

func (f *FbAPI) UploadPagePhotos(pageID, pageAccessToken, url string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + pageID + "/photos?access_token=" + pageAccessToken
	body := map[string]interface{}{"published": false, "url": url}
	res = f.client.PostJSON(body, uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) GetPhotos(postID, pageAccessToken string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postID + "?fields=full_picture,picture&access_token=" + pageAccessToken
	res = f.client.Get(uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) SendPrivateMessage(commentID, messageTag string, message map[string]interface{}, pageAccessToken string) (res network.Response) {
	body := map[string]interface{}{
		"recipient": map[string]interface{}{
			"comment_id": commentID,
		},
		"message": message,
	}
	return f.SendMessage(pageAccessToken, body)
}

func (f *FbAPI) DeletePost(pageAccessToken, postid string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postid + "?access_token=" + pageAccessToken
	res = f.client.Delete(uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) SendMessage(pageAccessToken string, body interface{}) (res network.Response) {
	uri := config.FacebookAPIEndpoint + "me/messages?access_token=" + pageAccessToken
	res = f.client.Post(body, uri)
	checkFBErrors(&res)
	return
}

func (f *FbAPI) GetPostAttachments(pageAccessToken, postid string) (res network.Response) {
	uri := config.FacebookAPIEndpoint + postid + "/attachments?access_token=" + pageAccessToken
	res = f.client.Get(uri)
	checkFBErrors(&res)

	return
}

func checkFBErrors(res *network.Response) {

}

type FbError struct {
}

// {"error":{"message":"An active access token must be used to query information about the current user.","type":"OAuthException","code":2500,"fbtrace_id":"AbdmlZ1vKuvlzaCQTBKZc7l"}}
