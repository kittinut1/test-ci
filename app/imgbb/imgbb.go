package imgbb

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/network"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

type Imgbb struct {
	client http.Client
}

func New() *Imgbb {
	return &Imgbb{
		client: http.Client{},
	}
}

func (i *Imgbb) Upload(path string) (res network.Response) {
	var err error
	url := "https://api.imgbb.com/1/upload?key=" + config.ImgbbAPIKey
	values := map[string]io.Reader{}
	values["image"], err = mustOpen(path) // lets assume its this file
	if err != nil {
		res.Err = err
		return
	}
	b := new(bytes.Buffer)
	w := multipart.NewWriter(b)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}

		// Add an image file
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				res.Err = err
				return
			}
		} else {
			// Add other fields

			if fw, err = w.CreateFormField(key); err != nil {
				res.Err = err
				return
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			res.Err = err
			return
		}

	}
	err = w.Close()
	if err != nil {
		res.Err = err
		return
	}

	req, err := http.NewRequest("POST", url, b)

	if err != nil {
		res.Err = err
		return
	}
	req.Header.Set("Content-Type", w.FormDataContentType())

	response, err := i.client.Do(req)
	if err != nil {
		res.Err = err
		return
	}

	// Check the response

	body, _ := ioutil.ReadAll(response.Body)
	response.Body.Close()
	fmt.Println(string(body))
	img := new(ImgResponse)
	json.Unmarshal(body, img)
	if response.StatusCode != http.StatusOK {
		res.Err = fmt.Errorf("Response code :%v  , error: %s", response.StatusCode, string(body))
		return
	}
	res.Response = map[string]interface{}{
		"photo_id": img.Data.Medium.Url,
	}
	if img.Data.Medium.Url == "" {
		res.Response["photo_id"] = img.Data.Url

	}
	logger.Info("HAHAH RES ", res)
	return

}

type ImgResponse struct {
	Data ImgData `json:"data"`
}
type ImgData struct {
	ID     string    `json:"id"`
	Time   string    `json:"time"`
	Url    string    `json:"url"`
	Medium ImgMedium `json:"medium"`
}
type ImgMedium struct {
	Url string `json:"url"`
}

func mustOpen(f string) (*os.File, error) {
	return os.Open(f)
}
