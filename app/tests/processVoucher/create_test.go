package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func TestCreate(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	pageConfig := new(pageConfigMock)
	authService := new(authServiceMock)
	fbMock := new(FBMock)
	voucherMock := new(voucherMock)

	pv := processVoucher.ProcessVoucher{
		PageConfig:  pageConfig,
		AuthService: authService.MakeAuth(),
		FB:          fbMock,
		Voucher:     voucherMock,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("page-id", "1234")
		switch string(ctx.Path()) {
		case "/create":
			pv.Create(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	input := []map[string]interface{}{
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 10, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
		map[string]interface{}{"keys": []string{"#cat", "#bat"}, "title": "title", "message": "message", "content": "content", "voucher_src": "embedded", "voucher_type": "web", "how_to_use": "", "total_vouchers": 3, "voucher_sell_price": 0, "photos": []string{"photos"}, "voucher_expired_message": "expiredmessage", "inbox_message": "inbox message", "second_reply_comment": "second_reply_comment", "comment_inbox_message": "comment_inbox_message", "reply_comment_message": "reply coment msg", "language": "th", "draft": false, "start_time": time.Now().Add(1 * time.Minute).Unix(), "coupon_expire_time": time.Now().Add(24 * time.Hour).Unix(), "end_time": time.Now().Add(30 * time.Minute).Unix(), "coupon_list": []string{"sadas239231", "sdsd213e12", "ascas2143141"}},
	}

	pageConfigInput := []string{
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`, time.Now().Add(1*time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`, time.Now().Add(-1*time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`, time.Now().Add(time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 10}`, time.Now().Add(1*time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 9}`, time.Now().Add(1*time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`, time.Now().Add(1*time.Minute).Unix()),
		fmt.Sprintf(`{"user_id": "1234", "page_id": "1234", "consent_message": "", "subscription_end": %d, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`, time.Now().Add(1*time.Minute).Unix()),
	}

	expected := []string{
		`{"code":1010,"data":{"id":"1234"},"message":"Voucher Creation Successful","success":true}`,
		`{"code": 1012, "message": "Create Voucher Failed", "error":"subscription expired", "success": false }`,
		`{"code":1010,"data":{"id":"1234"},"message":"Voucher Creation Successful","success":true}`,
		`{"code": 1013, "message": "Create Voucher Failed", "error":"All Vouchers are used. Please buy."0 ", "success": false }`,
		`{"code": 1014, "message": "Create Voucher Failed", "error":"Maximum vouchers 1 ", "success": false }`,
		`{"code": 1015, "message": "Create Voucher Failed", "error":"please setup payment", "success": false }`,
		`{"code": 1015, "message": "Create Voucher Failed", "error":"please setup consent message", "success": false }`,
	}

	for i := 0; i < 7; i++ {
		fmt.Println("Create, TestCase: ", i+1)

		pageConfigData = pageConfigInput[i]

		reqByte, _ := json.Marshal(input[i])
		reqReader := bytes.NewReader(reqByte)
		req, _ := http.NewRequest("POST", "http://localhost:8084/create", reqReader)
		req.Header.Set("user-id", "1234")

		res, _ := c.Do(req)
		data, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		assert.Equal(t, expected[i], string(data), fmt.Sprintf("Create, Case#%d: Fail", i+1))
	}
}
