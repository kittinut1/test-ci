package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"botio-voucher/app/voucher"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"go.mongodb.org/mongo-driver/bson"
)

type voucherResendMock struct {
	voucher.Voucherer
}

var voucherResendData string

func (m *voucherResendMock) GetOne(query, selectedField bson.M, result interface{}) error {
	_ = json.Unmarshal([]byte(voucherResendData), &result)
	return nil
}

func (m *voucherResendMock) AtomicUpdate(query, update, selectedfields bson.M, result interface{}) error {
	return nil
}

func TestResendToken(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	voucherMock := new(voucherResendMock)
	pv := processVoucher.ProcessVoucher{
		Voucher: voucherMock,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("page-id", "1234")
		switch string(ctx.Path()) {
		case "/resend":
			pv.ResendToken(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	var tests = []struct {
		input    string
		expected string
	}{
		// #1 Test campaign has survey
		{
			`{"survey": [{"question": "q1","keyword": "k1"}]}`,
			`{"code":"3103","message":"survey campaign","success":false}`,
		},
		{
			`{"survey": []}`,
			`{"code":"3104","message":"processing history","success":true}`,
		},
	}

	req, _ := http.NewRequest("POST", "http://localhost:8084/resend", nil)
	req.Header.Set("voucher-id", "1234")

	for index, tt := range tests {
		voucherResendData = tt.input
		res, _ := c.Do(req)
		actual, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		assert.Equal(t, tt.expected, string(actual), fmt.Sprintf("Case#%d: Fail", index))
	}
}
