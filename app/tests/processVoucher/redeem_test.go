package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func TestRedeem(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	pageconfig := new(pageConfigMock)
	voucherMock := new(voucherMock)
	fbMock := new(FBMock)

	pv := processVoucher.ProcessVoucher{
		PageConfig: pageconfig,
		Voucher:    voucherMock,
		FB:         fbMock,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("page-id", "1234")
		switch string(ctx.Path()) {
		case "/redeem":
			pv.RedeemVoucher(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	input := []map[string]interface{}{
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
		map[string]interface{}{"id": "1234", "uid": "1234", "code": "code1234"},
	}

	pageConfigInput := []string{
		`{"redemption_password": {"code1234": "x"}}`,
		`{"redemption_password": {"code1234": "x"}}`,
		`{"redemption_password": {"code1234": "x"}}`,
		`{"redemption_password": {"code1234": "x"}}`,
		`{"redemption_password": {"code1234": "x"}}`,
		`{"redemption_password": {"code12345": "x"}}`,
		`{"redemption_password": {"code12345": "x"}}`,
	}

	voucherInput := []string{
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},	"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(-1*time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": true}},"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(-1*time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(1*time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(-1*time.Minute).Unix(), time.Now().Add(-5*time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},	"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(-1*time.Minute).Unix()),
		fmt.Sprintf(`{"history": {"1234": {"redeemed": false}},	"voucher_expired_on": %d,"page_id": "1234","voucher_start_on": %d}`, time.Now().Add(30*time.Minute).Unix(), time.Now().Add(-1*time.Minute).Unix()),
	}

	expected := []string{
		`{"code":8005,"message":"Redeem Voucher Successful","data":{"code": ""},"success":true}`,
		`{"code":8002,"message":"Voucher already redeemed", "data":{"code": ""},"success":true}`,
		`{"code":9001,"message":"Redemption is not started yet","success":false}`,
		`{"code":9001,"message":"Redemption is not started yet","success":false}`,
		`{"code":8003,"message":"Voucher Expired","success":false}`,
		`{"code":9001,"message":"password is wrong","success":false}`,
		`{"code":9001,"message":"password is wrong","success":false}`,
	}

	for i := 0; i < 7; i++ {
		fmt.Println("Redeem, TestCase: ", i+1)

		pageConfigData = pageConfigInput[i]
		voucherData = voucherInput[i]

		reqByte, _ := json.Marshal(input[i])
		reqReader := bytes.NewReader(reqByte)
		req, _ := http.NewRequest("POST", "http://localhost:8084/redeem", reqReader)
		req.Header.Set("user-id", "1234")

		res, _ := c.Do(req)
		data, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		assert.Equal(t, expected[i], string(data), fmt.Sprintf("Redeem, Case#%d: Fail", i+1))
	}
}
