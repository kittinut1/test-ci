package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func TestSetting(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	pageConfig := new(pageConfigMock)

	pv := processVoucher.ProcessVoucher{
		PageConfig: pageConfig,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("page-id", "1234")
		switch string(ctx.Path()) {
		case "/pages":
			pv.Setting(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	cases := []map[string]interface{}{
		map[string]interface{}{},
		map[string]interface{}{"redeemed_password": map[string]interface{}{"pass1234": ""}},
		map[string]interface{}{"consent_message": "1wdasdx"},
		map[string]interface{}{"redeemed_password": map[string]interface{}{"pass1234": "haha"}},
		map[string]interface{}{"consent_message": "1wdasdx", "redeemed_password": map[string]interface{}{"pass1234": "haha"}},
	}
	expect := []string{
		`{\"code\": 8500, \"message\": \"params not found\", \"success\": false}`,
		`{\"code\": 8500, \"message\": \"label not found\", \"success\": false}`,
		`{\"code\": 8503, \"message\": \"Page config updated\", \"success\": true}`,
		`{\"code\": 8503, \"message\": \"Page config updated\", \"success\": true}`,
		`{\"code\": 8503, \"message\": \"Page config updated\", \"success\": true}`,
	}
	for i := range cases {
		fmt.Println("TestCase: ", i+1)
		reqByte, _ := json.Marshal(cases[i])
		reqReader := bytes.NewReader(reqByte)

		req, _ := http.NewRequest("POST", "http://localhost:8084/pages", reqReader)
		req.Header.Set("user-id", "1234")

		res, _ := c.Do(req)
		data, _ := ioutil.ReadAll(res.Body)

		assert.Equal(t, expect[i], string(data))
		res.Body.Close()
	}
}
