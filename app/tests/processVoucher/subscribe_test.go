package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func TestCreateSubscription(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	authMock := new(authServiceMock)
	fbMock := new(FBMock)
	pageConfigMock := new(pageConfigMock)
	transactionMock := new(transactionMock)
	paymentMock := new(paymentGatewayMock)

	pv := processVoucher.ProcessVoucher{
		AuthService:    authMock.MakeAuth(),
		FB:             fbMock,
		PageConfig:     pageConfigMock,
		Transaction:    transactionMock,
		PaymentGateway: paymentMock,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("voucher-id", "1234")
		switch string(ctx.Path()) {
		case "/subscription":
			pv.CreateSubscription(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	input := []map[string]interface{}{
		map[string]interface{}{"page_id": "1234", "subscription_type": "free", "duration_type": "month", "company": false},
		map[string]interface{}{"page_id": "1234", "subscription_type": "basic", "duration_type": "month", "company": false},
		map[string]interface{}{"page_id": "1234", "subscription_type": "free", "duration_type": "month", "company": false},
	}

	expected := []string{
		"{\"code\":4003,\"data\":\"1234\",\"message\":\"Create Subscription successful\",\"success\":true}",
		"{\"code\":4006,\"data\":{\"code_url\":\"\",\"id\":\"1234\",\"img_data\":\"\",\"mch_order_id\":\"0\"},\"message\":\"Create Subscription successful\",\"success\":true}",
		"{\"code\": 4008, \"message\": \"Create Subscription failed\", \"error\": \"free subscription is available only first time\", \"success\": false}"}

	pageConfigInput := []string{
		"",
		"",
		fmt.Sprintf(`{"page_id": "1234", "created_on": %d, "subscription_type": "free"}`, time.Now().Add(1*time.Minute).Unix()),
	}

	for i := 0; i < 3; i++ {
		fmt.Println("Subscription, TestCase: ", i+1)

		pageConfigData = pageConfigInput[i]

		reqByte, _ := json.Marshal(input[i])
		reqReader := bytes.NewReader(reqByte)
		req, _ := http.NewRequest("POST", "http://localhost:8084/subscription", reqReader)
		req.Header.Set("user-id", "1234")

		res, _ := c.Do(req)
		data, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		if i == 1 {
			m := map[string]interface{}{}
			_ = json.Unmarshal(data, &m)
			fmt.Println(m)
			tt := map[string]interface{}{
				"id":           "1234",
				"mch_order_id": "0",
				"img_data":     "",
				"code_url":     "",
			}
			m["data"] = tt

			data, _ = json.Marshal(m)
		}

		assert.Equal(t, expected[i], string(data), fmt.Sprintf("Subscription, Case#%d: Fail", i+1))
	}
}
