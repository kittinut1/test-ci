package processVoucher_test

// func TestEditPost(t *testing.T) {
// 	l := fasthttputil.NewInmemoryListener()
// 	defer l.Close()

// 	voucherMock := new(voucherMock)
// 	pv := processVoucher.ProcessVoucher{
// 		Voucher: voucherMock,
// 	}

// 	m := func(ctx *fasthttp.RequestCtx) {
// 		ctx.SetUserValue("campaign-id", "1234")
// 		switch string(ctx.Path()) {
// 		case "/edit":
// 			pv.EditCampaign(ctx)
// 		}
// 	}

// 	go func() {
// 		_ = fasthttp.Serve(l, m)
// 	}()

// 	c := http.Client{
// 		Transport: &http.Transport{
// 			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
// 				conn, _ := l.Dial()
// 				return conn, nil
// 			},
// 		},
// 	}

// 	a := map[string]interface{}{
// 		"end_time":              time.Now().Add(5 * time.Minute).Unix(),
// 		"voucher_start_on":      time.Now().Add(5 * time.Minute).Unix(),
// 		"distribution_start_on": time.Now().Add(5 * time.Minute).Unix(),
// 		"voucher_expired_on":    time.Now().Add(5 * time.Minute).Unix(),
// 		"keys":                  []string{"eat"},
// 	}
// 	reqByte, _ := json.Marshal(a)
// 	reqReader := bytes.NewReader(reqByte)

// 	req, _ := http.NewRequest("POST", "http://localhost:8084/edit", reqReader)
// 	req.Header.Set("voucher-id", "1234")
// 	res, _ := c.Do(req)
// 	actual, _ := ioutil.ReadAll(res.Body)

// 	assert.Equal(t, `{"code": 2303, "error":"Campign updated", "success": true}`, string(actual))
// 	defer res.Body.Close()
// }
