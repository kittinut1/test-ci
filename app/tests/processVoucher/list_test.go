package processVoucher_test

import (
	"botio-voucher/app/processVoucher"
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func TestListCampaign(t *testing.T) {
	l := fasthttputil.NewInmemoryListener()
	defer l.Close()

	pageConfig := new(pageConfigMock)
	// authService := new(authServiceMock)
	// fbMock := new(FBMock)
	voucherMock := new(voucherMock)

	pv := processVoucher.ProcessVoucher{
		PageConfig: pageConfig,
		Voucher:    voucherMock,
	}

	m := func(ctx *fasthttp.RequestCtx) {
		ctx.SetUserValue("page-id", "1234")
		switch string(ctx.Path()) {
		case "/list":
			pv.ListCampaign(ctx)
		}
	}

	go func() {
		_ = fasthttp.Serve(l, m)
	}()

	c := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	pageConfigInput := []string{
		`{"user_id": "1234", "page_id": "1234", "consent_message": "message", "subscription_end": 1585725546, "subscription_voucher_total": 10, "subscription_voucher_count": 3}`,
	}

	expected := []string{
		"{\"code\":2002,\"config\":{\"_id\":\"\",\"page_id\":\"1234\",\"redeemed_at\":null,\"consent_message\":\"message\",\"payments\":{\"ksher\":{\"app_id\":\"\",\"private_key\":\"\"}},\"campaign_total\":0,\"vouchers_total\":0,\"subscription_voucher_count\":3,\"subscription_voucher_total\":10,\"subscription_type\":\"\",\"subscription_start\":0,\"subscription_end\":1585725546},\"data\":[],\"success\":true}",
	}

	for i := 0; i < 1; i++ {
		fmt.Println("ListCampaign, TestCase: ", i+1)

		pageConfigData = pageConfigInput[i]

		req, _ := http.NewRequest("GET", "http://localhost:8084/list", nil)
		req.Header.Set("user-id", "1234")

		res, _ := c.Do(req)
		data, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		assert.Equal(t, expected[i], string(data), fmt.Sprintf("ListCampaign Case#%d: Fail", i+1))
	}
}
