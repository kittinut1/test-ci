package processVoucher_test

import (
	"botio-voucher/app/authService"
	"botio-voucher/app/fbapis"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/network"
	"botio-voucher/app/pageconfig"
	"botio-voucher/app/paymentgateway"
	"botio-voucher/app/transaction"
	"botio-voucher/app/voucher"
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
type pageConfigMock struct {
	pageconfig.PageConfiger
}

var pageConfigData string

func (m pageConfigMock) GetOne(query, selectedField bson.M, result interface{}) error {
	if len(pageConfigData) == 0 {
		return mongo.ErrNoDocuments
	}
	_ = json.Unmarshal([]byte(pageConfigData), &result)
	return nil
}

func (m pageConfigMock) AtomicUpdate(query, update, selectedfields bson.M, result interface{}, new bool) error {
	return nil
}

func (m pageConfigMock) Update(query, update bson.M) error {
	logger.Info(update)
	return nil
}
func (m pageConfigMock) Create(pc *model.PageConfig) (string, error) {
	return "1234", nil
}

type authServiceMock struct{}

func (m authServiceMock) MakeAuth() *authService.AuthService {
	return &authService.AuthService{}
}

func (m *authServiceMock) GetTokenFromAuthService(pageID string) network.Response {
	res := network.Response{Response: map[string]interface{}{"access_token": "1234"}, Err: nil}

	return res
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
type FBMock struct {
	fbapis.ExternalAPI
}

func (m *FBMock) SubscribeWebhook(pageID, pageAccessToken string) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"access_token": "1234"}, Err: nil}
	return
}
func (m *FBMock) CreatePost(ageID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"id": "1234"}, Err: nil}
	return
}
func (m *FBMock) UpdatePost2(postID, pageAccessToken string, post map[string]interface{}) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"success": true}, Err: nil}
	return
}
func (m *FBMock) PublishPost(postID, pageAccessToken string) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"success": true}, Err: nil}
	return
}
func (m *FBMock) Text(text string) map[string]interface{} {
	res := map[string]interface{}{}
	return res
}
func (m *FBMock) SendMessage(pageAccessToken string, body interface{}) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"id": "1234"}, Err: nil}
	return
}
func (m *FBMock) PageInfo(pageAccessToken string) (res network.Response) {
	res = network.Response{Response: map[string]interface{}{"name": "MyPage"}, Err: nil}
	return
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
type voucherMock struct {
	voucher.Voucherer
}

var voucherData string

func (m *voucherMock) Create(userID, pageID, postID, title, message, inboxmessage, replycomment, secondreplycomment, voucherexpiredmessage, commentinboxmessage, language, vouchersrc, vouchertype, howToUse, details, voucher_redeemed_message, early_distribution_message, couponcode_expired_message, voucher_run_out_of_message string, totalVouchers int, triggerKeys, bannedKeys, couponsList, photos []string, survey interface{}, voucherExpiredOn, startTime, endTime int64, voucherSellPrice float64, started, draft bool, vouchertemplate interface{}) (string, error) {
	return "1234", nil
}

func (m *voucherMock) GetOne(query, selectedField bson.M, result interface{}) error {
	_ = json.Unmarshal([]byte(voucherData), &result)
	return nil
}

func (m *voucherMock) Update(query, update bson.M) error {
	return nil
}

func (m *voucherMock) GetAll(query, selected bson.M, result interface{}, page, limit int64) error {
	_ = json.Unmarshal([]byte(voucherData), &result)
	return nil
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
type transactionMock struct {
	transaction.Transactioner
}

func (m *transactionMock) Create(value *model.Transaction) (string, error) {
	return "1234", nil
}

type paymentGatewayMock struct {
	paymentgateway.PaymentGatewayer
}

func (m *paymentGatewayMock) PaymentRequest(merchantOrderNo, channel, currency, product, appID, privatekey, data string, totalAmount float64) (map[string]string, error) {
	return map[string]string{
		"nonce_str":      "1234",
		"ksher_order_no": "1234",
	}, nil
}
