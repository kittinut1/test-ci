package transaction

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/storage/mongo"
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/bson"
)

type Transactioner interface {
	Create(value *model.Transaction) (string, error)
	Update(query, update bson.M) error
	GetOne(query, selectedfields bson.M, result interface{}) error
	AtomicUpdate(query, update, selectedfield bson.M, result interface{}, new bool) error
}

var DB = "BotioUjung"
var Collection = "VoucherTransaction"

type Transaction struct {
	mongo *mongo.Mongo
}

func New(dbMongo *mongo.Mongo) *Transaction {
	err := dbMongo.Client.Ping(context.TODO(), nil)
	if err != nil {
		fmt.Println("Mongo Error: ", err)
		os.Exit(1)
	}
	a := &Transaction{
		mongo: dbMongo,
	}
	err = a.mongo.IndexCollections(DB, Collection, model.TransactionKeys)
	if err != nil {
		logger.Error(err, "Indexing "+Collection+" Collection Failed")
	}
	return a
}

func (t *Transaction) Create(value *model.Transaction) (string, error) {
	return t.mongo.Insert(DB, Collection, value)
}

func (t *Transaction) Update(query, update bson.M) error {
	return t.mongo.Update(DB, Collection, query, update)
}

func (t *Transaction) AtomicUpdate(query, update, selectedfields bson.M, result interface{}, new bool) error {
	return t.mongo.FindAndModify(DB, Collection, query, update, selectedfields, result, new)
}

func (t *Transaction) GetOne(query, selectedfields bson.M, result interface{}) error {
	return t.mongo.FindOne(DB, Collection, query, selectedfields, result)
}
