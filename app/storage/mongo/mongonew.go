package mongo

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Mongoer interface {
	IndexCollections(database, collection string, keys []string) error
	Insert(database, collection string, docs interface{}) error
	FindOne(database, collection string, query interface{}, selectedField interface{}, result interface{}) (err error)
	Update(database, collection string, filter interface{}, update interface{}) error
	Upsert(database, collection string, filter interface{}, update interface{}) error
	FindAll(database, collection string, query interface{}, selectedField interface{}, result interface{}, sort interface{}, page, limit int64) (err error)
	FindAndModify(database, collection string, filter interface{}, update interface{}, selectedField interface{}, result interface{}, new bool) error
}

type Mongo struct {
	Client *mongo.Client
}

func New(url string, connectionpoolsize uint64) (*Mongo, error) {
	clientOptions := options.Client().ApplyURI(url)
	poolOptions := options.Client().SetMaxPoolSize(connectionpoolsize)
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions, poolOptions)
	if err != nil {
		return nil, err
	}
	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}
	return &Mongo{
		Client: client,
	}, err
}

func (m *Mongo) Insert(database, collection string, docs interface{}) (string, error) {
	result, err := m.Client.Database(database).Collection(collection).InsertOne(context.TODO(), docs)
	if err != nil {
		return "", err
	}
	id, _ := result.InsertedID.(primitive.ObjectID)
	return id.Hex(), err
}

func (m *Mongo) FindOne(database, collection string, query interface{}, selectedField interface{}, result interface{}) (err error) {
	return m.Client.Database(database).Collection(collection).FindOne(context.TODO(), query, options.FindOne().SetProjection(selectedField)).Decode(result)
}

func (m *Mongo) Update(database, collection string, filter interface{}, update interface{}) error {
	updateresult, err := m.Client.Database(database).Collection(collection).UpdateOne(context.TODO(), filter, update)
	fmt.Printf("Upadate Result: %v \n", updateresult)
	return err
}

func (m *Mongo) Upsert(database, collection string, filter interface{}, update interface{}) error {
	updateresult, err := m.Client.Database(database).Collection(collection).UpdateOne(context.TODO(), filter, update, options.Update().SetUpsert(true))
	if err == nil && updateresult.ModifiedCount == 0 && updateresult.UpsertedCount == 0 {
		return fmt.Errorf("update failed %+v %+v", err, updateresult)
	}
	return err
}

func (m *Mongo) FindAll(database, collection string, query interface{}, selectedField interface{}, result interface{}, sort interface{}, page, limit int64) (err error) {
	cursor, err := m.Client.Database(database).Collection(collection).Find(context.TODO(), query,
		options.Find().SetProjection(selectedField),
		options.Find().SetLimit(limit),
		options.Find().SetSkip(page*limit),
		options.Find().SetSort(sort))
	if err != nil {
		return err
	}

	return cursor.All(context.TODO(), result)
}

func (m *Mongo) IndexCollections(database, collection string, keys []string) error {
	if len(keys) == 0 {
		return fmt.Errorf("no index keys : %v", keys)
	}
	indexview := m.Client.Database(database).Collection(collection).Indexes()
	models := []mongo.IndexModel{}
	for i := range keys {
		models = append(models, mongo.IndexModel{
			Keys:    bson.M{keys[i]: 1},
			Options: options.Index().SetUnique(true),
		})
	}
	names, err := indexview.CreateMany(context.TODO(), models)
	fmt.Println(names)
	return err
}

func (m *Mongo) FindAndModify(database, collection string, filter interface{}, update interface{}, selectedField interface{}, result interface{}, new bool) error {
	// opt := options.Before
	// if new {
	// 	opt = options.After
	// }
	return m.Client.Database(database).Collection(collection).FindOneAndUpdate(context.TODO(), filter, update,
		options.FindOneAndUpdate().SetProjection(selectedField),
		options.FindOneAndUpdate().SetUpsert(false)).Decode(result)
}
