package storage

import (
	"botio-voucher/app/config"
	"fmt"
	"reflect"

	"github.com/go-redis/redis"
)

type Storager interface {
	Get(key string) (interface{}, error)
	Set(key string, value interface{}) error
	// HSet(key, field string, value interface{}) error
	// HGetAll(key string) (map[string]string, error)
	// LPush(key string, value ...interface{}) error
	// RPush(key string, value ...interface{}) error
	// LIndex(key string, index int64) (string, error)
	// RPop(key string) (interface{}, error)
	// LPop(key string) (interface{}, error)
	Ping() interface{}
	Del(key string) error
	// FlushAll() interface{}
}

type Redis struct {
	conn *redis.Client
}

func (r *Redis) Ping() interface{} {
	return r.conn.Ping()
}
func (r *Redis) FlushAll() interface{} {
	if config.AppEnvironment == "test" {
		return r.conn.FlushDB()
	}
	return nil
}

func NewConn(address, password string) (*Redis, error) {
	options := &redis.Options{
		Addr:     address,
		Password: password,
		DB:       0,
	}
	client := redis.NewClient(options)
	if client == nil {
		return nil, fmt.Errorf("Cannot connect Redis")
	}
	return &Redis{
		conn: client,
	}, nil
}

func (r *Redis) Get(key string) (interface{}, error) {
	result, err := r.conn.Get(key).Result()
	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return result, nil
}

func (r *Redis) Set(key string, value interface{}) error {
	return r.conn.Set(key, value, 0).Err()
}

func (r *Redis) HSet(key, field string, value interface{}) error {
	return r.conn.HSet(key, field, value).Err()
}

func (r *Redis) HGetAll(key string) (map[string]string, error) {
	result, err := r.conn.HGetAll(key).Result()
	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return result, nil
}

func (r *Redis) LPush(key string, value ...interface{}) error {
	params := []interface{}{}
	for _, v := range value {
		if reflect.TypeOf(v).Kind() == reflect.Slice {
			val := reflect.ValueOf(v)
			for i := 0; i < val.Len(); i++ {
				params = append(params, val.Index(i).Interface())
			}
			continue
		}
		params = append(params, v)
	}

	if len(params) == 0 {
		return nil
	}
	return r.conn.LPush(key, params...).Err()
}

func (r *Redis) RPush(key string, value ...interface{}) error {
	return r.conn.RPush(key, value...).Err()
}

func (r *Redis) LIndex(key string, index int64) (string, error) {
	return r.conn.LIndex(key, index).Result()
}

func (r *Redis) RPop(key string) (string, error) {
	value, err := r.conn.RPop(key).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return value, nil
}

func (r *Redis) LPop(key string) (string, error) {
	value, err := r.conn.RPop(key).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return value, nil
}

func (r *Redis) Del(key string) error {
	return r.conn.Del(key).Err()
}

func (r *Redis) Close() {
	if r.conn != nil {
		r.conn.Close()
	}
}
