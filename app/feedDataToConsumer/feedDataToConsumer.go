package feedDataToConsumer

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/processVoucher"
	"log"
	"time"

	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func New(pv *processVoucher.ProcessVoucher, c []*kafka.Consumer, p *kafka.Producer) *FeedDataToConsumer {
	fpc := &FeedDataToConsumer{
		PV:        pv,
		Consumer:  c,
		Producer:  p,
		semaphore: make(chan struct{}, config.Concurrency),
		result:    make(chan int),
	}
	go func() {
		for range fpc.result {
		}
	}()
	return fpc
}

type FeedDataToConsumer struct {
	PV        *processVoucher.ProcessVoucher
	Consumer  []*kafka.Consumer
	Producer  *kafka.Producer
	semaphore chan struct{}
	result    chan int
}

func (fpc *FeedDataToConsumer) Listeners() {
	messaging := config.NumberOfMessagingConsumers
	retry := config.NumberOfRetryConsumers
	feed := config.NumbetOfFeedConsumers
	for i := 0; i < len(fpc.Consumer); i++ {
		if messaging > 0 {

			go fpc.listenMessages(fpc.Consumer[i], "messaging", 100*time.Millisecond)
			messaging = messaging - 1
			continue
		}
		if retry > 0 {
			go fpc.listenMessages(fpc.Consumer[i], "retry_messaging", 1*time.Second)
			retry = retry - 1
			continue
		}
		if feed > 0 {
			go fpc.listenMessages(fpc.Consumer[i], "feed", 100*time.Millisecond)
			feed = feed - 1
			continue
		}
	}
}

func (fpc *FeedDataToConsumer) listenMessages(c *kafka.Consumer, topic string, wait time.Duration) {
	err := c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		logger.Error(err)
		os.Exit(1)
	}
	defer c.Close()
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)
	run := true
	logger.Info("Listening Kafka: ", topic)
	for run == true {
		select {
		case sig := <-sigchan:
			c.Close()
			run = false
			fmt.Println("Caught signal : terminating\n", sig)
			time.Sleep(10 * time.Second)
			close(fpc.result)

		default:
			{
				message, err := c.ReadMessage(wait)
				if err == nil {
					fpc.dowork(message.Value, topic)
				}
			}
		}
	}
	log.Println("Listening Kafka Stopped")
}

func (fpc *FeedDataToConsumer) dowork(value []byte, topic string) {
	fpc.semaphore <- struct{}{}
	go func() {
		status := 0
		msg := ""
		defer func() {
			<-fpc.semaphore
			fpc.result <- status
		}()
		msg, status = fpc.PV.HandleComments2(value)
		logger.Info("Message: ", string(value), "Status: ", status, "Message: ", msg)
		if status == 2 && topic != "retry_messaging" {
			fpc.handleCommentStatus(status, value, "retry_messaging")
		}
	}()
}

func (fpc *FeedDataToConsumer) handleCommentStatus(status int, data []byte, topic string) {
	if status == 2 || status == 3 {
		if status == 3 {
			fpc.handleCommentStatus(status, data, topic)
			logger.Info("DB connection failedexiting")
			os.Exit(1)
		}
		fpc.Producer.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic,
				Partition: kafka.PartitionAny},
			Value: data}, nil)

		e := <-fpc.Producer.Events()

		message, ok := e.(*kafka.Message)
		if !ok {
			fmt.Printf("Kafka Message Error: %v", e)
			return
		}
		if message.TopicPartition.Error != nil {
			fmt.Printf("failed to deliver message: %v\n",
				message.TopicPartition)
			fpc.handleCommentStatus(status, data, topic)
		}
		fmt.Printf("delivered to topic %s [%d] at offset %v\n",
			*message.TopicPartition.Topic,
			message.TopicPartition.Partition,
			message.TopicPartition.Offset)
	}
}
