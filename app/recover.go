package main

import (
	"botio-voucher/app/logger"
	"fmt"
	"log"
	"os"
	"runtime"

	"github.com/valyala/fasthttp"
)

func Recover(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		defer func() {
			if err := recover(); err != nil {
				s := fmt.Sprintf("Recovered in API : %v \nAPI Panic Error: %v \n Runtime Stack : \n", string(ctx.Request.URI().Path()), err)
				storePanicData(string(ctx.Request.URI().Path()), s+"\n"+string(stack()))

				fmt.Println("Recovered in API : ", ctx.Request.URI())
				fmt.Println("API Panic Error: ", err)
				fmt.Println("Runtime Stack : ")
				printStack()
				ctx.SetStatusCode(fasthttp.StatusInternalServerError)
				ctx.SetBodyString(`{"messsge": "Internal Server Error", "status": false }`)
				return
			}
		}()
		h(ctx)
		return
	})
}
func storePanicData(apiPath, data string) {
	f, err := os.OpenFile("panic.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		logger.Info(err)
	}
	defer f.Close()
	logger := log.New(f, apiPath+": ", log.LstdFlags)
	logger.Println(data)
}

func printStack() {
	os.Stderr.Write(stack())
}
func stack() []byte {
	buf := make([]byte, 1024)
	for {
		n := runtime.Stack(buf, false)
		if n < len(buf) {
			return buf[:n]
		}
		buf = make([]byte, 2*len(buf))
	}
}
