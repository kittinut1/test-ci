package fasthttp_client

import (
	"encoding/json"
	"fmt"

	"botio-voucher/app/network"

	"github.com/valyala/fasthttp"
)

const (
	MethodGet  = "GET"
	MethodPost = "POST"
)

type FastHTTPClient struct {
	Client *fasthttp.Client
}

func New() *FastHTTPClient {
	client := &fasthttp.Client{MaxConnsPerHost: 2048}
	return &FastHTTPClient{client}
}

func (fhc *FastHTTPClient) Redirect(req *fasthttp.Request, url string) network.Response {
	req.SetRequestURI(url)
	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) PostForm(body []byte, url string) network.Response {
	req := getFormDataRequest(url, MethodPost)
	req.SetBody(body)
	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) PostFormData(body interface{}, url string) network.Response {
	req := getFormDataRequest(url, MethodPost)
	payloads, ok := body.(map[string]interface{})
	if !ok {
		return network.Response{Response: nil, Err: fmt.Errorf("Cannot cast payloads to []map[string]interface{}")}
	}

	var formBody string
	for key, value := range payloads {
		if formBody == "" {
			formBody = fmt.Sprintf("%s=%s", key, value)
		} else {
			formBody = fmt.Sprintf("%s&%s=%s", formBody, key, value)
		}
	}
	req.SetBodyString(formBody)

	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) PostFormDataBytes(body interface{}, url string) network.ResponseBytes {
	req := getFormDataRequest(url, MethodPost)
	payloads, ok := body.(map[string]interface{})
	if !ok {
		return network.ResponseBytes{Response: nil, Err: fmt.Errorf("Cannot cast payloads to []map[string]interface{}")}
	}

	var formBody string
	for key, value := range payloads {
		if formBody == "" {
			formBody = fmt.Sprintf("%s=%s", key, value)
		} else {
			formBody = fmt.Sprintf("%s&%s=%s", formBody, key, value)
		}
	}
	req.SetBodyString(formBody)

	resp := fasthttp.AcquireResponse()
	return doBytes(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) Post(body interface{}, url string) network.Response {
	var bodyMessage []byte
	bodyMessage, _ = json.Marshal(body)

	req := getJSONRequest(url, MethodPost)
	req.SetBody(bodyMessage)
	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) PostJSON(body interface{}, url string) network.Response {
	bodyMessage, err := json.Marshal(body)
	if err != nil {
		return network.Response{
			Err: err,
		}
	}
	req := getJSONRequest(url, MethodPost)
	req.SetBody(bodyMessage)
	resp := fasthttp.AcquireResponse()

	return do(req, resp, fhc.Client)
}

func (fhc *FastHTTPClient) Get(url string) network.Response {
	req := getJSONRequest(url, MethodGet)
	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func doBytes(req *fasthttp.Request, resp *fasthttp.Response, c *fasthttp.Client) (r network.ResponseBytes) {
	err := c.Do(req, resp)
	if err != nil {
		fmt.Printf("ERROR: %+v\n", err.Error())
		return network.ResponseBytes{Response: nil, Err: err}
	}
	fmt.Printf("Request: %+v\n", req)
	fmt.Printf("Response: %s\n", string(resp.Body()))
	if resp.StatusCode() != 200 {
		return network.ResponseBytes{Response: resp.Body(), Err: fmt.Errorf("Response status is %d", resp.StatusCode())}
	}
	return network.ResponseBytes{Response: resp.Body(), Err: nil}
}

func do(req *fasthttp.Request, resp *fasthttp.Response, c *fasthttp.Client) (r network.Response) {
	err := c.Do(req, resp)
	if err != nil {
		fmt.Printf("ERROR: %+v\n", err.Error())
		return network.Response{Response: nil, Err: err}
	}
	fmt.Printf("Request: %+v\n", req)
	fmt.Printf("Response: %s\n", string(resp.Body()))
	response := map[string]interface{}{}
	if resp.StatusCode() != 200 {
		err = json.Unmarshal(resp.Body(), &response)
		if err != nil {
			fmt.Printf("ERROR: %+v\n", err)
			return network.Response{Response: nil, Err: fmt.Errorf("%+v", err)}
		}
		return network.Response{Response: response, Err: fmt.Errorf("Response status is %d", resp.StatusCode())}
	}

	err = json.Unmarshal(resp.Body(), &response)
	if err != nil {
		fmt.Printf("ERROR: %+v\n", err)
		if err.Error() == "json: cannot unmarshal array into Go value of type map[string]interface {}" {
			arrayResponse := []map[string]interface{}{}
			err = json.Unmarshal(resp.Body(), &arrayResponse)
			if err != nil {
				fmt.Printf("ERROR: %+v\n", err)
				return network.Response{Response: nil, Err: fmt.Errorf("%+v", err)}
			}
			response["data"] = arrayResponse
			return network.Response{Response: response, Err: nil}
		}
		return network.Response{Response: nil, Err: fmt.Errorf("%+v", err)}
	}

	return network.Response{Response: response, Err: nil}
}

func (fhc *FastHTTPClient) Delete(url string) network.Response {
	req := fasthttp.AcquireRequest()
	req.SetRequestURI(url)
	req.Header.SetMethod("DELETE")
	resp := fasthttp.AcquireResponse()
	return do(req, resp, fhc.Client)
}

func getJSONRequest(url string, method string) (req *fasthttp.Request) {
	req = fasthttp.AcquireRequest()
	req.SetRequestURI(url)
	req.Header.SetMethod(method)
	req.Header.SetContentType("application/json")
	return
}

func getFormDataRequest(url string, method string) (req *fasthttp.Request) {
	req = fasthttp.AcquireRequest()
	req.SetRequestURI(url)
	req.Header.SetMethod(method)
	req.Header.SetContentType("application/x-www-form-urlencoded")
	return
}
