package network

import "github.com/valyala/fasthttp"

type Client interface {
	PostForm(body []byte, url string) Response
	PostFormData(body interface{}, url string) Response
	PostFormDataBytes(body interface{}, url string) ResponseBytes
	Post(body interface{}, url string) Response
	PostJSON(body interface{}, url string) Response
	Get(url string) Response
	Delete(url string) Response
	Redirect(req *fasthttp.Request, url string) Response
}

type Response struct {
	Response map[string]interface{}
	Err      error
}

type ResponseBytes struct {
	Response []byte
	Err      error
}
