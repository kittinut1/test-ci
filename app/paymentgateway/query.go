package paymentgateway

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/utils"
)

func (k *Ksher) PaymentQuery(appid, privatekey, merchantOrderNo, ksherOrderno, channel string) (map[string]string, error) {
	nonceStr := utils.StringWithCharset(32)
	str := "appid=" + config.KsherAppID + "channel=" + channel + "mch_order_no=" + merchantOrderNo + "nonce_str=" + nonceStr

	sign, err := utils.CreateSign(str, privatekey)
	if err != nil {
		return nil, err
	}

	body := map[string]interface{}{
		"appid":          appid,
		"channel":        channel,
		"ksher_order_no": ksherOrderno,
		"mch_order_no":   merchantOrderNo,
		"nonce_str":      nonceStr,
		"sign":           sign,
	}
	url := config.KsherAPI + "KsherPay/native_pay"
	res := k.client.PostFormDataBytes(body, url)
	logger.Info(res)
	return nil, nil
}

type QueryResponse struct {
	Code   int    `json:"code"`
	Sign   string `json:"sign"`
	Result string `json:"result"`

	// FAIL SUCCESS NOTPAY CLOSED PAYERROR REFUND
	Data QueryData `json:"data"`
}

type QueryData struct {
	AppID  string `json:"app_id"`
	Attach string `json:"attach"`
	// CashFee strin
}
