package paymentgateway

type PaymentGatewayer interface {
	PaymentRequest(merchantOrderNo, channel, currency, product, appID, privatekey, data string, totalAmount float64) (map[string]string, error)
	RecieveNotification(body []byte) (map[string]string, error)
	// PaymentQuery(appid, privatekey, merchantOrderNo, ksherOrderno, channel string) (map[string]string, error)
}
