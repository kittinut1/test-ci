package paymentgateway

import (
	"encoding/json"
	"fmt"
)

func (k *Ksher) RecieveNotification(body []byte) (map[string]string, error) {
	fmt.Println(string(body))
	kn := new(KsherNotification)
	err := json.Unmarshal(body, kn)
	if err != nil || kn.Code != 0 {
		return nil, err
	}
	return map[string]string{
		"sign":             kn.Sign,
		"result":           kn.Data.Result,
		"appid":            kn.Data.AppID,
		"mch_order_no":     kn.Data.MerchantOrderNo,
		"ksher_order_no":   kn.Data.KsherOrderNo,
		"order_no":         kn.Data.OrderNo,
		"open_id":          kn.Data.OpenID,
		"rate":             kn.Data.Rate,
		"channel_order_no": kn.Data.ChannelOrderNo,
	}, nil
}

type KsherNotification struct {
	Code       int           `json:"code"`
	Version    string        `json:"version"`
	StatusCode string        `json:"status_code"`
	Msg        string        `json:"msg"`
	Timestamp  string        `json:"time_stamp"`
	StatusMsg  string        `json:"status_msg"`
	Data       KsherNotiData `json:"data"`
	Sign       string        `json:"sign"`
}
type KsherNotiData struct {
	OpenID          string `json:"openid"`
	ChannelOrderNo  string `json:"channel_order_no"`
	OperatorID      string `json:"operator_id"`
	CashFeeType     string `json:"cash_fee_type"`
	KsherOrderNo    string `json:"ksher_order_no"`
	NonceStr        string `json:"nonce_str"`
	TimeEnd         string `json:"time_end"`
	FeeType         string `json:"fee_type"`
	Attach          string `json:"attach"`
	Rate            string `json:"rate"`
	Result          string `json:"result"`
	TotalFee        int    `json:"total_fee"`
	AppID           string `json:"appid"`
	OrderNo         string `json:"order_no"`
	Operation       string `json:"operation"`
	DeviceID        string `json:"device_id"`
	CashFee         int    `json:"cash_fee"`
	Channel         string `json:"channel"`
	MerchantOrderNo string `json:"mch_order_no"`
}
