package paymentgateway

import (
	"botio-voucher/app/config"
	"botio-voucher/app/network"
	"botio-voucher/app/utils"
	"encoding/json"
	"fmt"
	"strconv"
)

type Ksher struct {
	client network.Client
	signer map[string]utils.Signer
}

func New(c network.Client) *Ksher {
	return &Ksher{
		client: c,
		signer: map[string]utils.Signer{},
	}
}

func (k *Ksher) PaymentRequest(merchantOrderNo, channel, currency, product, appID, privatekey, data string, totalAmount float64) (map[string]string, error) {
	nonceStr := utils.StringWithCharset(32)
	str := "appid=" + appID + "channel=" + channel + "fee_type=" + currency + "mch_order_no=" + merchantOrderNo + "nonce_str=" + nonceStr + "total_fee=" + strconv.Itoa(int(totalAmount))
	sign, err := utils.CreateSign(str, privatekey)
	if err != nil {
		return nil, err
	}
	body := map[string]interface{}{
		"appid":        appID,
		"nonce_str":    nonceStr,
		"channel":      channel,
		"sign":         sign,
		"mch_order_no": merchantOrderNo,
		"total_fee":    strconv.Itoa(int(totalAmount)),
		"fee_type":     currency,
		"notify_url":   "https://api.botio.io/voucher/subscriptions/payment",
		"img_type":     "png",
		"product":      product,
		"attach":data,
		// "device_id": "",
	}
	url := config.KsherAPI + "KsherPay/native_pay"
	res := k.client.PostFormDataBytes(body, url)
	pr := new(PaymentResponse)
	err = json.Unmarshal(res.Response, pr)
	if err != nil {
		return nil, err
	}
	if res.Err != nil || pr.Code != 0 {
		return nil, fmt.Errorf(pr.Data.ErrMessage)
	}
	if pr.Data.NonceStr != nonceStr {
		return nil, fmt.Errorf("Wrong NonceStr recieved")
	}
	if pr.Data.Result != "SUCCESS" {
		return nil, fmt.Errorf("Ksher failed")
	}
	return map[string]string{
		"nonce_str":      pr.Data.NonceStr,
		"ksher_order_no": pr.Data.KsherOrderNo,
		"mch_order_no":   pr.Data.MerchantOrderNo,
		"result":         pr.Data.Result,
		"time_stamp":     pr.Timestamp,
		// "code_url":       pr.Data.CodeURL,
		"imgdat": pr.Data.ImgData,
	}, nil
}

type PaymentResponse struct {
	Code          int                 `json:"code"`
	Message       string              `json:"msg"`
	Version       string              `json:"version"`
	Timestamp     string              `json:"time_stamp"`
	StatusCode    string              `json:"status_code"`
	StatusMessage string              `json:"status_msg"`
	Sign          string              `json:"sign"`
	Data          PaymentResponseData `json:"data"`
}
type PaymentResponseData struct {
	AppID           string `json:"appid"`
	TradeType       string `json:"trade_type"`
	DeviceID        string `json:"device_id"`
	CodeURL         string `json:"code_url"`
	ImgData         string `json:"imgdat"`
	Rate            string `json:"rate"`
	KsherOrderNo    string `json:"ksher_order_no"`
	MerchantOrderNo string `json:"mch_order_no"`
	FeeType         string `json:"fee_type"`
	Result          string `json:"result"`
	NonceStr        string `json:"nonce_str"`
	ErrCode         string `json:"err_code"`
	ErrMessage      string `json:"err_msg"`
}
