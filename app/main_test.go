package main

// import (
// 	"botio-voucher/app/authService"
// 	"botio-voucher/app/config"
// 	_ "botio-voucher/app/config"
// 	"botio-voucher/app/fbapis"
// 	"botio-voucher/app/feedDataToConsumer"
// 	"botio-voucher/app/model"
// 	"botio-voucher/app/network/fasthttp_client"
// 	"botio-voucher/app/pageconfig"
// 	"botio-voucher/app/processVoucher"
// 	"botio-voucher/app/voucher"
// 	"context"
// 	"encoding/json"
// 	"fmt"
// 	"log"
// 	"math/rand"
// 	"os"
// 	"testing"
// 	"time"

// 	"github.com/valyala/fasthttp"
// 	"go.mongodb.org/mongo-driver/bson"
// )

// var mockpv = new(processVoucher.ProcessVoucher)
// var mockfpc = new(feedDataToConsumer.FeedDataToConsumer)
// var mockpageID = "1234567890"
// var mockuserID = "12312312414"
// var mockLanguage = "en"
// var part1 = []byte(`{"action":"add","page_id":"id_","name":"FromName_","post_id":"132312413431","comment_id":"CommentID_","psid":"`)
// var part2 = []byte(`", "message":"#bat", "timestamp":1578647892 }`)
// var totalv = 10000
// var randomize = true
// var stringhah = []string{}
// var result = map[string]int{
// 	"inputerr": 0,
// 	"pass":     0,
// 	"retry":    0,
// }

// type responseStruct struct {
// 	Code    int              `json:"code"`
// 	Data    []model.Campaign `json:"data"`
// 	Success bool             `json:"success"`
// }

// const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

// func StringWithCharset(length int, charset string) string {
// 	b := make([]byte, length)
// 	for i := range b {
// 		b[i] = charset[seededRand.Intn(len(charset))]
// 	}
// 	return string(b)
// }

// func String(length int) string {
// 	return StringWithCharset(length, charset)
// }
// func flushDB() {
// 	if config.AppEnvironment == "test" {
// 		var err error
// 		status := dbRedis.FlushAll()
// 		if status != "OK" {

// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("Vouchers").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("VoucherPageConfigs").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 	}
// }

// func createRandomString(n int) []string {
// 	arr := []string{}
// 	for i := 0; i < n; i++ {
// 		arr = append(arr, fmt.Sprintf("vucher_%v", i))
// 	}
// 	return arr
// }
// func TestMain(m *testing.M) {
// 	if config.AppEnvironment != "test" {
// 		os.Exit(1)
// 	}
// 	dbRedis = getRedisConnection()
// 	dbMongo = getMongoConnection()
// 	stringhah = createRandomString(totalv)

// 	client := fasthttp_client.New()
// 	mockfb := fbapis.NewMock(client)
// 	authService := &authService.AuthService{client, dbRedis}

// 	vouchers := voucher.New(dbMongo)
// 	pageconfigs := pageconfig.New(dbMongo)

// 	mockpv = processVoucher.New(vouchers, pageconfigs, authService, mockfb, dbRedis)
// 	mockfpc = feedDataToConsumer.New(mockpv, nil, nil)
// 	flushDB()
// 	t1 := time.Now()
// 	retCode := m.Run()
// 	fmt.Println("Elapsed", time.Since(t1))
// 	dbRedis.Close()
// 	if retCode != 0 {
// 		log.Println("\n********************************\n😡 😡 😡 😡 😡 😡 😡 😡 \n🔥🔥🔥🚫🚫🚫🚫🚫🚫Test Failed. Lol.🚫🚫🚫🚫🚫🚫🔥🔥🔥\n😢😢😢😢😢😢😢\n********************************")
// 	} else {
// 		log.Println("\n********************************\n 👏🎉 👏🎉 👏🎉 👏🎉\n😘😘😍😘😘😍 Test Passed.👌👌👌👌👌👌\n🏆🏆🏆🏆🏆🏆🏆\n********************************")
// 	}
// 	// dbMongo.Session.DB("BotioUjung").DropDatabase()
// 	dbMongo.Client.Disconnect(context.TODO())

// 	os.Exit(retCode)
// }

// func TestEmbeddedVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          3,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)

// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if rs.Code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 1010, found %+v", rs.Code)
// 	}
// 	if rs.Success == false {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected true, found %+v", rs.Success)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestSingleVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	resData := map[string]interface{}{}
// 	err = json.Unmarshal(resp.Body(), &resData)
// 	if err != nil {
// 		t.Errorf("TestVoucherCreation: Failed to read response body %+v", err)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	code, ok := resData["code"].(float64)
// 	if !ok {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param not found %+v", resData)

// 	}
// 	if code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 5010, found %+v", code)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestListVouchers(t *testing.T) {
// 	for i := 0; i < 1; i++ {
// 		paramsObject := map[string]interface{}{
// 			"keys":                    []string{"#cat", "#bat"},
// 			"title":                   "title",
// 			"message":                 "message",
// 			"content":                 "content",
// 			"voucher_src":             "embedded",
// 			"photos":                  []string{"photos"},
// 			"voucher_expired_message": "expiredmessage",
// 			"inbox_message":           "inbox message",
// 			"second_reply_comment":    "second_reply_comment",
// 			"comment_inbox_message":   "comment_inbox_message",
// 			"reply_comment_message":   "reply coment msg",
// 			"language":                mockLanguage,
// 			"draft":                   false,
// 			"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 			"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 			"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 			"total_vouchers":          3,
// 			"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 		}
// 		body, err := json.Marshal(paramsObject)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		req := fasthttp.AcquireRequest()
// 		req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 		req.Header.SetContentType("application/json")
// 		req.Header.Set("user-id", mockuserID)
// 		req.SetBody(body)

// 		var ctx fasthttp.RequestCtx
// 		ctx.Init(req, nil, nil)
// 		ctx.SetUserValue("page-id", mockpageID)

// 		mockpv.Create(&ctx)

// 		resp := ctx.Response

// 		if resp.StatusCode() != 200 {
// 			t.Errorf("TestListVouchers Failed ")
// 			return
// 		}
// 	}
// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.ListCampaign(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestListVoucher Failed ")
// 	}

// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if len(rs.Data) != 1 || rs.Success == false {
// 		t.Errorf("TestListVoucher Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()

// }

// func TestExportData(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}

// 	fmt.Println(string(resp.Body()))

// 	mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	mockpv.ListCampaign(&ctx)

// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))

// 	rs := new(responseStruct)
// 	err = json.Unmarshal(resp.Body(), rs)
// 	if err != nil {
// 		t.Errorf("TestExportData Failed %+v ", err)
// 	}
// 	if len(rs.Data) == 0 {
// 		t.Errorf("TestExportData Failed %+v ", "length is 0")
// 		return
// 	}
// 	voucherID := rs.Data[0].ID

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	ctx.SetUserValue("campaign-id", voucherID)
// 	fmt.Println(voucherID)

// 	mockpv.ExportData(&ctx)
// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }
// func BenchmarkHandleComments(b *testing.B) {
// 	flushDB()
// 	paramsObject := map[string]interface{}{
// 		"keys":                    []string{"#cat", "#bat"},
// 		"language":                mockLanguage,
// 		"title":                   "title",
// 		"message":                 "message",
// 		"additional_message":      "additional_message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"reply_comment_message":   "reply coment msg",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          totalv,
// 		"coupon_list":             stringhah,
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// 		}
// 	})
// 	// logger.Info(a)
// }

// func BenchmarkHandleCommentsSecondTime(b *testing.B) {
// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// 		}
// 	})
// }

// type responseStruct struct {
// 	Code    int              `json:"code"`
// 	Data    []model.Voucher `json:"data"`
// 	Success bool             `json:"success"`
// }

// const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

// func StringWithCharset(length int, charset string) string {
// 	b := make([]byte, length)
// 	for i := range b {
// 		b[i] = charset[seededRand.Intn(len(charset))]
// 	}
// 	return string(b)
// }

// func String(length int) string {
// 	return StringWithCharset(length, charset)
// }
// func flushDB() {
// 	if config.AppEnvironment == "test" {
// 		var err error
// 		status := dbRedis.FlushAll()
// 		if status != "OK" {

// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("Vouchers").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("VoucherPageConfigs").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 	}
// }

// func createRandomString(n int) []string {
// 	arr := []string{}
// 	for i := 0; i < n; i++ {
// 		arr = append(arr, fmt.Sprintf("vucher_%v", i))
// 	}
// 	return arr
// }
// func TestMain(m *testing.M) {
// 	if config.AppEnvironment != "test" {
// 		os.Exit(1)
// 	}
// 	dbRedis = getRedisConnection()
// 	dbMongo = getMongoConnection()
// 	stringhah = createRandomString(totalv)

// 	client := fasthttp_client.New()
// 	mockfb := fbapis.NewMock(client)
// 	authService := &authService.AuthService{client, dbRedis}

// 	vouchers := voucher.New(dbMongo)
// 	pageconfigs := pageconfig.New(dbMongo)

// 	mockpv = processVoucher.New(vouchers, pageconfigs, authService, mockfb, dbRedis)
// 	mockfpc = feedDataToConsumer.New(mockpv, nil, nil)
// 	flushDB()
// 	t1 := time.Now()
// 	retCode := m.Run()
// 	fmt.Println("Elapsed", time.Since(t1))
// 	dbRedis.Close()
// 	if retCode != 0 {
// 		log.Println("\n********************************\n😡 😡 😡 😡 😡 😡 😡 😡 \n🔥🔥🔥🚫🚫🚫🚫🚫🚫Test Failed. Lol.🚫🚫🚫🚫🚫🚫🔥🔥🔥\n😢😢😢😢😢😢😢\n********************************")
// 	} else {
// 		log.Println("\n********************************\n 👏🎉 👏🎉 👏🎉 👏🎉\n😘😘😍😘😘😍 Test Passed.👌👌👌👌👌👌\n🏆🏆🏆🏆🏆🏆🏆\n********************************")
// 	}
// 	// dbMongo.Session.DB("BotioUjung").DropDatabase()
// 	dbMongo.Client.Disconnect(context.TODO())

// 	os.Exit(retCode)
// }

// func TestEmbeddedVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          3,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)

// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if rs.Code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 1010, found %+v", rs.Code)
// 	}
// 	if rs.Success == false {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected true, found %+v", rs.Success)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestSingleVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	resData := map[string]interface{}{}
// 	err = json.Unmarshal(resp.Body(), &resData)
// 	if err != nil {
// 		t.Errorf("TestVoucherCreation: Failed to read response body %+v", err)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	code, ok := resData["code"].(float64)
// 	if !ok {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param not found %+v", resData)

// 	}
// 	if code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 5010, found %+v", code)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestListVouchers(t *testing.T) {
// 	for i := 0; i < 1; i++ {
// 		paramsObject := map[string]interface{}{
// 			"keys":                    []string{"#cat", "#bat"},
// 			"title":                   "title",
// 			"message":                 "message",
// 			"content":                 "content",
// 			"voucher_src":             "embedded",
// 			"photos":                  []string{"photos"},
// 			"voucher_expired_message": "expiredmessage",
// 			"inbox_message":           "inbox message",
// 			"second_reply_comment":    "second_reply_comment",
// 			"comment_inbox_message":   "comment_inbox_message",
// 			"reply_comment_message":   "reply coment msg",
// 			"language":                mockLanguage,
// 			"draft":                   false,
// 			"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 			"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 			"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 			"total_vouchers":          3,
// 			"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 		}
// 		body, err := json.Marshal(paramsObject)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		req := fasthttp.AcquireRequest()
// 		req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 		req.Header.SetContentType("application/json")
// 		req.Header.Set("user-id", mockuserID)
// 		req.SetBody(body)

// 		var ctx fasthttp.RequestCtx
// 		ctx.Init(req, nil, nil)
// 		ctx.SetUserValue("page-id", mockpageID)

// 		mockpv.Create(&ctx)

// 		resp := ctx.Response

// 		if resp.StatusCode() != 200 {
// 			t.Errorf("TestListVouchers Failed ")
// 			return
// 		}
// 	}
// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.UserVouchersList(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestListVoucher Failed ")
// 	}

// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if len(rs.Data) != 1 || rs.Success == false {
// 		t.Errorf("TestListVoucher Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()

// }

// func TestExportData(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.Header.Set("page-id", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}

// 	fmt.Println(string(resp.Body()))

// 	mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	mockpv.UserVouchersList(&ctx)

// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))

// 	rs := new(responseStruct)
// 	err = json.Unmarshal(resp.Body(), rs)
// 	if err != nil {
// 		t.Errorf("TestExportData Failed %+v ", err)
// 	}
// 	if len(rs.Data) == 0 {
// 		t.Errorf("TestExportData Failed %+v ", "length is 0")
// 		return
// 	}
// 	voucherID := rs.Data[0].ID

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	ctx.SetUserValue("voucher-id", voucherID)
// 	fmt.Println(voucherID)

// 	mockpv.ExportData(&ctx)
// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }
// func BenchmarkHandleComments(b *testing.B) {
// 	flushDB()
// 	paramsObject := map[string]interface{}{
// 		"keys":                    []string{"#cat", "#bat"},
// 		"language":                mockLanguage,
// 		"title":                   "title",
// 		"message":                 "message",
// 		"additional_message":      "additional_message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"reply_comment_message":   "reply coment msg",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          totalv,
// 		"coupon_list":             stringhah,
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// 		}
// 	})
// 	// logger.Info(a)
// }

// func BenchmarkHandleCommentsSecondTime(b *testing.B) {
// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// 		}
// 	})
// }

// func String(length int) string {
// 	return StringWithCharset(length, charset)
// }
// func flushDB() {
// 	if config.AppEnvironment == "test" {
// 		var err error
// 		status := dbRedis.FlushAll()
// 		if status != "OK" {

// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("Vouchers").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 		_, err = dbMongo.Client.Database("BotioUjung").Collection("VoucherPageConfigs").DeleteMany(context.TODO(), bson.M{})
// 		if err != nil {
// 			log.Println(err)
// 		}
// 	}
// }

// func createRandomString(n int) []string {
// 	arr := []string{}
// 	for i := 0; i < n; i++ {
// 		arr = append(arr, fmt.Sprintf("vucher_%v", i))
// 	}
// 	return arr
// }
// func TestMain(m *testing.M) {
// 	if config.AppEnvironment != "test" {
// 		os.Exit(1)
// 	}
// 	dbRedis = getRedisConnection()
// 	dbMongo = getMongoConnection()
// 	stringhah = createRandomString(totalv)

// 	client := fasthttp_client.New()
// 	mockfb := fbapis.NewMock(client)
// 	authService := &authService.AuthService{client, dbRedis}

// 	vouchers := voucher.New(dbMongo)
// 	pageconfigs := pageconfig.New(dbMongo)

// 	mockpv = processVoucher.New(vouchers, pageconfigs, authService, mockfb, dbRedis)
// 	mockfpc = feedDataToConsumer.New(mockpv, nil, nil)
// 	flushDB()
// 	t1 := time.Now()
// 	retCode := m.Run()
// 	fmt.Println("Elapsed", time.Since(t1))
// 	dbRedis.Close()
// 	if retCode != 0 {
// 		log.Println("\n********************************\n😡 😡 😡 😡 😡 😡 😡 😡 \n🔥🔥🔥🚫🚫🚫🚫🚫🚫Test Failed. Lol.🚫🚫🚫🚫🚫🚫🔥🔥🔥\n😢😢😢😢😢😢😢\n********************************")
// 	} else {
// 		log.Println("\n********************************\n 👏🎉 👏🎉 👏🎉 👏🎉\n😘😘😍😘😘😍 Test Passed.👌👌👌👌👌👌\n🏆🏆🏆🏆🏆🏆🏆\n********************************")
// 	}
// 	// dbMongo.Session.DB("BotioUjung").DropDatabase()
// 	dbMongo.Client.Disconnect(context.TODO())

// 	os.Exit(retCode)
// }

// func TestEmbeddedVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          3,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("UserID", mockuserID)
// 	req.Header.Set("PageID", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)

// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if rs.Code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 1010, found %+v", rs.Code)
// 	}
// 	if rs.Success == false {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected true, found %+v", rs.Success)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestSingleVoucherCreation(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("UserID", mockuserID)
// 	req.Header.Set("PageID", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestVoucherCreation Failed ")
// 	}
// 	resData := map[string]interface{}{}
// 	err = json.Unmarshal(resp.Body(), &resData)
// 	if err != nil {
// 		t.Errorf("TestVoucherCreation: Failed to read response body %+v", err)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	code, ok := resData["code"].(float64)
// 	if !ok {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param not found %+v", resData)

// 	}
// 	if code != 1010 {
// 		t.Errorf("TestVoucherCreation: Failed to read response body. code param expected 5010, found %+v", code)
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }

// func TestListVouchers(t *testing.T) {
// 	for i := 0; i < 1; i++ {
// 		paramsObject := map[string]interface{}{
// 			"keys":                    []string{"#cat", "#bat"},
// 			"title":                   "title",
// 			"message":                 "message",
// 			"content":                 "content",
// 			"voucher_src":             "embedded",
// 			"photos":                  []string{"photos"},
// 			"voucher_expired_message": "expiredmessage",
// 			"inbox_message":           "inbox message",
// 			"second_reply_comment":    "second_reply_comment",
// 			"comment_inbox_message":   "comment_inbox_message",
// 			"reply_comment_message":   "reply coment msg",
// 			"language":                mockLanguage,
// 			"draft":                   false,
// 			"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 			"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 			"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 			"total_vouchers":          3,
// 			"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 		}
// 		body, err := json.Marshal(paramsObject)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		req := fasthttp.AcquireRequest()
// 		req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 		req.Header.SetContentType("application/json")
// 		req.Header.Set("UserID", mockuserID)
// 		req.SetBody(body)

// 		var ctx fasthttp.RequestCtx
// 		ctx.Init(req, nil, nil)
// 		ctx.SetUserValue("page-id", mockpageID)

// 		mockpv.Create(&ctx)

// 		resp := ctx.Response

// 		if resp.StatusCode() != 200 {
// 			t.Errorf("TestListVouchers Failed ")
// 			return
// 		}
// 	}
// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("UserID", mockuserID)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.UserVouchersList(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestListVoucher Failed ")
// 	}

// 	rs := new(responseStruct)
// 	json.Unmarshal(resp.Body(), rs)
// 	if len(rs.Data) != 1 || rs.Success == false {
// 		t.Errorf("TestListVoucher Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()

// }

// func TestExportData(t *testing.T) {
// 	paramsObject := map[string]interface{}{
// 		// "coupon_initial":     "acxs",
// 		"keys":                    []string{"#cat", "#bat"},
// 		"title":                   "title",
// 		"message":                 "message",
// 		"content":                 "content",
// 		"voucher_src":             "single",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"reply_comment_message":   "reply coment msg",
// 		"language":                mockLanguage,
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          1000000,
// 		"coupon_list":             []string{"sadas239231", "sdsd213e12", "ascas2143141"},
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("UserID", mockuserID)
// 	req.Header.Set("PageID", mockpageID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	resp := ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}

// 	fmt.Println(string(resp.Body()))

// 	mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	mockpv.UserVouchersList(&ctx)

// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))

// 	rs := new(responseStruct)
// 	err = json.Unmarshal(resp.Body(), rs)
// 	if err != nil {
// 		t.Errorf("TestExportData Failed %+v ", err)
// 	}
// 	if len(rs.Data) == 0 {
// 		t.Errorf("TestExportData Failed %+v ", "length is 0")
// 		return
// 	}
// 	voucherID := rs.Data[0].ID

// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)
// 	ctx.SetUserValue("voucher-id", voucherID)
// 	fmt.Println(voucherID)

// 	mockpv.ExportData(&ctx)
// 	resp = ctx.Response

// 	if resp.StatusCode() != 200 {
// 		t.Errorf("TestExportData Failed ")
// 	}
// 	fmt.Println(string(resp.Body()))
// 	flushDB()
// }
// func BenchmarkHandleComments(b *testing.B) {
// 	flushDB()
// 	paramsObject := map[string]interface{}{
// 		"keys":                    []string{"#cat", "#bat"},
// 		"language":                mockLanguage,
// 		"title":                   "title",
// 		"message":                 "message",
// 		"additional_message":      "additional_message",
// 		"content":                 "content",
// 		"voucher_src":             "embedded",
// 		"photos":                  []string{"photos"},
// 		"voucher_expired_message": "expiredmessage",
// 		"inbox_message":           "inbox message",
// 		"reply_comment_message":   "reply coment msg",
// 		"second_reply_comment":    "second_reply_comment",
// 		"comment_inbox_message":   "comment_inbox_message",
// 		"draft":                   false,
// 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// 		"total_vouchers":          totalv,
// 		"coupon_list":             stringhah,
// 	}
// 	body, err := json.Marshal(paramsObject)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req := fasthttp.AcquireRequest()
// 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// 	req.Header.SetContentType("application/json")
// 	req.Header.Set("user-id", mockuserID)
// 	req.SetBody(body)

// 	var ctx fasthttp.RequestCtx
// 	ctx.Init(req, nil, nil)
// 	ctx.SetUserValue("page-id", mockpageID)

// 	mockpv.Create(&ctx)

// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// 		}
// 	})
// 	// logger.Info(a)
// }

// // func BenchmarkHandleCommentsSecondTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsThirdTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsFourthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsfifthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsSixthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsSeventhTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsEighthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsninethTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsTenthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsEleventhTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsTwelvethTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleCommentsThirteenthTime(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments14Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func BenchmarkHandleComments15Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments16Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments17Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments18Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments19Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments20Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments21Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments22Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments23Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments24Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments25Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments26Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments27Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments28Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments29Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments30Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments31Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments32Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments33Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments34Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments35Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments36Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments37Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments38Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments39Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments40Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments41Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments42Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments43Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments44Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments45Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments46Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments47Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments48Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments49Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }
// // func BenchmarkHandleComments50Time(b *testing.B) {
// // 	b.RunParallel(func(pb *testing.PB) {
// // 		for pb.Next() {
// // 			mockfpc.PV.HandleComments2(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 		}
// // 	})
// // }

// // func TestHandlecomments(t *testing.T) {
// // 	flushDB()

// // 	paramsObject := map[string]interface{}{
// // 		"keys":                    []string{"#cat", "#bat"},
// // 		"language":                mockLanguage,
// // 		"title":                   "title",
// // 		"message":                 "message",
// // 		"additional_message":      "additional_message",
// // 		"content":                 "content",
// // 		"voucher_src":             "embedded",
// // 		"photos":                  []string{"photos"},
// // 		"voucher_expired_message": "expiredmessage",
// // 		"inbox_message":           "inbox message",
// // 		"reply_comment_message":   "reply coment msg",
// // 		"second_reply_comment":    "second_reply_comment",
// // 		"comment_inbox_message":   "comment_inbox_message",
// // 		"draft":                   false,
// // 		"start_time":              time.Now().Add(1 * time.Minute).Unix(),
// // 		"coupon_expire_time":      time.Now().Add(24 * time.Hour).Unix(),
// // 		"end_time":                time.Now().Add(30 * time.Minute).Unix(),
// // 		"total_vouchers":          totalv,
// // 		"coupon_list":             stringhah,
// // 	}
// // 	body, err := json.Marshal(paramsObject)
// // 	if err != nil {
// // 		log.Fatal(err)
// // 	}

// // 	req := fasthttp.AcquireRequest()
// // 	req.SetRequestURI("http://localhost:8084/ujung/pages/" + mockpageID + "/vouchers") // task URI
// // 	req.Header.SetContentType("application/json")
// // 	req.Header.Set("UserID", mockuserID)
// // 	req.SetBody(body)

// // 	var ctx fasthttp.RequestCtx
// // 	ctx.Init(req, nil, nil)
// // 	ctx.SetUserValue("page-id", mockpageID)

// // 	mockfpc.PV.Create(&ctx)
// // 	status := 0
// // 	t1 := time.Now()
// // 	var t2 float64
// // 	done := make(chan error)
// // 	go func() {
// // 		for i := 0; i < totalv; i++ {
// // 			status = <-mockfpc.result
// // 			if i%1000 == 0 {
// // 				t2 = time.Since(t1).Seconds()
// // 				logger.Info("req/sec: ", i+1, " ", t2, 1000/t2)
// // 				t1 = time.Now()
// // 			}
// // 			if status == 0 {
// // 				result["inputerr"] = result["inputerr"] + 1
// // 			} else if status == 1 {
// // 				result["pass"] = result["pass"] + 1
// // 			} else if status == 0 {
// // 				result["retry"] = result["retry"] + 1
// // 			}
// // 		}
// // 		done <- nil
// // 	}()
// // 	for i := 0; i < totalv; i++ {
// // 		mockfpc.testwork(append(append(part1[:], []byte(String(5))...), part2[:]...))
// // 	}
// // 	<-done
// // 	logger.Info(result)
// // }

// // func (mockfpc *FeedDataToConsumer) testwork(data []byte) {
// // 	mockfpc.semaphore <- struct{}{}
// // 	status := 0
// // 	go func() {
// // 		defer func() {
// // 			<-mockfpc.semaphore
// // 			mockfpc.result <- status
// // 		}()
// // 		status = mockfpc.PV.HandleComments2(data)
// // 	}()

// // }
