package voucher

import (
	"botio-voucher/app/model"
	"botio-voucher/app/storage"
	"encoding/json"
	"fmt"
)

// key post_id
type VoucherInfo struct {
	PostID                   string                `json:"post_id"  bson:"post_id"`
	VoucherTotal             int                   `json:"voucher_total" bson:"voucher_total"`
	VoucherSrc               string                `json:"voucher_src" bson:"voucher_src"`
	Title                    string                `json:"title"  bson:"title"`
	Language                 string                `json:"language"  bson:"language"`
	VoucherTriggers          []model.Trigger       `json:"voucher_triggers"  bson:"voucher_triggers"`
	InboxMessage             string                `json:"inbox_message"  bson:"inbox_message"`
	ReplyCommentMessage      string                `json:"reply_comment_message"  bson:"reply_comment_message"`
	VoucherExpiredMessage    string                `json:"voucher_expired_message"  bson:"voucher_expired_message"`
	SecondReplyComment       string                `json:"second_reply_comment" bson:"second_reply_comment"`
	CommentInboxMessage      string                `json:"comment_inbox_message" bson:"comment_inbox_message"`
	EndTime                  int64                 `json:"end_time"  bson:"end_time"`
	Ended                    bool                  `json:"ended" bson:"ended"`
	Survey                   []model.Survey        `json:"survey" bson:"survey"`
	Photos                   []string              `json:"photos" bson:"photos"`
	VoucherType              string                `json:"voucher_type" bson:"voucher_type"`
	VoucherTemplate          model.VoucherTemplate `json:"voucher_template" bson:"voucher_template"`
	VoucherSellPrice         float64               `json:"voucher_sell_price" bson:"voucher_sell_price"`
	EarlyDistributionMessage string                `json:"early_distribution_message" bson:"early_distribution_message"`
	DistributionStartOn      int64                 `json:"distribution_start_on" bson:"distribution_start_on"`
	CouponCodeExpiredMessage string                `json:"couponcode_expired_message" bson:"couponcode_expired_message"`
	VoucherRunOutOfMessage   string                `json:"voucher_run_out_of_message" bson:"voucher_run_out_of_message"`
}

func (c *VoucherInfo) MarshalBinary() ([]byte, error) {
	return json.Marshal(c)
}
func (c *VoucherInfo) UnmarshalBinary(body []byte) error {
	return json.Unmarshal(body, c)
}

func (c *VoucherInfo) Get(key string, redis storage.Storager) error {
	i, err := redis.Get(key)
	if err != nil {
		return err
	}
	if i == nil {
		return fmt.Errorf("Doesnt exist in redis")
	}
	return c.UnmarshalBinary([]byte(i.(string)))
}

func (c *VoucherInfo) Set(key string, redis storage.Storager) error {
	err := redis.Set(key, c)
	return err
}

func (c *VoucherInfo) Delete(key string, redis storage.Storager) error {
	return redis.Del(key)
}
