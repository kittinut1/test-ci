package voucher

import (
	"botio-voucher/app/storage"
	"encoding/json"
)

// key psid
type UserState struct {
	UserID    string            `json:"user_id"  bson:"user_id"`
	PostsInfo map[string]string `json:"post_info" bson:"post_info"`
}

func (c *UserState) MarshalBinary() ([]byte, error) {
	return json.Marshal(c)
}
func (c *UserState) UnmarshalBinary(body []byte) error {
	return json.Unmarshal(body, c)
}

func (c *UserState) Get(key string, redis storage.Storager) error {
	i, err := redis.Get(key)
	if err != nil {
		return err
	}
	if i == nil {
		return nil
	}
	return c.UnmarshalBinary([]byte(i.(string)))
}

func (c *UserState) Set(key string, redis storage.Storager) error {
	return redis.Set(key, c)
}

func (c *UserState) Delete(key string, redis storage.Storager) error {
	return redis.Del(key)
}
