package voucher

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/storage/mongo"
	"context"
	"fmt"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

type Voucherer interface {
	Create(userID, pageID, postID, title, message, inboxmessage, replycomment, secondreplycomment, voucherexpiredmessage, commentinboxmessage, language, vouchersrc, vouchertype, howToUse, details, voucher_redeemed_message, early_distribution_message, couponcode_expired_message, voucher_run_out_of_message string, totalVouchers int, triggerKeys, bannedKeys, couponsList, photos []string, survey interface{}, voucherExpiredOn, startTime, endTime int64, voucherSellPrice float64, started, draft bool, vouchertemplate interface{}) (string, error)
	GetOne(query, selectedField bson.M, result interface{}) error
	GetAll(query, selected bson.M, result interface{}, page, limit int64) error
	Update(query, update bson.M) error
	AtomicUpdate(query, update, selectedfields bson.M, result interface{}) error
}

type Voucher struct {
	mongo *mongo.Mongo
}

const DB = "BotioUjung"
const Collection = "Vouchers"

func New(dbMongo *mongo.Mongo) *Voucher {
	err := dbMongo.Client.Ping(context.TODO(), nil)
	if err != nil {
		fmt.Println("Mongo Error: ", err)
		os.Exit(1)
	}
	a := &Voucher{
		mongo: dbMongo,
	}
	err = a.mongo.IndexCollections(DB, Collection, model.VoucherKeys)
	if err != nil {
		logger.Error(err, "Indexing Vouchers Collection Failed")
	}
	return a
}

func (v *Voucher) Create(userID, pageID, postID, title, message, inboxmessage, replycomment, secondreplycomment, voucherexpiredmessage, commentinboxmessage, language, vouchersrc, vouchertype, howToUse, details, voucher_redeemed_message, early_distribution_message, couponcode_expired_message, voucher_run_out_of_message string, totalVouchers int, triggerKeys, bannedKeys, couponsList, photos []string, survey interface{}, voucherExpiredOn, startTime, endTime int64, voucherSellPrice float64, started, draft bool, vouchertemplate interface{}) (string, error) {
	tkeys := []model.Trigger{}
	for i := range triggerKeys {
		tkeys = append(tkeys, model.Trigger{
			Type:      "keyword",
			Condition: triggerKeys[i],
		})
	}
	timestamp := time.Now().Unix()
	document := bson.M{
		"user_id":                    userID,
		"page_id":                    pageID,
		"post_id":                    postID,
		"title":                      title,
		"message":                    message,
		"inbox_message":              inboxmessage,
		"reply_comment_message":      replycomment,
		"voucher_expired_message":    voucherexpiredmessage,
		"language":                   language,
		"voucher_triggers":           tkeys,
		"survey":                     survey,
		"voucher_total":              totalVouchers,
		"voucher_count":              0,
		"voucher_list":               couponsList,
		"comment_inbox_message":      commentinboxmessage,
		"voucher_expired_on":         voucherExpiredOn,
		"published":                  draft,
		"start_time":                 startTime,
		"history":                    map[string](model.Voucher){},
		"end_time":                   endTime,
		"started":                    started,
		"ended":                      false,
		"created_on":                 timestamp,
		"edited_on":                  timestamp,
		"status":                     "running",
		"voucher_type":               vouchertype,
		"voucher_src":                vouchersrc,
		"second_reply_comment":       secondreplycomment,
		"voucher_dirty_list":         []string{},
		"photos":                     photos,
		"voucher_template":           vouchertemplate,
		"voucher_sell_price":         voucherSellPrice,
		"how_to_use":                 howToUse,
		"details":                    details,
		"voucher_redeemed_message":   voucher_redeemed_message,
		"early_distribution_message": early_distribution_message,
		"couponcode_expired_message": couponcode_expired_message,
		"voucher_run_out_of_message": voucher_run_out_of_message,
	}
	id, err := v.mongo.Insert(DB, Collection, document)
	if err != nil {
		fmt.Printf("Mongo Error: %vn \n", err)
	}
	return id, err
}

func (v *Voucher) GetOne(query, selectedField bson.M, result interface{}) error {
	return v.mongo.FindOne(DB, Collection, query, selectedField, result)
}

func (v *Voucher) GetAll(query, selected bson.M, result interface{}, page, limit int64) error {
	return v.mongo.FindAll(DB, Collection, query, selected, result, bson.M{"_id": -1}, page, limit)
}

func (v *Voucher) Update(query, update bson.M) error {
	return v.mongo.Update(DB, Collection, query, update)
}

func (v *Voucher) AtomicUpdate(query, update, selectedfields bson.M, result interface{}) error {
	return v.mongo.FindAndModify(DB, Collection, query, update, selectedfields, result, true)
}
