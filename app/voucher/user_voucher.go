package voucher

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/storage"
	"encoding/json"
)

// key bidderid_postid
type UserVoucher struct {
	TransactionID     string         `json:"transaction_id"`
	VoucherType       string         `json:"voucher_type"`
	Type              string         `json:"type"`
	Name              string         `json:"name"`
	VoucherSellPrice  float64        `json:"voucher_sell_price"`
	PostID            string         `json:"post_id"`
	PSID              string         `json:"psid"`
	State             int            `json:"state"`
	Answers           []string       `json:"answers"`
	Survey            []model.Survey `json:"survey"`
	UserID            string         `json:"user_id"`
	CouponCode        string         `json:"coupon_code"`
	InboxMessage      string         `json:"inbox_message"`
	Language          string         `json:"language"`
	VoucherTemplate   model.VoucherTemplate
	ExpireReservation int64 `json:"expire_reservation"`
	Photos            []string
	Recieve           bool `json:"recieve"`
	Send              bool `json:"Send"`
	LastSentTimestamp int64
}

func (c *UserVoucher) MarshalBinary() ([]byte, error) {
	return json.Marshal(c)
}
func (c *UserVoucher) UnmarshalBinary(body []byte) error {
	return json.Unmarshal(body, c)
}

func (c *UserVoucher) Get(key string, redis storage.Storager) error {
	i, err := redis.Get(key)
	if err != nil {
		return err
	}
	if i == nil {
		return nil
	}
	logger.Info(i)
	return c.UnmarshalBinary([]byte(i.(string)))
}

func (c *UserVoucher) Set(key string, redis storage.Storager) error {
	return redis.Set(key, c)
}

func (c *UserVoucher) Delete(key string, redis storage.Storager) error {
	return redis.Del(key)
}
