package pageconfig

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/storage/mongo"
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/bson"
)

type PageConfiger interface {
	Create(pc *model.PageConfig) (string, error)
	GetOne(query, selectedField bson.M, result interface{}) error
	Upsert(query, update bson.M) error
	GetAll(query, selected bson.M, result interface{}, page, limit int64) error
	Update(query, update bson.M) error
	AtomicUpdate(query, update, selectedfields bson.M, result interface{}, new bool) error
}

type PageConfig struct {
	mongo *mongo.Mongo
}

const DB = "BotioUjung"
const Collection = "VoucherPageConfigs"

func New(dbMongo *mongo.Mongo) *PageConfig {
	err := dbMongo.Client.Ping(context.TODO(), nil)
	if err != nil {
		fmt.Println("Mongo Error: ", err, "Indexing ", Collection, " Collection Failed")
		os.Exit(1)
	}
	pc := &PageConfig{
		mongo: dbMongo,
	}
	err = pc.mongo.IndexCollections(DB, Collection, model.PageConfigKeys)
	if err != nil {
		logger.Error(err, "Indexing ", Collection, " Collection Failed")
	}
	return pc
}

func (pc *PageConfig) Create(value *model.PageConfig) (string, error) {
	doc := map[string]interface{}{
		"user_id":                    value.UserID,
		"page_id":                    value.PageID,
		"page_type":                  value.PageType,
		"created_on":                 value.CreatedOn,
		"edited_on":                  value.EditedOn,
		"subscribers":                value.Subscribers,
		"campaign_total":             value.CampaignTotal,
		"vouchers_total":             value.VouchersTotal,
		"subscription_type":          value.SubscriptionType,
		"subscription_start":         value.SubscriptionStart,
		"subscription_end":           value.SubscriptionEnd,
		"subscription_voucher_count": value.SubscriptionVoucherCount,
		"subscription_voucher_total": value.SubscriptionVoucherTotal,
		"payments": map[string]interface{}{
			"ksher": map[string]interface{}{
				"app_id":      value.Payments.Ksher.AppID,
				"private_key": value.Payments.Ksher.PrivateKey,
			},
		},
		"redemption_password": value.RedemptionPassword,
		"consent_message":     value.ConsentMessage,
	}
	return pc.mongo.Insert(DB, Collection, doc)
}

func (pc *PageConfig) GetOne(query, selectedField bson.M, result interface{}) error {
	return pc.mongo.FindOne(DB, Collection, query, selectedField, result)
}

func (pc *PageConfig) Upsert(query, update bson.M) error {
	return pc.mongo.Upsert(DB, Collection, query, update)
}

func (pc *PageConfig) GetAll(query, selected bson.M, result interface{}, page, limit int64) error {
	return pc.mongo.FindAll(DB, Collection, query, selected, result, bson.M{"_id": -1}, page, limit)
}

func (pc *PageConfig) Update(query, update bson.M) error {
	return pc.mongo.Update(DB, Collection, query, update)
}

func (pc *PageConfig) AtomicUpdate(query, update, selectedfields bson.M, result interface{}, new bool) error {
	return pc.mongo.FindAndModify(DB, Collection, query, update, selectedfields, result, new)
}
