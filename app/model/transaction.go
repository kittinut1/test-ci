package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var TransactionKeys = []string{
	"payment.order_number",
	"payment.service_order_number",
}

type Transaction struct {
	ID        primitive.ObjectID  `json:"_id bson:"_id"`
	UserID    string              `json:"user_id" bson:"user_id"`
	PageID    string              `json:"page_id" bson:"page_id"`
	PostID    string              `json:"post_id" bson:"post_id"`
	CreatedOn int64               `json:"created_on" bson:"created_on"`
	EditedOn  int64               `json:"edited_on" bson:"edited_on"`
	Data      TransactionData     `json:"data" bson:"data"`
	History   []TransactionStatus `json:"history" bson:"history"`
	Payment   TransactionPayment  `json:"payment" bson:"payment"`
}

type TransactionPayment struct {
	Service            string `json:"service" bson:"service"`
	Channel            string `json:"channel" bson:"channel"`
	TotalFee           string `json:"total_fee" bson:"total_fee"`
	Currency           string `json:"currency" bson:"currency"`
	Reference          string `json:"reference" bson:"reference"`
	OrderNumber        string `json:"order_number" bson:"order_number"`
	ServiceOrderNumber string `json:"service_order_number" bson:"service_order_number"`
}

type TransactionStatus struct {
	Status    string `json:"status" bson:"status"`
	Timestamp string `json:"timestamp" bson:"timestamp"`
}

type TransactionData struct {
	ID               string `json:"id" bson:"id"`
	Status           string `json:"status"`
	Type             string `json:"type" bson:"type"`
	SubscriptionType string `json:"subscription_type" bson:"subscription_type"`
	DurationType     string `json:"duration_type" bson:"duration_type"`
	QrCode           string `json:"qrcode" bson:"qrcode"`
}

// type Transaction struct {
// 	ID                primitive.ObjectID `json:"_id" bson:"_id"`
// 	PageID            string             `json:"page_id" bson:"page_id"`
// 	MerchantAccountID string             `json:"appid" bson:"appid"`
// 	NonceStr          string             `json:"nonce_str" bson:"nonce_str"`
// 	Channel           string             `json:"channel" bson:"channel"`
// 	TotalFee          string             `json:"total_fee" bson:"total_fee"`
// 	Sign              string             `json:"sign" bson:"sign"`
// 	MerchantOrderNo   string             `json:"mch_order_no" bson:"mch_order_no"`
// 	KsherOrderNo      string             `json:"ksher_order_no" bson:"ksher_order_no"`
// 	FeeType           string             `json:"fee_type" bson:"fee_type"`
// 	NotifyURL         string             `json:"notify_url" bson:"notify_url"`
// 	ImgType           string             `json:"img_type" bson:"img_type"`
// 	ProductInfo       string             `json:"product" bson:"product"`
// 	AttachData        string             `json:"attach" bson:"attach"`
// 	DeviceID          string             `json:"device_id" bson:"device_id"`
// 	CreatedOn         int64              `json:"created_on" bson:"created_on"`
// 	EditedOn          int64              `json:"edited_on" bson:"edited_on"`
// 	Result            string             `json:"result" bson:"result"`
// 	QrCodeURL         string             `json:"code_url" bson:"code_url"`
// 	APIStatus         int                `json:"code" bson:"code"`
// 	QrCodeImgDate     string             `json:"imgdat" bson:"imgdat"`
// }

// var TransactionKeys = []string{
// 	"nonce_str",
// 	"mch_order_no",
// 	"ksher_order_no",
// 	"sign",
// }
