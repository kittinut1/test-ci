package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PageConfig struct {
	ID                       primitive.ObjectID `json:"_id" bson:"_id"`
	UserID                   string             `json:"user_id" bson:"user_id"`
	PageID                   string             `json:"page_id" bson:"page_id"`
	PageType                 string             `json:"page_type" bson:"page_type"`
	CreatedOn                int64              `json:"created_on" bson:"created_on"`
	EditedOn                 int64              `json:"edited_on" bson:"edited_on"`
	Subscribers              []string           `json:"subscribers" bson:"subscribers"`
	CampaignTotal            int                `json:"campaign_total" bson:"campaign_total"`
	VouchersTotal            int                `json:"vouchers_total" bson:"vouchers_total"`
	SubscriptionType         string             `json:"subscription_type" bson:"subscription_type"`
	SubscriptionStart        int64              `json:"subscription_start" bson:"subscription_start"`
	SubscriptionEnd          int64              `json:"subscription_end" bson:"subscription_end"`
	SubscriptionVoucherCount int                `json:"subscription_voucher_count" bson:"subscription_voucher_count"`
	SubscriptionVoucherTotal int                `json:"subscription_voucher_total" bson:"subscription_voucher_total"`
	Payments                 PageConfigPayment  `json:"payments" bson:"payments"`
	ConsentMessage           string             `json:"consent_message" bson:"consent_message"`
	RedemptionPassword       map[string]string  `json:"redemption_password" bson:"redemption_password"`
}

type PageConfigPayment struct {
	Ksher KsherInfo `json:"ksher" bson:"ksher"`
}
type KsherInfo struct {
	AppID      string `json:"app_id" bson:"app_id"`
	PrivateKey string `json:"private_key" bson:"private_key"`
}

var PageConfigKeys = []string{"page_id"}
