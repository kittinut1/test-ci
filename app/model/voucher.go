package model

type Campaign struct {
	ID                       string             `json:"_id"  bson:"_id"`
	Title                    string             `json:"title"  bson:"title"`
	UserID                   string             `json:"user_id"  bson:"user_id"`
	PageID                   string             `json:"page_id"  bson:"page_id"`
	PostID                   string             `json:"post_id"  bson:"post_id"`
	CreatedOn                int64              `json:"created_on"  bson:"created_on"`
	EditedOn                 int64              `json:"edited_on"  bson:"edited_on"`
	Message                  string             `json:"message"  bson:"message"`
	Language                 string             `json:"language"  bson:"language"`
	Published                bool               `json:"published"  bson:"published"`
	Photos                   []string           `json:"photos"  bson:"photos"`
	StartTime                int64              `json:"start_time"  bson:"start_time"`
	EndTime                  int64              `json:"end_time"  bson:"end_time"`
	Started                  bool               `json:"started"  bson:"started"`
	Ended                    bool               `json:"ended"  bson:"ended"`
	History                  map[string]Voucher `json:"history"  bson:"history"`
	IndexData                []string           `json:"index_data" bson:"index_data"`
	VoucherTotal             int                `json:"voucher_total"  bson:"voucher_total"`
	VoucherSellPrice         float64            `json:"voucher_sell_price" bson:"voucher_sell_price"`
	VoucherCount             int                `json:"voucher_count"  bson:"voucher_count"`
	VoucherType              string             `json:"voucher_type" bson:"voucher_type"`
	VoucherSrc               string             `json:"voucher_src" bson:"voucher_src"`
	VoucherDirtyList         []string           `json:"voucher_dirty_list" bson:"voucher_dirty_list"`
	VoucherList              []string           `json:"voucher_list" bson:"voucher_list"`
	DistributionStartOn      int64              `json:"distribution_start_on" bson:"distribution_start_on"`
	RedemptionStartOn        int64              `json:"voucher_start_on" bson:"voucher_start_on"`
	RedemptionExpiredOn      int64              `json:"voucher_expired_on" bson:"voucher_expired_on"`
	VoucherTriggers          []Trigger          `json:"voucher_triggers" bson:"voucher_triggers"`
	Survey                   []Survey           `json:"survey" bson:"survey"`
	InboxMessage             string             `json:"inbox_message" bson:"inbox_message"`
	ReplyCommentMessage      string             `json:"reply_comment_message" bson:"reply_comment_message"`
	VoucherExpiredMessage    string             `json:"voucher_expired_message" bson:"voucher_expired_message"`
	SecondReplyComment       string             `json:"second_reply_comment" bson:"second_reply_comment"`
	CommentInboxMessage      string             `json:"comment_inbox_message" bson:"comment_inbox_message"`
	VoucherTemplate          VoucherTemplate    `json:"voucher_template" bson:"voucher_template"`
	Status                   string             `json:"status" bson:"status"`
	Details                  string             `json:"details" bson:"details"`
	HowToUse                 string             `json:"how_to_use" bson:"how_to_use"`
	VoucherRedeemedMessage   string             `json:"voucher_redeemed_message" bson:"voucher_redeemed_message"`
	EarlyDistributionMessage string             `json:"early_distribution_message" bson:"early_distribution_message"`
	CouponCodeExpiredMessage string             `json:"couponcode_expired_message" bson:"couponcode_expired_message"`
	VoucherRunOutOfMessage   string             `json:"voucher_run_out_of_message" bson:"voucher_run_out_of_message"`
}

type VoucherTemplate struct {
	Type         string   `json:"type" bson:"type"`
	Title        string   `json:"title" bson:"title"`
	Subtitle     string   `json:"subtitle" bson:"subtitle"`
	ImageURL     []string `json:"image_url" bson:"image_url"`
	ActionURL    string   `json:"action_url" bson:"action_url"`
	InboxMessage string   `json:"inbox_message"  bson:"inbox_message"`
	Buttons      []Button `json:"buttons" bson:"buttons"`
}

type Button struct {
	Title string `json:"title" bson:"title"`
	Url   string `json:"url" bson:"url"`
}

type Trigger struct {
	Type      string `json:"type" bson:"type"`
	Condition string `json:"condition" bson:"condition"`
}

type Survey struct {
	Type       string              `json:"type" bson:"type"`       // quick_reply, question, message, release_voucher
	Message    map[string]string   `json:"message" bson:"message"` //
	Image      string              `json:"image" bson:"image"`
	Options    []map[string]string `json:"options" bson:"options"`
	FieldName  string              `json:"field_name" bson:"field_name"`
	Validation string              `json:"validation" bson:"validation"`
}

type Voucher struct {
	VoucherID          string       `json:"id" bson:"id"`
	RedeemCode         string       `json:"redeem_code" bson:"redeem_code"`
	Name               string       `json:"name"  bson:"name"`
	CommentID          string       `json:"comment_id"  bson:"comment_id"`
	Answers            []string     `json:"answers" bson:"answers"`
	CouponCode         string       `json:"coupon_code"  bson:"coupon_code"`
	Redeemed           bool         `json:"redeemed"  bson:"redeemed"`
	RecievedOn         int64        `json:"recieved_on"  bson:"recieved_on"`
	UsedOn             int64        `json:"used_on"  bson:"used_on"`
	PSID               string       `json:"psid"  bson:"psid"`
	Message            string       `json:"message"  bson:"message"`
	Timestamp          int64        `json:"timestamp"  bson:"timestamp"`
	Status             string       `json:"status"  bson:"status"`
	Error              VoucherError `json:"error"  bson:"error"`
	ExpiredReservation int64        `json:"expired_reservation" bson:"expired_reservation"`
}

type VoucherError struct {
	Type string `json:"type" bson:"type"`
	Info string `json:"info" bson:"bson"`
}

var VoucherKeys = []string{"post_id"}
