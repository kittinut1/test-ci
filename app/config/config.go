package config

import (
	"os"
	"time"
)

type SubscritptionModel struct {
	Description      string                  `json:"description" bson:"description"`
	VouchersPerMonth int                     `json:"vouchers_per_campaign"`
	PaymentModel     map[string]PaymentModel `json:"payment" bson:"payment"` // month, year
}

type PaymentModel struct {
	Duration time.Duration `json:"duration" bson:"duration"`
	Price    float64       `json:"price" bson:"price"`
}

var (
	ImgbbAPIKey      = "d452aeb42e86e8b048c9b70060fda253"
	AllSubscriptions = map[string]SubscritptionModel{
		"free": SubscritptionModel{
			Description:      "free",
			VouchersPerMonth: 100,
			PaymentModel: map[string]PaymentModel{
				"month": PaymentModel{
					Duration: 7 * 24 * time.Hour,
					Price:    0,
				},
				"year": PaymentModel{
					Duration: 7 * 24 * time.Hour,
					Price:    0,
				},
			},
		},
		"basic": SubscritptionModel{
			Description:      "advance",
			VouchersPerMonth: 1400,
			PaymentModel: map[string]PaymentModel{
				"month": PaymentModel{
					Duration: 30 * 24 * time.Hour,
					Price:    9900,
				},
				"year": PaymentModel{
					Duration: 365 * 24 * time.Hour,
					Price:    988800,
				},
			},
		},
		"advance": SubscritptionModel{
			Description:      "advance",
			VouchersPerMonth: 10000,
			PaymentModel: map[string]PaymentModel{
				"month": PaymentModel{
					Duration: 30 * 24 * time.Hour,
					Price:    249900,
				},
				"year": PaymentModel{
					Duration: 365 * 24 * time.Hour,
					Price:    2698800,
				},
			},
		},
	}

	AppEnvironment = func() string {
		if os.Getenv("APP_ENV") == "" {
			os.Exit(1)
		}
		return os.Getenv("APP_ENV")
	}()
	QrCodeURL                  = "https://rocket-voucher.botio.io/voucher?"
	Concurrency                = 50
	FreeDuration               = 24 * 30 * time.Hour
	MaxConnectionPool          = 100
	KafkaTopics                = []string{"feed", "messaging", "retry_messaging"}
	NumberOfProducers          = 1
	MaximumVouchers            = 20000
	NumberOfMessagingConsumers = 1
	NumberOfRetryConsumers     = 0
	NumbetOfFeedConsumers      = 1
	VoucherReservationTime     = 3 * time.Hour
	RedisHost                  = os.Getenv("REDIS_HOST")
	RedisPort                  = os.Getenv("REDIS_PORT")
	RedisPassword              = os.Getenv("REDIS_PASSWORD")
	MongoHost                  = os.Getenv("MONGO_HOST")
	MongoPort                  = os.Getenv("MONGO_PORT")
	MongoUserName              = os.Getenv("MONGO_USERNAME")
	MongoPassword              = os.Getenv("MONGO_PASSWORD")
	MongoDBURL                 = os.Getenv("MONGODB_URL")
	AppID                      = os.Getenv("APP_ID")
	AppSecret                  = os.Getenv("APP_SECRET")
	WebURL                     = os.Getenv("WEB_URL")

	FacebookAPIEndpoint      = "https://graph.facebook.com/v4.0/"
	FacebookVideoAPIEndpoint = "https://graph-video.facebook.com/"
	FacebookAPIVersion       = "v4.0"
	BasicAuthPrefix          = []byte("Bearer ")
	TokenIssuer              = os.Getenv("TOKEN_ISSUER")
	TokenSecret              = []byte(os.Getenv("JWT_SECRET"))
	Environment              = os.Getenv("APP_ENV")

	SupportedCurrency    = []string{"thb", "usd", "ntd"}
	SupportedLanguage    = []string{"th", "en", "zh-tw"}
	SupportedVoucherSrc  = []string{"embedded", "single"}
	SupportedVoucherType = []string{"instant", "web"}
	SupportedRegex       = map[string]string{
		"email":  "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
		"number": `^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`,
	}
	MessageTemplate = []string{"carousal", "imagebutton", "text"}

	AuthService = os.Getenv("SERVER_URL")

	VoucherResponse = map[string]map[string]string{
		"en": map[string]string{
			"1":                     "{{reply_comment}}",
			"2":                     "Your voucher code is {{voucher_code}}{{inbox_message}}\n-- powered by Rocket Voucher (botio.io)",
			"3":                     "hello, thanks for interesting in our {{event_name}}'s voucher 🙂",
			"3_qr_1":                "OK",
			"4":                     "There are no coupons left. Try next time",
			"5":                     "{{second_reply_comment}}",
			"6":                     "No duplicaiotns",
			"7":                     "{{comment_inbox_message}}",
			"8":                     "Answer some questions? And complete the survey in 10 minutes, otherwise your voucher reservation will expire.",
			"9":                     "Are you sure?",
			"9_qr_1":                "confirm",
			"9_qr_2":                "edit",
			"10":                    "Voucher Reservation Expired",
			"title":                 "wudhqw",
			"subtitle":              "dwqdqwdwqd",
			"carousal_button_1":     "Map",
			"carousal_button_1_url": "http://maps.apple.com/maps?q=bangkok&ll=13.7,100.5&z=15",
			"carousal_button_2":     "Web",
			"carousal_button_2_url": "https://www.botio.io",
			"carousal_button_3":     "See condo",
			"carousal_button_3_url": "https://www.facebook.com/thethamm/",
			"11":                    "Qrcode URL",
			"12":                    "consent ",
			"12_qr":                 "✅ok",
		},
		"th": map[string]string{
			"1":                     "{{reply_comment}}",
			"2":                     "{{inbox_message}}\n",
			"3":                     "ขอบคุณที่สนใจงานเปิดตัวโครงการใหม่! เดอะ ธาม 2 อ่อนนุช-มอเตอร์เวย์ ทาวน์โฮมเหนือระดับ ใกล้รถไฟฟ้า-โรบินสัน",
			"3_qr_1":                "รับบัตรเชิญ",
			"4":                     "{{voucher_expired_message}}",
			"5":                     "{{second_reply_comment}}",
			"6":                     "กิจกรรมนี้ ผู้เล่นจะได้รับ1โค้ดเท่านั้น",
			"7":                     "{{comment_inbox_message}}",
			"8":                     "เพื่อรับสิทธิสุดพิเศษ เราขอทราบข้อมูลของคุณ {{first_name}} หน่อยนะคะ กรุณากรอกข้อมูลเพื่อตอบคำถามต่อไปนี้",
			"9":                     "กรุณากด 'ยืนยัน' เพื่อรับบัตรเชิ",
			"9_qr_1":                "ยืนยัน",
			"9_qr_2":                "แก้ไขข้อมูล",
			"10":                    "Voucher Reservation Expired",
			"validation":            "คุณกรอกข้อมูลไม่ถูกต้อง กรุณากรอกใหม่",
			"title":                 "บัตรเชิญเลขที่ XXXX {{voucher_code}}",
			"subtitle":              "โปรดแสดงหน้าจอนี้เพื่อยืนยันตัวตนกับเจ้าหน้าที่หน้างานในวันที่ 15-16 ก.พ. 2020",
			"carousal_button_1":     "📌 นำทาง (Map)",
			"carousal_button_1_url": "http://maps.apple.com/maps?q=bangkok&ll=13.7,100.5&z=15",
			"carousal_button_2":     "👉 รายละเอียดโครงการ",
			"carousal_button_2_url": "https://www.facebook.com/thethamm/",
			"carousal_button_3":     "🏠 ดูภาพบ้านตัวอย่าง",
			"carousal_button_3_url": "https://www.facebook.com/thethamm/",
			"11":                    "กดแลกรับสิทธิ์ ",

			"12":    "ยินดีด้วย คุณได้รับ สิทธิพิเศษในซื้อคูปอง ",
			"12_qr": " ✅ok",

			"BC1-1":   "ขอบคุณที่ร่วมสนุกครับ วอชเชอร์ถูกส่งไปเรียบร้อยแล้ว 🚀 \n เช็คในอินบ๊อกซ์ได้เลย 😁",
			"BC1-2":   "ยินดีด้วยครับ ~ \n คุณได้รับวอชเชอร์ {{event_name}} \n กดปุ่ม 'ยอมรับข้อเสนอ' ด้านล่างเพื่อรับวอชเชอร์",
			"BC1-2-1": "ยอมรับข้อเสนอ",
			"BC5-1":   "ลิงค์สำหรับรายละเอียดการขอความยินยอม",
			"BC5-1-1": "",
			"BC1-3":   "คุุณได้แลกรับสิทธิ์เรียบร้อยแล้ว ขอบคุณครับ",
			"BC3-1-1": "ขออภัยครับ วอชเชอร์หมดแล้วครับ 😔 \n ไว้มาร่วมสนุกกันครั้งต่อไปนะครับ 😁",
			"BC3-1-2": "ขออภัยครับ คุณได้รับวอชเชอร์ไปแล้วครับ 😔(จำกัดสิทธิ์ 1 ใบต่อ 1 คน) \n ไว้มาร่วมสนุกกันครั้งต่อไปนะครับ 😁",
			"BC3-2-1": "ขออภัยครับ ขณะนี้หมดเวลาแจกวอชเชอร์แล้วครับ 😔 \n ไว้มาร่วมสนุกกันครั้งต่อไปนะครับ 😁",

			"BC3-2-4": "ขออภัยครับ ยังไม่ถึงเวลาเริ่มแจกวอชเชอร์ครับ 😔 \n วอชเชอร์จะเริ่มแจกใน {{distribution_start_on}}",
			"BC4-1":   "ขอโทษครับ ขณะนี้ระบบขัดข้อง ไม่สามารถส่งวอชเชอร์ไปในอินบ๊อกซ์ได้ครับ 😔😔😔 \n ระบบจะส่งลิงค์สำหรับวอชเชอร์ของคุณในคอมเมนท์ข้างล่างนี้ครับ 🚀 \n (คำแนะนำ: เมื่อได้ลิงค์แล้วรีบนำไปใช้ เพื่อป้องกันคนอื่นนำสิทธิ์ไปใช้ก่อนครับ 😁)",
		},
	}

	BootstrapServers = os.Getenv("KAFKA_ADDRESS")
	CcloudAPIKey     = os.Getenv("KAFKA_APIKEY")
	CcloudAPISecret  = os.Getenv("KAFKA_APISECRET")

	KsherAPI            = "http://api.mch.ksher.net/"
	KsherChannel        = []string{"wechat", "alipay", "bbl_promptpay"}
	ServerURL           = os.Getenv("SERVER_URL")
	KsherAppID          = "mch33523"
	KsherPrivateKeyPath = []byte{230, 159, 214, 46, 253, 133, 210, 146, 93, 169, 73, 210, 113, 127, 39, 12, 185, 17, 131, 130, 189, 131, 240, 106, 229, 249, 25, 55, 205, 74, 212, 244, 186, 202, 27, 171, 7, 52, 183, 62, 185, 54, 57, 185, 73, 222, 179, 20, 74, 3, 231, 194, 110, 8, 222, 80, 35, 110, 15, 173, 38, 193, 30, 89, 33, 107, 129, 235, 249, 67, 33, 53, 18, 255, 205, 120, 225, 99, 132, 135, 89, 252, 178, 242, 146, 224, 24, 113, 152, 240, 184, 48, 231, 116, 86, 235, 171, 108, 137, 221, 99, 222, 233, 26, 62, 105, 159, 41, 11, 226, 99, 34, 223, 175, 17, 124, 181, 141, 108, 205, 137, 133, 244, 235, 236, 66, 82, 112, 86, 218, 111, 195, 113, 57, 154, 39, 150, 150, 174, 111, 190, 221, 238, 118, 157, 98, 94, 8, 57, 80, 36, 234, 118, 91, 231, 184, 159, 199, 5, 114, 164, 103, 109, 35, 83, 41, 34, 195, 40, 221, 243, 20, 199, 36, 241, 223, 82, 137, 131, 26, 241, 116, 181, 184, 65, 232, 186, 58, 141, 100, 248, 174, 183, 26, 0, 212, 111, 188, 108, 237, 237, 206, 238, 71, 174, 132, 60, 59, 151, 243, 10, 156, 253, 209, 104, 115, 98, 25, 16, 229, 111, 72, 14, 253, 176, 226, 251, 187, 124, 5, 47, 3, 191, 208, 29, 8, 189, 210, 17, 165, 201, 34, 235, 24, 248, 169, 7, 57, 25, 132, 246, 178, 126, 76, 189, 38, 250, 118, 175, 84, 125, 137, 208, 152, 95, 125, 33, 111, 76, 11, 19, 243, 143, 110, 143, 156, 151, 34, 51, 204, 166, 111, 96, 174, 0, 105, 255, 174, 207, 42, 167, 32, 14, 153, 7, 183, 115, 107, 150, 65, 220, 59, 97, 156, 135, 48, 85, 33, 40, 2, 191, 154, 17, 182, 24, 167, 11, 214, 219, 37, 24, 198, 217, 47, 181, 64, 18, 106, 38, 175, 98, 52, 122, 6, 57, 130, 142, 154, 113, 201, 119, 18, 140, 21, 145, 78, 109, 162, 68, 214, 18, 159, 67, 167, 238, 211, 199, 26, 37, 42, 66, 183, 237, 60, 189, 30, 165, 178, 9, 64, 253, 173, 54, 71, 18, 67, 159, 207, 251, 127, 85, 177, 210, 67, 108, 245, 210, 74, 70, 89, 197, 20, 228, 189, 119, 173, 143, 150, 110, 74, 35, 51, 58, 194, 211, 201, 96, 224, 166, 6, 105, 240, 45, 208, 73, 99, 75, 98, 135, 245, 243, 194, 243, 192, 157, 86, 225, 150, 247, 45, 154, 147, 186, 50, 232, 223, 237, 89, 62, 1, 216, 68, 12, 71, 153, 162, 169, 136, 32, 138, 64, 179, 82, 248, 226, 198, 69, 8, 184, 22, 119, 130, 143, 93, 6, 91, 158, 245, 246, 251, 72, 142, 59, 52, 75, 196, 233, 245, 132, 37, 104, 9, 177, 214, 63, 242, 67, 57, 105, 189, 87, 78, 34, 228, 57, 111, 48, 222, 22, 116, 10, 216, 72, 0, 43, 20, 109, 132, 250, 137, 128, 7, 186, 208, 50, 192, 224, 17, 150, 28, 115, 66, 187, 173, 223, 181, 25, 250, 202, 55, 142, 80, 185, 122, 55, 70, 62, 250, 196, 6, 208, 245, 172, 71, 183, 96, 163, 126, 216, 183, 180, 199, 156, 33, 209, 199, 105, 214, 192, 22, 156, 143, 80, 181, 180, 131, 163, 123, 119, 28, 201, 128, 148, 250, 45, 81, 232, 197, 75, 200, 189, 195, 87, 155, 79, 71, 179, 252, 160, 10, 134, 133, 200, 128, 184, 162, 85, 9, 119, 83, 102, 63, 8, 211, 237, 135, 245, 91, 116, 184, 166, 122, 8, 90, 17, 70, 48, 84, 147, 119, 194, 158, 0, 91, 196, 48, 88, 25, 106, 32, 131, 6, 62, 240, 184, 237, 44, 29, 9, 220, 215, 34, 158, 226, 72, 234, 9, 82, 107, 172, 182, 253, 136, 8, 20, 26, 171, 76, 129, 232, 1, 144, 205, 54, 127, 22, 75, 246, 208, 44, 110, 148, 94, 112, 190, 65, 8, 78, 195, 132, 144, 206, 210, 95, 197, 237, 84, 175, 15, 4, 92, 229, 86, 3, 112, 17, 20, 145, 48, 250, 145, 246, 13, 173, 103, 40, 51, 157, 83, 194, 205, 80, 185, 251, 252, 63, 5, 66, 34, 28, 46, 137, 112, 89, 93, 188, 243, 138, 169, 171, 31, 95, 25, 45, 125, 95, 148, 60, 21, 106, 1, 45, 194, 32, 5, 219, 80, 206, 161, 196, 146, 24, 44, 160, 169, 52, 157, 132, 146, 29, 124, 94, 190, 177, 83, 33, 112, 159, 95, 130, 168, 230, 166, 27, 73, 154, 104, 78, 52, 212, 6, 225, 225, 100, 12, 23, 102, 37, 209, 171, 141, 94, 143, 43, 161, 129, 192, 148, 12, 66, 109, 220, 68, 200, 204, 124, 158, 137, 2, 136, 186, 194, 100, 206, 233, 65, 156, 11, 241, 219, 26, 87, 161, 24, 92, 175, 22, 189, 117, 202, 98, 223, 4, 181, 119, 180, 201, 52, 247, 100, 255, 230, 95, 192, 223, 196, 122, 58, 138, 47, 236, 36, 253, 156, 57, 63, 168, 136, 19, 87, 84, 29, 208, 79, 189, 97, 61, 25, 68, 37, 71, 231, 153, 121, 51, 54, 124, 210, 9, 224, 78, 103, 26, 49, 155, 7, 63, 204, 185, 168, 116, 167, 53, 245, 112, 196, 234, 31, 20, 118, 108, 234, 5, 108, 195, 221, 183, 114, 38, 63, 179, 75, 111, 116, 40, 132, 80, 203, 123}
	KsherFeeType        = "THB"
	KsherUsedChannel    = "bbl_promptpay"
	KsherSecretKey      = "password"
)
