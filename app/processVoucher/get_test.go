package processVoucher

// func TestGetCampaign(t *testing.T) {

// 	vouchermockdata = fmt.Sprintf(`{"_id":"567890","user_id":"2132","page_id":"1234","post_id":"dasndk","voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`,
// 		1,
// 		time.Now().Add(10*time.Minute).Unix(),
// 		time.Now().Add(35*time.Minute).Unix(),
// 		time.Now().Add(4*time.Hour).Unix(),
// 		time.Now().Add(-35*time.Minute).Unix(),
// 		time.Now().Add(4*time.Hour).Unix(),
// 		1000,
// 		2000)
// 	l := fasthttputil.NewInmemoryListener()
// 	defer l.Close()

// 	// authService := new(AuthServiceMock)
// 	// fbMock := new(FBMock)

// 	pageConfig := new(pageConfigMock)
// 	voucherMock := new(voucherMock)
// 	redisMock := new(redisMock)
// 	pv := ProcessVoucher{
// 		PageConfig: pageConfig,
// 		Voucher:    voucherMock,
// 		Redis:      redisMock,
// 		// AuthService: authService.MakeAuth(),
// 		// FB:          fbMock,
// 	}

// 	m := func(ctx *fasthttp.RequestCtx) {
// 		ctx.SetUserValue("page-id", "1234")
// 		ctx.SetUserValue("campaign-id", "567890")
// 		switch string(ctx.Path()) {
// 		case "/get":
// 			pv.GetCampaign(ctx)
// 		}
// 	}

// 	go func() {
// 		_ = fasthttp.Serve(l, m)
// 	}()

// 	c := http.Client{
// 		Transport: &http.Transport{
// 			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
// 				conn, _ := l.Dial()
// 				return conn, nil
// 			},
// 		},
// 	}

// 	expect := []string{
// 		"{\"code\":3302,\"data\":{},\"success\":true}",
// 	}
// 	for i := range expect {

// 		req, _ := http.NewRequest("GET", "http://localhost:8084/get", nil)

// 		res, _ := c.Do(req)

// 		data, _ := ioutil.ReadAll(res.Body)

// 		assert.Equal(t, expect[i], string(data))

// 		res.Body.Close()
// 	}
// }

// func TestGetPageConfig(t *testing.T) {

// 	pageconfigmockdata = `{"_id":"5e60ef28e99db3bcaef02900","page_type":"page","created_on":1583410984,"edited_on":1583410984,"subscription_type":"free","subscription_voucher_count":0,"subscription_voucher_total":100,"vouchers_total":100,"subscription_end":1584015784,"campaign_password":[],"user_id":"10214654802873783","subscription_start":1583410984,"payments":{"ksher":{"app_id":"","private_key":""}},"page_id":"395420184519977","subscribers":[],"campaign_total":0}`
// 	l := fasthttputil.NewInmemoryListener()
// 	defer l.Close()

// 	pageConfig := new(pageConfigMock)
// 	pv := ProcessVoucher{
// 		PageConfig: pageConfig,
// 	}

// 	m := func(ctx *fasthttp.RequestCtx) {
// 		ctx.SetUserValue("page-id", "1234")
// 		ctx.SetUserValue("campaign-id", "567890")
// 		switch string(ctx.Path()) {
// 		case "/get":
// 			pv.GetPageConfig(ctx)
// 		}
// 	}

// 	go func() {
// 		_ = fasthttp.Serve(l, m)
// 	}()

// 	c := http.Client{
// 		Transport: &http.Transport{
// 			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
// 				conn, _ := l.Dial()
// 				return conn, nil
// 			},
// 		},
// 	}

// 	expect := []string{
// 		"{\"code\":3502,\"data\":{},\"success\":true}",
// 	}
// 	for i := range expect {

// 		req, _ := http.NewRequest("GET", "http://localhost:8084/get", nil)

// 		res, _ := c.Do(req)

// 		data, _ := ioutil.ReadAll(res.Body)

// 		assert.Equal(t, expect[i], string(data))

// 		res.Body.Close()
// 	}
// }
