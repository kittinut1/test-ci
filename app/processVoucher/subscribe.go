package processVoucher

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"

	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
)

type subModel struct {
	PageID           string `json:"page_id" bson:"page_id"`
	SubscriptionType string `json:"subscription_type" bson:"subscription_type"`
	DurationType     string `json:"duration_type" bson:"duration_type"`
	Company          bool   `json:"company" bson:"company"`
}

func (p *ProcessVoucher) CreateSubscription(ctx *fasthttp.RequestCtx) {
	pc, s, err := checkPageConfigModel(ctx.PostBody())
	if err != nil {
		ctx.SetBodyString(`{"code": 4001, "message": "Create Subscription failed", "error": "` + err.Error() + `", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	logger.Info(fmt.Sprintf("%+v \n %+v ", pc, s))
	userID := string(ctx.Request.Header.Peek("user-id"))

	res := p.AuthService.GetTokenFromAuthService(pc.PageID)
	pageAccessToken, ok := res.Response["access_token"].(string)
	if !ok {
		ctx.SetBodyString(`{"code": 4012, "message": "Create Subscription failed", "error": "token not found", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	res = p.FB.PageInfo(pageAccessToken)
	if res.Err != nil {
		logger.Info(err)
		ctx.SetBodyString(`{"code": 4012, "message": "Create Subscription failed", "error": "` + res.Err.Error() + `", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	attach := res.Response["name"].(string) + ":" + pc.PageID + ":" + userID
	pc.UserID = userID
	query := bson.M{
		"page_id": pc.PageID,
	}

	timestamp := time.Now().Unix()
	result := new(model.PageConfig)
	err = p.PageConfig.GetOne(query, bson.M{}, result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			logger.Info(fmt.Sprintf("%+v", config.AllSubscriptions[pc.SubscriptionType]))
			logger.Info("Price: ", config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price)
			if config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price == 0 {
				pc.PageType = "page"
				pc.CreatedOn = timestamp
				pc.EditedOn = timestamp
				pc.Subscribers = []string{}
				pc.CampaignTotal = 0
				pc.VouchersTotal = config.AllSubscriptions[pc.SubscriptionType].VouchersPerMonth
				pc.SubscriptionVoucherCount = 0
				pc.SubscriptionVoucherTotal = config.AllSubscriptions[pc.SubscriptionType].VouchersPerMonth
				pc.Payments = model.PageConfigPayment{}
				pc.RedemptionPassword = map[string]string{}

				id, err := p.PageConfig.Create(pc)
				if err != nil {
					logger.Error(err)
					ctx.SetBodyString(`{"code": 4002, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
					ctx.SetStatusCode(fasthttp.StatusInternalServerError)
					return
				}
				pc.ID, _ = primitive.ObjectIDFromHex(id)
				data := map[string]interface{}{
					"code":    4003,
					"message": "Create Subscription successful",
					"data":    id,
					"success": true,
				}
				body, _ := json.Marshal(data)
				ctx.SetBody(body)
				ctx.SetStatusCode(fasthttp.StatusCreated)
				return
			}

			merchantOrderID := uuid.New().String()
			currency := config.KsherFeeType
			channel := config.KsherUsedChannel
			price := config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price
			if s.Company {
				price = (price * 1.04) / 1.07
			}
			kresult, err := p.PaymentGateway.PaymentRequest(merchantOrderID, channel, currency, config.AllSubscriptions[pc.SubscriptionType].Description, config.KsherAppID, string(config.KsherPrivateKeyPath), attach, price)
			if err != nil {
				logger.Error(err)
				ctx.SetBodyString(`{"code": 4004, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
				ctx.SetStatusCode(fasthttp.StatusInternalServerError)
				return
			}
			t := model.Transaction{
				UserID:    userID,
				PageID:    pc.PageID,
				CreatedOn: pc.CreatedOn,
				EditedOn:  pc.CreatedOn,
				Payment: model.TransactionPayment{
					Service:            channel,
					Currency:           currency,
					Channel:            channel,
					TotalFee:           strconv.Itoa(int(config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price)),
					Reference:          kresult["nonce_str"],
					OrderNumber:        merchantOrderID,
					ServiceOrderNumber: kresult["ksher_order_no"],
				},
				History: []model.TransactionStatus{
					{
						Status:    kresult["result"],
						Timestamp: kresult["time_stamp"],
					},
				},
				Data: model.TransactionData{
					Type:             "sub_new",
					ID:               "",
					SubscriptionType: s.SubscriptionType,
					DurationType:     s.DurationType,
				},
			}
			id, err := p.Transaction.Create(&t)
			if err != nil {
				fmt.Println("MOngo Error: ", err)
				ctx.SetBodyString(`{"code": 4005, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
				ctx.SetStatusCode(fasthttp.StatusInternalServerError)
				return
			}
			d := map[string]interface{}{
				"code":    4006,
				"message": "Create Subscription successful",
				"data": map[string]interface{}{
					"id":           id,
					"mch_order_id": merchantOrderID,
					"img_data":     kresult["imgdat"],
					"code_url":     kresult["code_url"],
				},
				"success": true,
			}
			body, _ := json.Marshal(d)
			ctx.SetBody(body)
			ctx.SetStatusCode(fasthttp.StatusOK)
			return
		}

		ctx.SetBodyString(`{"code": 4007, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	if strings.ToLower(pc.SubscriptionType) == "free" || config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price == 0 {
		ctx.SetBodyString(`{"code": 4008, "message": "Create Subscription failed", "error": "free subscription is available only first time", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusOK)
		return
	}

	merchantOrderID := uuid.New().String()
	currency := config.KsherFeeType
	channel := config.KsherUsedChannel
	price := config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price
	if s.Company {
		price = (price * 1.04) / 1.07
	}
	kresult, err := p.PaymentGateway.PaymentRequest(merchantOrderID, channel, currency, config.AllSubscriptions[pc.SubscriptionType].Description, config.KsherAppID, string(config.KsherPrivateKeyPath), attach, price)
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"code": 4009, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	t := model.Transaction{
		UserID:    userID,
		PageID:    pc.PageID,
		CreatedOn: timestamp,
		EditedOn:  timestamp,
		Payment: model.TransactionPayment{
			Service:            channel,
			Currency:           currency,
			Channel:            channel,
			TotalFee:           strconv.Itoa(int(config.AllSubscriptions[pc.SubscriptionType].PaymentModel[s.DurationType].Price)),
			Reference:          kresult["nonce_str"],
			OrderNumber:        merchantOrderID,
			ServiceOrderNumber: kresult["ksher_order_no"],
		},
		History: []model.TransactionStatus{
			{
				Status:    kresult["result"],
				Timestamp: kresult["time_stamp"],
			},
		},
		Data: model.TransactionData{
			Type:             "sub_upgrade",
			ID:               "",
			SubscriptionType: s.SubscriptionType,
			DurationType:     s.DurationType,
		},
	}
	id, err := p.Transaction.Create(&t)
	if err != nil {
		fmt.Println("MOngo Error: ", err)
		ctx.SetBodyString(`{"code": 4010, "message": "Create Subscription failed", "error": "unknown error", "success": false}`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	d := map[string]interface{}{
		"code":    4011,
		"message": "Create Subscription successful",
		"data": map[string]interface{}{
			"id":           id,
			"mch_order_id": merchantOrderID,
			"img_data":     kresult["imgdat"],
			"code_url":     kresult["code_url"],
		},
		"success": true,
	}
	body, _ := json.Marshal(d)
	ctx.SetBody(body)
	ctx.SetStatusCode(fasthttp.StatusOK)

	// ctx.SetBodyString(`{"code": 4007, "message": "Create Subscription failed", "error": "upgrade not available", "success": false}`)
	// ctx.SetStatusCode(fasthttp.StatusOK)
	// upgrade

}

func checkPageConfigModel(body []byte) (*model.PageConfig, *subModel, error) {
	s := new(subModel)

	err := json.Unmarshal(body, s)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, s, fmt.Errorf("post params not found")
	}
	if s.PageID == "" || s.SubscriptionType == "" || s.DurationType == "" {
		return nil, s, fmt.Errorf("params missing value")
	}
	pc := model.PageConfig{}
	timestamp := time.Now().Unix()
	for i := range config.AllSubscriptions {
		if i == s.SubscriptionType {
			for j := range config.AllSubscriptions[i].PaymentModel {
				if j == s.DurationType {
					pc = model.PageConfig{
						PageID:            s.PageID,
						SubscriptionType:  s.SubscriptionType,
						SubscriptionStart: timestamp,
						SubscriptionEnd:   time.Unix(timestamp, 0).Add(config.AllSubscriptions[i].PaymentModel[j].Duration).Unix(),
						EditedOn:          timestamp,
					}
					return &pc, s, err
				}
			}
		}
	}
	// pc := new(model.PageConfig)
	// pc.SubscriptionType = strings.ToLower(pc.SubscriptionType)
	// for i := range config.AllSubscriptions {
	// 	if i == pc.SubscriptionType {
	// 		pc.CreatedOn = time.Now().Unix()
	// 		pc.SubscriptionStart = pc.CreatedOn
	// 		pc.SubscriptionEnd = time.Unix(pc.CreatedOn, 0).Add(config.AllSubscriptions[i].Duration).Unix()
	// 		return pc, err
	// 	}
	// }
	return &pc, s, fmt.Errorf("unsupported type subscription type")
}

func (p *ProcessVoucher) KsherNotification(ctx *fasthttp.RequestCtx) {
	logger.Info("KSHERNOTIFICATION: ", string(ctx.PostBody()))
	data, err := p.PaymentGateway.RecieveNotification(ctx.PostBody())
	timestamp := time.Now().Unix()
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"result": "FAIL", "msg": "` + err.Error() + `"}`)
		return
	}
	query := bson.M{
		"payment.order_number":         data["mch_order_no"],
		"payment.service_order_number": data["ksher_order_no"],
	}
	update := bson.M{
		"$push": bson.M{
			"history": bson.M{
				"status":    data["result"],
				"timestamp": strconv.Itoa(int(timestamp)),
			},
		},
		"$set": bson.M{
			"edited_on": timestamp,
		},
	}
	result := new(model.Transaction)
	err = p.Transaction.AtomicUpdate(query, update, bson.M{}, result, true)
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"result": "FAIL", "msg": "` + err.Error() + `"}`)
		return
	}
	if data["result"] != "SUCCESS" {
		ctx.SetBodyString(`{"result": "FAIL", "msg": "` + err.Error() + `"}`)
		return
	}

	switch result.Data.Type {
	case "sub_new":
		{
			pc := &model.PageConfig{
				UserID:                   result.UserID,
				PageID:                   result.PageID,
				PageType:                 "page",
				CreatedOn:                timestamp,
				EditedOn:                 timestamp,
				Subscribers:              []string{},
				CampaignTotal:            0,
				VouchersTotal:            config.AllSubscriptions[result.Data.SubscriptionType].VouchersPerMonth + config.AllSubscriptions["free"].VouchersPerMonth,
				SubscriptionType:         result.Data.SubscriptionType,
				SubscriptionStart:        timestamp,
				SubscriptionEnd:          time.Unix(timestamp, 0).Add(config.AllSubscriptions[result.Data.SubscriptionType].PaymentModel[result.Data.DurationType].Duration).Unix(),
				SubscriptionVoucherCount: 0,
				SubscriptionVoucherTotal: config.AllSubscriptions[result.Data.SubscriptionType].VouchersPerMonth + config.AllSubscriptions["free"].VouchersPerMonth,
				Payments:                 model.PageConfigPayment{},
				RedemptionPassword:       map[string]string{},
			}
			_, err = p.PageConfig.Create(pc)
			if err != nil {
				logger.Error(err)
				ctx.SetBodyString(`{"result": "SUCCESS", "msg": "OK"}`)
				return
			}
		}
	case "sub_upgrade":
		{

			query = bson.M{
				"page_id": result.PageID,
			}
			pcresult := new(model.PageConfig)
			err = p.PageConfig.GetOne(query, bson.M{}, pcresult)
			if err != nil {
				logger.Error(err)
				ctx.SetBodyString(`{"result": "SUCCESS", "msg": "OK"}`)
				return
			}
			subend := pcresult.SubscriptionEnd
			if subend > time.Now().Unix() {
				subend = time.Unix(subend, 0).Add(config.AllSubscriptions[result.Data.SubscriptionType].PaymentModel[result.Data.DurationType].Duration).Unix()
			} else {
				subend = time.Now().Add(config.AllSubscriptions[result.Data.SubscriptionType].PaymentModel[result.Data.DurationType].Duration).Unix()
			}
			update = bson.M{
				"$set": bson.M{
					"subscription_type": result.Data.SubscriptionType,
					"edited_on":         timestamp,
				},
				"$inc": bson.M{
					"subscription_end":           subend,
					"vouchers_total":             config.AllSubscriptions[result.Data.SubscriptionType].VouchersPerMonth + config.AllSubscriptions["free"].VouchersPerMonth,
					"subscription_voucher_total": config.AllSubscriptions[result.Data.SubscriptionType].VouchersPerMonth + config.AllSubscriptions["free"].VouchersPerMonth,
				},
			}
			err = p.PageConfig.Update(query, update)
			if err != nil {
				logger.Error(err)
				ctx.SetBodyString(`{"result": "SUCCESS", "msg": "OK"}`)
				return
			}
		}
	case "sell":
		{
			query = bson.M{
				"post_id": result.PostID,
			}
			selectedfield := bson.M{
				"page_id":                  1,
				"history." + result.UserID: 1,
				"voucher_template":         1,
				"inbox_message":            1,
			}
			v := new(model.Campaign)
			err = p.Voucher.GetOne(query, selectedfield, v)
			if err != nil {
				logger.Error(err)
				ctx.SetBodyString(`{"result": "SUCCESS", "msg": "OK"}`)
				return
			}

			res := p.AuthService.GetTokenFromAuthService(result.PageID)
			pageAccessToken, ok := res.Response["access_token"].(string)
			if !ok {
				logger.Error(fmt.Errorf("Token not available for page: %v", result.PageID))
			}

			buttons := []map[string]interface{}{}
			// buttons = append(buttons, p.FB.WebURLButton("QrCode URL", config.QrCodeURL+"id="+b.PostID+"&uid="+b.BidderID+"&code="+code))
			for i := range v.VoucherTemplate.Buttons {
				buttons = append(buttons, p.FB.WebURLButton(v.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(v.VoucherTemplate.Buttons[i].Url, result.PostID, result.UserID, v.PageID)))
			}

			el := []map[string]interface{}{}
			for i := range v.VoucherTemplate.ImageURL {
				src := v.VoucherTemplate.ImageURL[i]
				el = append(el, p.FB.GenericElement(
					replaceVoucherResponseValues(v.VoucherTemplate.Title, v.History[result.UserID].CouponCode, "", v.InboxMessage, "", "", "", "", "", ""),
					src,
					replaceVoucherResponseValues(v.VoucherTemplate.Subtitle, v.History[result.UserID].CouponCode, "", v.InboxMessage, "", "", "", "", "", ""),
					replaceIDanfUID(v.VoucherTemplate.Buttons[i].Url, result.PostID, result.UserID, v.PageID),
					buttons))
			}

			m := p.FB.GenericTemplate(el, []map[string]interface{}{})

			body := map[string]interface{}{
				"messaging_type": "MESSAGE_TAG",
				"tag":            "POST_PURCHASE_UPDATE",
				"recipient": map[string]interface{}{
					"id": result.UserID,
				},
				"message": m,
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			if res.Err != nil {
				logger.Error(nil, res)
			}
			query = bson.M{
				"post_id": result.PostID,
			}

			update := bson.M{
				"$set": bson.M{
					"edited_on": timestamp,
					"history." + result.UserID + ".error.type": "sent",
					"history." + result.UserID + ".error.info": fmt.Sprintf("%+v", res),
				},
			}
			err = p.Voucher.Update(query, update)
			if err != nil {
				logger.Info("saving db error", err)
			}
		}
	}

	query = bson.M{
		"payment.order_number":         data["mch_order_no"],
		"payment.service_order_number": data["ksher_order_no"],
	}
	update = bson.M{
		"$set": bson.M{
			"data.status": "completed",
			"edited_on":   "timestamp",
		},
	}
	err = p.Transaction.Update(query, update)
	if err != nil {
		logger.Error(err)
	}
	ctx.SetBodyString(`{"result": "SUCCESS", "msg": "OK"}`)
	return
}

func (p *ProcessVoucher) CheckPayment(ctx *fasthttp.RequestCtx) {

}
