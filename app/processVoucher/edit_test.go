package processVoucher

// func TestEditCampaign(t *testing.T) {

// 	vouchermockdata = fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":true}`,
// 		1,
// 		time.Now().Add(10*time.Minute).Unix(),
// 		time.Now().Add(35*time.Minute).Unix(),
// 		time.Now().Add(4*time.Hour).Unix(),
// 		time.Now().Add(-35*time.Minute).Unix(),
// 		time.Now().Add(4*time.Hour).Unix(),
// 		1000,
// 		2000)
// 	l := fasthttputil.NewInmemoryListener()
// 	defer l.Close()

// 	// authService := new(AuthServiceMock)
// 	// fbMock := new(FBMock)

// 	pageConfig := new(pageConfigMock)
// 	voucherMock := new(voucherMock)
// 	redisMock := new(redisMock)
// 	pv := ProcessVoucher{
// 		PageConfig: pageConfig,
// 		Voucher:    voucherMock,
// 		Redis:      redisMock,
// 		// AuthService: authService.MakeAuth(),
// 		// FB:          fbMock,
// 	}

// 	m := func(ctx *fasthttp.RequestCtx) {
// 		ctx.SetUserValue("page-id", "1234")
// 		ctx.SetUserValue("campaign-id", "567890")
// 		switch string(ctx.Path()) {
// 		case "/edit":
// 			pv.EditCampaign(ctx)
// 		}
// 	}

// 	go func() {
// 		_ = fasthttp.Serve(l, m)
// 	}()

// 	c := http.Client{
// 		Transport: &http.Transport{
// 			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
// 				conn, _ := l.Dial()
// 				return conn, nil
// 			},
// 		},
// 	}

// 	cases := []map[string]interface{}{
// 		map[string]interface{}{},
// 		map[string]interface{}{"title": "new title",
// 			"message":                 "newmessage",
// 			"inbox_message":           "newinboxmessage",
// 			"reply_comment_message":   "newreplycommentmessage",
// 			"voucher_expired_message": "newvoucherexpiredmessage",
// 			"second_reply_comment":    "newsecondreplycomment",
// 			"comment_inbox_message":   "newcommentinboxmessage",
// 			"details":                 "newdetails",
// 			"how_to_use":              "newhowtouse",
// 		},
// 		// wrong end_time
// 		map[string]interface{}{
// 			"end_time": time.Now().Add(-35 * time.Minute).Unix(),
// 		},
// 		map[string]interface{}{
// 			"voucher_total": 1020,
// 		},
// 		map[string]interface{}{
// 			"voucher_sell_price": 0,
// 		},
// 		map[string]interface{}{
// 			"distribution_start_on": time.Now().Add(-40 * time.Minute).Unix(),
// 		},
// 		map[string]interface{}{
// 			"voucher_start_on": time.Now().Add(-35 * time.Minute).Unix(),
// 		},
// 		map[string]interface{}{
// 			"voucher_expired_on": time.Now().Add(-35 * time.Minute).Unix(),
// 		},
// 		map[string]interface{}{
// 			"voucher_template": map[string]interface{}{
// 				"type":          "sa",
// 				"inbox_message": "dsad",
// 			},
// 		},
// 		map[string]interface{}{
// 			"voucher_template": map[string]interface{}{
// 				"type":           "carousal",
// 				"title":          "xtitle",
// 				"subtitle":       "xsubstitie",
// 				"carousal_image": "dsnajk",
// 				"image_url":      []string{"dasda"},
// 				"buttons": []map[string]interface{}{
// 					map[string]interface{}{"title": "sda", "url": "dsad"},
// 				},
// 				"inbox_message": "dsad",
// 			},
// 		},
// 		map[string]interface{}{
// 			"survey": []map[string]interface{}{{"question": ""}},
// 		},
// 		map[string]interface{}{
// 			"survey": []map[string]interface{}{{"question": "da", "keyword": "random"}},
// 		},
// 		map[string]interface{}{
// 			"ended": true,
// 		},
// 	}
// 	input := []string{
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":false}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 		fmt.Sprintf(`{"voucher_template":{"type":"carousal","title":"vt title","subtitle":"vt subttite","image_url":["fsfas"],"action_url":"hthtps:?dasndj","inbox_message":"inbsdk asndcx"},"voucher_triggers":[{"type": "keyword", "condition": "#123"}],"photos":["348713432"],"voucher_sell_price":%v,"distribution_start_on":%v,"voucher_start_on":%v,"voucher_expired_on":%v,"start_time":%v,"end_time":%v,"voucher_count":%v,"voucher_total":%v,"title":"oldtitle","message":"oldmessage","language":"th","inbox_message":"old inbox message","reply_comment_message":"old reply comment message","voucher_expired_message":"old voucher expired message","second_reply_comment":"old second reply comment","comment_inbox_message":"old comment inbox message","details":"details","how_to_use":"how to use","ended":true}`, 1, time.Now().Add(10*time.Minute).Unix(), time.Now().Add(35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), time.Now().Add(-35*time.Minute).Unix(), time.Now().Add(4*time.Hour).Unix(), 1000, 2000),
// 	}

// 	expect := []string{
// 		"{\"code\":2302,\"error\": \"params not found\",\"success\":false}",
// 		"{\"code\":2304,\"message\": \"Campign updated\",\"success\":true}",
// 		"{\"code\":2302,\"error\": \"invalid end_time\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"invalid voucher_total\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"invalid voucher_sell_price\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"invalid distribution_start_on\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"invalid voucher_start_on\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"invalid voucher_expired_on\",\"success\":false}",
// 		"{\"code\":2304,\"message\": \"Campign updated\",\"success\":true}",
// 		"{\"code\":2304,\"message\": \"Campign updated\",\"success\":true}",
// 		"{\"code\":2302,\"error\": \"survey error\",\"success\":false}",
// 		"{\"code\":2302,\"error\": \"keyword error\",\"success\":false}",
// 		"{\"code\": 2307,\"message\": \"Campaign Stopped successfully\",\"success\":true}",
// 	}
// 	for i := range cases {
// 		fmt.Println("TestCase: ", i+1)
// 		reqByte, _ := json.Marshal(cases[i])
// 		vouchermockdata = input[i]
// 		reqReader := bytes.NewReader(reqByte)

// 		req, _ := http.NewRequest("POST", "http://localhost:8084/edit", reqReader)

// 		res, _ := c.Do(req)

// 		data, _ := ioutil.ReadAll(res.Body)

// 		assert.Equal(t, expect[i], string(data))

// 		res.Body.Close()
// 	}
// }
