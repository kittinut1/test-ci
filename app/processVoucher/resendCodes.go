package processVoucher

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (p *ProcessVoucher) ResendToken(ctx *fasthttp.RequestCtx) {
	vchrID, _ := ctx.UserValue("campaign-id").(string)
	selectedFields := bson.M{
		"_id":     1,
		"post_id": 1,
		"survey":  1,
	}
	vchr := new(model.Campaign)
	objID, _ := primitive.ObjectIDFromHex(vchrID)
	query := bson.M{
		"_id": objID,
	}
	err := p.Voucher.GetOne(query, selectedFields, vchr)

	if err != nil {
		logger.Info(err)
		if err == mongo.ErrNoDocuments {
			data := map[string]interface{}{
				"code":    "3101",
				"message": "campaign not found",
				"success": false,
			}
			bodyMessge, _ := json.Marshal(data)
			ctx.SetBody(bodyMessge)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		data := map[string]interface{}{
			"code":    "3101",
			"message": "internal server error",
			"success": false,
		}
		bodyMessge, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody(bodyMessge)
		return
	}
	if len(vchr.Survey) > 0 {
		data := map[string]interface{}{
			"code":    "3103",
			"message": "survey campaign",
			"success": false,
		}
		bodyMessge, _ := json.Marshal(data)
		ctx.SetBody(bodyMessge)
		return
	}
	selectedFields = bson.M{
		"_id":                     1,
		"history":                 1,
		"page_id":                 1,
		"post_id":                 1,
		"title":                   1,
		"inbox_message":           1,
		"reply_comment_message":   1,
		"voucher_expired_message": 1,
		"second_reply_comment":    1,
		"voucher_template":        1,
		"comment_inbox_message":   1,
		"language":                1,
		"index_data":              1,
		"voucher_src":             1,
		"voucher_list":            1,
		"survey":                  1,
	}
	query = bson.M{
		"post_id": vchr.PostID,
		"status": bson.M{
			"$ne": "resend",
		},
	}
	update := bson.M{
		"$set": bson.M{
			"status": "resend",
		},
	}

	err = p.Voucher.AtomicUpdate(query, update, selectedFields, vchr)
	if err != nil {
		logger.Info(err)
		data := map[string]interface{}{
			"code":    "3102",
			"message": "resend token failed",
			"success": false,
		}
		bodyMessge, _ := json.Marshal(data)
		ctx.SetBody(bodyMessge)
		return
	}

	data := map[string]interface{}{
		"code":    "3104",
		"message": "processing history",
		"success": true,
	}
	bodyMessge, _ := json.Marshal(data)
	ctx.SetBody(bodyMessge)
	go p.Resend(*vchr)
	return

}

func (p *ProcessVoucher) Resend(vchr model.Campaign) {
	res := p.AuthService.GetTokenFromAuthService(vchr.PageID)

	pageAccessToken, ok := res.Response["access_token"].(string)
	if !ok {
		logger.Info(fmt.Errorf("Token not available for page: %v", vchr.PageID))
		return
	}
	for hi := range vchr.History {
		b := new(bidRequest)
		b.BidderID = hi
		b.Name = vchr.History[hi].Name
		b.PostID = vchr.PostID
		b.PageID = vchr.PageID
		b.CommentID = vchr.History[hi].CommentID
		if vchr.History[b.BidderID].Error.Type == "sent" && vchr.History[b.BidderID].Error.Info != "" || vchr.History[b.BidderID].Error.Type == "not sent" {
			code := vchr.History[b.BidderID].CouponCode
			codeIndex := 0
			if vchr.History[b.BidderID].CouponCode == "" {
				code = vchr.VoucherList[0]
				for i := range vchr.IndexData {
					if b.BidderID == vchr.IndexData[i] {
						codeIndex = len(vchr.IndexData) - 1 - i
					}
				}
				if vchr.VoucherSrc == "embedded" {
					code = vchr.VoucherList[codeIndex]
				}
				if vchr.VoucherSrc == "single" {
					code = vchr.VoucherList[0]
				}
				if code == "" {
					continue
				}
				m := map[string]interface{}{}
				switch vchr.VoucherTemplate.Type {
				case "image_button":
					{
						buttons := []map[string]interface{}{}
						// buttons = append(buttons, p.FB.WebURLButton(config.VoucherResponse[vchr.Language]["11"], config.QrCodeURL+"id="+vchr.PostID+"&uid="+b.BidderID))
						for i := range vchr.VoucherTemplate.Buttons {
							buttons = append(buttons, p.FB.WebURLButton(vchr.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(vchr.VoucherTemplate.Buttons[i].Url, vchr.PostID, b.BidderID, b.PageID)))
						}
						m = p.FB.ImageButton(vchr.VoucherTemplate.ImageURL, buttons)
					}
				case "carousal":
					{
						buttons := []map[string]interface{}{}
						for i := range vchr.VoucherTemplate.Buttons {
							buttons = append(buttons, p.FB.WebURLButton(vchr.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(vchr.VoucherTemplate.Buttons[i].Url, vchr.PostID, b.BidderID, b.PageID)))
						}

						el := []map[string]interface{}{}
						for i := range vchr.VoucherTemplate.ImageURL {
							src := vchr.VoucherTemplate.ImageURL[i]
							// src := ""
							// s, err := p.Redis.Get(vchr.VoucherTemplate.ImageURL[i])
							// if s == nil || err != nil {
							// 	res := p.FB.GetImageFromPhotoID(pageAccessToken, vchr.VoucherTemplate.ImageURL[i])
							// 	src, _ = res.Response["src"].(string)
							// }
							// if s != nil {
							// 	src, _ = s.(string)
							// }

							el = append(el, p.FB.GenericElement(
								replaceVoucherResponseValues(vchr.VoucherTemplate.Title, code, "", vchr.InboxMessage, "", "", "", "", "", ""),
								src,
								replaceVoucherResponseValues(vchr.VoucherTemplate.Subtitle, code, "", vchr.InboxMessage, "", "", "", "", "", ""),
								"https://facebook.com/"+b.PostID,
								buttons))
						}
						// for i := range vchr.VoucherTemplate.ImageURL {
						// 	el = append(el, p.FB.GenericElement(
						// 		replaceVoucherResponseValues(vchr.VoucherTemplate.Title, code, "", vchr.InboxMessage, "", "", "", "", "", ""),
						// 		vchr.VoucherTemplate.ImageURL[i],
						// 		replaceVoucherResponseValues(vchr.VoucherTemplate.Subtitle, code, "", vchr.InboxMessage, "", "", "", "", "", ""),
						// 		"https://facebook.com/"+b.PostID,
						// 		buttons))
						// }
						m = p.FB.GenericTemplate(el, []map[string]interface{}{})
					}
				default:
					m = p.FB.Text(replaceVoucherResponseValues(config.VoucherResponse[vchr.Language]["2"], code, "", vchr.InboxMessage, vchr.VoucherExpiredMessage, vchr.ReplyCommentMessage, vchr.SecondReplyComment, vchr.CommentInboxMessage, b.Name, vchr.Title))
				}
				res = p.FB.SendPrivateMessage(b.CommentID, "RESPONSE", m, pageAccessToken)
				if res.Err != nil {
					msg := replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+config.VoucherResponse[vchr.Language]["7"], code, "", vchr.InboxMessage, vchr.VoucherExpiredMessage, vchr.ReplyCommentMessage, vchr.SecondReplyComment, vchr.CommentInboxMessage, b.Name, vchr.Title)
					if vchr.CommentInboxMessage == "" {
						msg = replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+config.VoucherResponse[vchr.Language]["2"], code, "", vchr.InboxMessage, vchr.VoucherExpiredMessage, vchr.ReplyCommentMessage, vchr.SecondReplyComment, vchr.CommentInboxMessage, b.Name, vchr.Title)
					}
					res = p.FB.ReplyToComment(b.CommentID, pageAccessToken, msg)
					if res.Err != nil {
						logger.Info(res.Err)
					}
				}

				update := bson.M{
					"$set": bson.M{
						"edited_on":                              time.Now().Unix(),
						"history." + b.BidderID + ".coupon_code": code,
						"history." + b.BidderID + ".error.type":  "sent",
						"history." + b.BidderID + ".error.info":  fmt.Sprintf("%+v", res),
					},
				}
				objID, _ := primitive.ObjectIDFromHex(vchr.ID)
				query := bson.M{
					"_id": objID,
				}
				err := p.Voucher.Update(query, update)
				// err := p.Voucher.Mongo.Session.DB("BotioUjung").C("Vouchers").UpdateId(vchr.ID, update)
				if err != nil {
					logger.Info(err)
				}
			}
		}
	}
}
