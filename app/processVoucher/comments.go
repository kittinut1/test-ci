package processVoucher

import (
	"botio-voucher/app/logger"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/valyala/fasthttp"
)

type bidRequest struct {
	Name      string  `json:"name"`
	AuctionID string  `json:"bid_post_id"`
	PageID    string  `json:"page_id"`
	PostID    string  `json:"post_id"`
	CommentID string  `json:"comment_id"`
	BidderID  string  `json:"psid"`
	Price     float64 `json:"price"`
	Message   string  `json:"message"`
	Timestamp int64   `json:"timestamp"`

	Action  string `json:"action"`
	Payload string `json:"payload"`
}

func (p *ProcessVoucher) WebhookHandler(ctx *fasthttp.RequestCtx) {
	response, _ := p.HandleComments2(ctx.PostBody())
	ctx.SetBodyString(response)
	return
}

func (p *ProcessVoucher) HandleComments2(data []byte) (string, int) {
	// defer func() {
	// 	if err := recover(); err != nil {
	// 		logger.Error(fmt.Errorf("%s %+v", err, "voucher handler crashed"))
	// 	}
	// }()

	b, err := p.checkBidRequestBody(data)
	if err != nil {
		logger.Info("Body Error", string(data))
		logger.Error(err)
		return err.Error(), 0
	}
	// logger.Info(string(data))
	if b.Action == "rtc" {
		return "", 0
	}

	if b.Action == "msg" {
		return p.ProcessMessage(b)
	}

	if b.Action == "add" {
		return p.ProcessComment(b)
	}

	if b.Action == "payload" {
		return "postback", p.ProcessPostback(b)
	}
	return "", 0
}

func (p *ProcessVoucher) PostComment(b *bidRequest, message, pageAccessToken string, hide bool) {
	res := p.FB.ReplyToComment(b.CommentID, pageAccessToken, message)
	if res.Err != nil {
		logger.Error(res.Err, fmt.Sprintf("Post comment error: %+v", b))
	}
}
func replaceVoucherResponseValues(text, voucherCode, voucherLeft, inboxmessage, voucherexpiredmessage, replycomment, secondreply, commentinbox, name, eventname string) string {
	text = strings.Replace(text, "{{comment_inbox_message}}", commentinbox, -1)
	text = strings.Replace(text, "{{second_reply_comment}}", secondreply, -1)
	text = strings.Replace(text, "{{voucher_left}}", voucherLeft, -1)
	text = strings.Replace(text, "{{inbox_message}}", inboxmessage, -1)
	text = strings.Replace(text, "{{reply_comment}}", replycomment, -1)
	text = strings.Replace(text, "{{voucher_expired_message}}", voucherexpiredmessage, -1)
	text = strings.Replace(text, "{{voucher_code}}", voucherCode, -1)
	text = strings.Replace(text, "{{event_name}}", eventname, -1)
	if name != "" {
		str := strings.SplitN(name, " ", 2)
		return strings.Replace(text, "{{first_name}}", str[0], -1)
	}
	return strings.Replace(text, "{{first_name}}", name, -1)
}

func (p *ProcessVoucher) checkBidRequestBody(body []byte) (*bidRequest, error) {
	we := strings.ReplaceAll(string(body), "\n", "")
	we = strings.TrimSpace(we)

	// body = bytes.ReplaceAll(body, empty, empty)
	reqData := new(bidRequest)
	err := json.Unmarshal([]byte(we), reqData)
	if err != nil {
		logger.Info(err, string(body))
		return nil, fmt.Errorf(`cannot read post parameters`)
	}
	if reqData.Action == "payload" || reqData.Action == "msg" {
		return reqData, err
	}
	if reqData.Message == "" || reqData.BidderID == "" || reqData.CommentID == "" || reqData.PageID == "" || reqData.PostID == "" || reqData.Timestamp < 0 {
		return reqData, fmt.Errorf(`params are missing`)
	}
	reqData.BidderID = strings.TrimSpace(reqData.BidderID)
	return reqData, err
}
