package processVoucher

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (p *ProcessVoucher) UploadPrivateKey(ctx *fasthttp.RequestCtx) {
	pageID := ctx.UserValue("page-id").(string)
	query := bson.M{
		"page_id": pageID,
	}
	selectedFields := bson.M{
		"payments": 1,
	}
	result := new(model.PageConfig)
	err := p.PageConfig.GetOne(query, selectedFields, result)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			ctx.SetBodyString(`{"code": 9000, "error": "Invalid PageID", "success": false }`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		ctx.SetBodyString(`{"code": 9001, "error": "file upload error", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	appID := string(ctx.FormValue("app_id"))
	if appID == "" {
		ctx.SetBodyString(`{"code": 9002, "error": "app_id not found", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	header, err := ctx.FormFile("file")
	if err != nil {
		ctx.SetBodyString(`{"code": 9003, "error": "file not found", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	path := fmt.Sprintf("%s", header.Filename)
	err = fasthttp.SaveMultipartFile(header, path)

	logger.Info(err, " H =>", header.Filename, " P => ", path)

	fileBytes, err := ioutil.ReadFile(path)
	if err != nil || len(fileBytes) == 0 {
		fmt.Println(len(fileBytes))
		ctx.SetBodyString(`{"code": 9006, "error": "file error", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	ciphertext := string(utils.Encrypt(fileBytes, config.KsherSecretKey))
	update := bson.M{
		"$set": bson.M{
			"payments": bson.M{
				"ksher": bson.M{
					"app_id":      appID,
					"private_key": ciphertext,
				},
			},
		},
	}
	logger.Info(string(fileBytes))
	err = p.PageConfig.Update(query, update)
	if err != nil {
		ctx.SetBodyString(`{"code": 9007, "error": "file upload error", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	ctx.SetBodyString(`{"code": 9008, "message": "file upload successful", "success": true }`)
	ctx.SetStatusCode(fasthttp.StatusOK)
	logger.Error(os.Remove(path))
	return

}

func (p *ProcessVoucher) UploadImageToMessage(ctx *fasthttp.RequestCtx) {
	// pageID := ctx.UserValue("page-id").(string)
	header, err := ctx.FormFile("file")
	if err != nil {
		ctx.SetBodyString(`{"code": 1201, "error": "file not found", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	path := fmt.Sprintf("/tmp/%s", header.Filename)
	err = fasthttp.SaveMultipartFile(header, path)
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"code": 1202, "error": "file error", "success": false }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	// res := p.AuthService.GetTokenFromAuthService(pageID)
	// pageAccessToken, ok := res.Response["access_token"].(string)
	// if !ok {
	// 	logger.Info(fmt.Errorf("Token not available for page: %v", pageID))
	// 	ctx.SetBodyString(`{"code": 1203, "error": "token error", "success": false }`)
	// 	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	// 	return
	// }

	// res = p.FB.AttachmentUploadAPI(path, pageAccessToken)
	// if res.Err != nil {
	// 	logger.Error(res.Err)
	// 	ctx.SetBodyString(`{"code": 1204, "error": "` + res.Err.Error() + `", "success": false }`)
	// 	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	// 	return
	// }
	res := p.Img.Upload(path)

	go os.Remove(path)
	// res.Response["photo_id"] = res.Response["attachment_id"]
	// delete(res.Response, "attachment_id")
	body := map[string]interface{}{
		"code":    1204,
		"message": "photo upload successful",
		"data":    res.Response,
		"success": true,
	}
	bodybytes, _ := json.Marshal(body)
	ctx.SetBody(bodybytes)
	ctx.SetStatusCode(fasthttp.StatusOK)
	return
}
