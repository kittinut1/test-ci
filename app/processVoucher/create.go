package processVoucher

import (
	"botio-voucher/app/authService"
	"botio-voucher/app/config"
	"botio-voucher/app/fbapis"
	"botio-voucher/app/imgbb"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/pageconfig"
	"botio-voucher/app/paymentgateway"
	"botio-voucher/app/storage"
	"botio-voucher/app/transaction"
	"botio-voucher/app/voucher"

	"encoding/json"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
)

type ProcessVoucher struct {
	Voucher        voucher.Voucherer
	PageConfig     pageconfig.PageConfiger
	Transaction    transaction.Transactioner
	AuthService    *authService.AuthService
	FB             fbapis.ExternalAPI
	PaymentGateway paymentgateway.PaymentGatewayer
	Redis          storage.Storager

	logger *log.Logger
	Img    *imgbb.Imgbb
	reg    map[string]*regexp.Regexp
}

func New(v *voucher.Voucher, p *pageconfig.PageConfig, t *transaction.Transaction, a *authService.AuthService, f fbapis.ExternalAPI, pk *paymentgateway.Ksher, r *storage.Redis) *ProcessVoucher {
	l := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)
	reg := map[string]*regexp.Regexp{}
	var err error
	for i := range config.SupportedRegex {
		reg[i], err = regexp.Compile(config.SupportedRegex[i])
		if err != nil {
			l.Println(err)
			os.Exit(1)
		}
	}

	return &ProcessVoucher{
		Voucher:        v,
		PageConfig:     p,
		Transaction:    t,
		AuthService:    a,
		Redis:          r,
		FB:             f,
		PaymentGateway: pk,
		Img:            imgbb.New(),
		logger:         l,
		reg:            reg,
	}
}

type voucherRequestBody struct {
	Title                 *string   `json:"title"`
	Message               *string   `json:"message"`
	Content               *string   `json:"content"`
	Keys                  *[]string `json:"keys"`
	TotalVoucher          *int      `json:"total_vouchers"`
	VoucherType           *string   `json:"voucher_type"`
	CouponExpireTime      *int64    `json:"coupon_expire_time"`
	VoucherSrc            *string   `json:"voucher_src"`
	StartTime             *int64    `json:"start_time"`
	EndTime               *int64    `json:"end_time"`
	Draft                 *bool     `json:"draft"`
	Photos                *[]string `json:"photos"`
	CouponsList           *[]string `json:"coupon_list"`
	Language              *string   `json:"language"`
	InboxMessage          *string   `json:"inbox_message"`
	ReplyCommentMessage   *string   `json:"reply_comment_message"`
	VoucherExpiredMessage *string   `json:"voucher_expired_message"`
	SecondReplyComment    *string   `json:"second_reply_comment"`
	CommentInboxMessage   *string   `json:"comment_inbox_message"`
}
type voucherRequestBodyDefault struct {
	Title                    string                `json:"title"`
	Message                  string                `json:"message"`
	Content                  string                `json:"content"`
	VoucherType              string                `json:"voucher_type"`
	Keys                     []string              `json:"keys"`
	BannedKeys               []string              `json:"banned_keys"`
	TotalVoucher             int                   `json:"total_vouchers"`
	VoucherSrc               string                `json:"voucher_src"`
	CouponExpireTime         int64                 `json:"coupon_expire_time"`
	StartTime                int64                 `json:"start_time"`
	EndTime                  int64                 `json:"end_time"`
	Draft                    bool                  `json:"draft"`
	Photos                   []string              `json:"photos"`
	Started                  bool                  `json:"started"`
	CouponsList              []string              `json:"coupon_list"`
	Language                 string                `json:"language"`
	Survey                   []model.Survey        `json:"survey"`
	InboxMessage             string                `json:"inbox_message"`
	ReplyCommentMessage      string                `json:"reply_comment_message"`
	VoucherExpiredMessage    string                `json:"voucher_expired_message"`
	SecondReplyComment       string                `json:"second_reply_comment"`
	CommentInboxMessage      string                `json:"comment_inbox_message"`
	VoucherTemplate          model.VoucherTemplate `json:"voucher_template"`
	VoucherSellPrice         float64               `json:"voucher_sell_price"`
	HowToUse                 string                `json:"how_to_use"`
	Details                  string                `json:"details"`
	VoucherRedeemedMessage   string                `json:"voucher_redeemed_message" bson:"voucher_redeemed_message"`
	EarlyDistributionMessage string                `json:"early_distribution_message" bson:"early_distribution_message"`
	CouponCodeExpiredMessage string                `json:"couponcode_expired_message" bson:"couponcode_expired_message"`
	VoucherRunOutOfMessage   string                `json:"voucher_run_out_of_message" bson:"voucher_run_out_of_message"`
}

func (p *ProcessVoucher) Create(ctx *fasthttp.RequestCtx) {
	userID := string(ctx.Request.Header.Peek("user-id"))
	// timestamp := time.Now().Unix()
	// userToken := string(ctx.Request.Header.Peek("AccessToken"))
	pageID, ok := ctx.UserValue("page-id").(string)
	if !ok {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":1000, Create Voucher Failed", "error":"page id is missing in url","success": false }`)
		return
	}
	v, err := checkVoucherRequestBody(ctx.PostBody())
	if err != nil {
		ctx.SetBodyString(`{"code": 1001, "message": "Create Voucher Failed", "error":"` + err.Error() + `" }`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	logger.Info(string(ctx.PostBody()))
	logger.Info(v.VoucherSellPrice)
	// query := bson.M{
	// "user_id": userID,
	// "page_id": pageID,
	// }
	// pc := new(model.PageConfig)
	// err = p.PageConfig.GetOne(query, bson.M{}, pc)
	// if err != nil && err != mongo.ErrNoDocuments {
	// 	ctx.SetBodyString(`{"code": 1001, "message": "Create Voucher Failed", "error":"` + err.Error() + ` has ended" }`)
	// 	return
	// }
	// if err != mongo.ErrNoDocuments && pc.SubscriptionEnd < time.Now().Unix() {
	// 	ctx.SetBodyString(`{"code": 1001, "message": "Create Voucher Failed", "error":"` + pc.SubscriptionType + ` has ended" }`)
	// 	return
	// }
	query := bson.M{
		"page_id": pageID,
	}
	pageConfig := new(model.PageConfig)
	err = p.PageConfig.GetOne(query, bson.M{}, pageConfig)
	if err != nil {
		logger.Error(err)
		ctx.SetStatusCode(500)
		ctx.SetBodyString(`{"code": 1011, "message": "Create Voucher Failed", "error":"invalid page", "success": false }`)
		return
	}
	if pageConfig.SubscriptionEnd < time.Now().Unix() {
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1012, "message": "Create Voucher Failed", "error":"subscription expired", "success": false }`)
		return
	}
	if v.VoucherType == "web" && v.TotalVoucher <= 0 {
		v.TotalVoucher = (pageConfig.SubscriptionVoucherTotal - pageConfig.SubscriptionVoucherCount)
	}

	if (pageConfig.SubscriptionVoucherTotal - pageConfig.SubscriptionVoucherCount) == 0 {
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1013, "message": "Create Voucher Failed", "error":"All Vouchers are used. Please buy."` + fmt.Sprintf("%v", (pageConfig.SubscriptionVoucherTotal-pageConfig.SubscriptionVoucherCount)) + ` ", "success": false }`)
		return
	}
	if (pageConfig.SubscriptionVoucherTotal - pageConfig.SubscriptionVoucherCount) < v.TotalVoucher {
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1014, "message": "Create Voucher Failed", "error":"Maximum vouchers ` + fmt.Sprintf("%v", (pageConfig.SubscriptionVoucherTotal-pageConfig.SubscriptionVoucherCount)) + ` ", "success": false }`)
		return
	}
	if v.VoucherSellPrice > 0 && pageConfig.Payments.Ksher.AppID == "" {
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1015, "message": "Create Voucher Failed", "error":"please setup payment", "success": false }`)
		return
	}
	if pageConfig.ConsentMessage == "" {
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1015, "message": "Create Voucher Failed", "error":"please setup consent message", "success": false }`)
		return
	}
	res := p.AuthService.GetTokenFromAuthService(pageID)
	pageAccessToken, ok := res.Response["access_token"].(string)
	if !ok {
		ctx.SetStatusCode(500)
		ctx.SetBodyString(`{"code": 1002, "message": "Create Voucher Failed", "error":"unknown error" }`)
		return
	}
	res = p.FB.SubscribeWebhook(pageID, pageAccessToken)
	if res.Err != nil {
		data := map[string]interface{}{
			"code":    1003,
			"message": "Voucher Creation Failed",
			"error":   "unknown error",
			"data":    res.Response,
			"success": false,
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody(bodyMessage)
		return
	}
	post := map[string]interface{}{
		"message":   v.Content,
		"published": false,
	}

	res = p.FB.CreatePost(pageID, pageAccessToken, post)
	if res.Err != nil {
		data := map[string]interface{}{
			"code":    1004,
			"message": "Voucher Creation Failed",
			"error":   "unknown error",
			"data":    res.Response,
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody(bodyMessage)
		return
	}
	postID, ok := res.Response["id"].(string)
	if !ok {
		go removePost(p, pageAccessToken, postID)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBodyString(`{ "code":1005, "message":"Voucher Creation Failed", "error":"unknown error"}`)
		return
	}
	attachedMedia := []map[string]interface{}{}
	if len(v.Photos) != 0 {
		for i := range v.Photos {
			attachedMedia = append(attachedMedia, map[string]interface{}{"media_fbid": (v.Photos)[i]})
		}
		post["attached_media"] = attachedMedia
	}

	res = p.FB.UpdatePost2(postID, pageAccessToken, map[string]interface{}{"attached_media": attachedMedia})
	if res.Err != nil {
		go removePost(p, pageAccessToken, postID)
		data := map[string]interface{}{
			"code":    1006,
			"message": "Voucher Creation Failed",
			"error":   "images expired",
			"data":    res.Response,
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBody(bodyMessage)
		return
	}

	id, err := p.Voucher.Create(userID, pageID, postID, v.Title, v.Message, v.InboxMessage, v.ReplyCommentMessage, v.SecondReplyComment, v.VoucherExpiredMessage, v.CommentInboxMessage, v.Language, v.VoucherSrc, v.VoucherType, v.HowToUse, v.Details, v.VoucherRedeemedMessage, v.EarlyDistributionMessage, v.CouponCodeExpiredMessage, v.VoucherRunOutOfMessage, v.TotalVoucher, v.Keys, v.BannedKeys, v.CouponsList, v.Photos, v.Survey, v.CouponExpireTime, v.StartTime, v.EndTime, v.VoucherSellPrice, v.Started, v.Draft, v.VoucherTemplate)
	if err != nil {
		go removePost(p, pageAccessToken, postID)
		ctx.SetBodyString(`{"code": 1007, "message": "Create Voucher Failed", "error":"unknown error" }`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	if v.Draft {
		data := map[string]interface{}{
			"code": 1008,
			"data": map[string]interface{}{
				"id": id,
			},
			"message": "Voucher Saved Successfully",
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusOK)
		ctx.SetBody(bodyMessage)
		return
	}
	query = bson.M{
		"page_id": pageID,
		"subscription_voucher_count": bson.M{
			"$lte": pageConfig.SubscriptionVoucherTotal,
		},
		"subscription_end": bson.M{
			"$gte": time.Now().Unix(),
		},
	}
	update := bson.M{
		"$inc": bson.M{
			"subscription_voucher_count": v.TotalVoucher,
			"campaign_total":             1,
		},
		"$set": bson.M{
			"edited_on": time.Now().Unix(),
		},
	}
	err = p.PageConfig.AtomicUpdate(query, update, bson.M{}, pageConfig, true)
	if err != nil {
		go removePost(p, pageAccessToken, postID)
		logger.Error(err)
		ctx.SetStatusCode(400)
		ctx.SetBodyString(`{"code": 1015, "message": "Create Voucher Failed", "error":"internal server error", "success": false }`)
		return
	}
	if v.Started {
		res = p.FB.PublishPost(postID, pageAccessToken)
	} else {
		res = p.FB.UpdatePost2(postID, pageAccessToken, map[string]interface{}{"scheduled_publish_time": v.StartTime})
	}
	success, ok := res.Response["success"].(bool)
	if res.Err != nil || !ok || !success {
		go removePost(p, pageAccessToken, postID)
		p.logger.Println(res.Err, "Publishing or Scheduling Error: ", res.Response)
		data := map[string]interface{}{
			"code":    1009,
			"message": "Voucher Creation Failed",
			"error":   "unknown error",
			"data":    res.Response,
		}
		bodyMessage, err := json.Marshal(data)
		if err != nil {
			p.logger.Println(err, "json.Marshal error: ", data)
		}
		ctx.SetBody(bodyMessage)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	data := map[string]interface{}{
		"code":    1010,
		"message": "Voucher Creation Successful",
		"data": map[string]interface{}{
			"id": id,
		},
		"success": true,
	}
	bodyMessage, err := json.Marshal(data)
	if err != nil {
		p.logger.Println(err, "json.Marshal error: ", data)
	}

	ctx.SetBody(bodyMessage)
	ctx.SetStatusCode(fasthttp.StatusOK)
	return
}

func checkVoucherRequestBody(body []byte) (*voucherRequestBodyDefault, error) {
	vBody := &voucherRequestBody{}
	err := json.Unmarshal(body, vBody)
	if err != nil {
		return nil, fmt.Errorf("post params not found")
	}
	vBodyDefault := &voucherRequestBodyDefault{}
	json.Unmarshal(body, vBodyDefault)
	if vBody.Draft == nil {
		vBodyDefault.Draft = false
	}
	if vBodyDefault.Draft == true {
		return nil, fmt.Errorf("Draft not allowed")
	}
	if vBody.Content == nil {
		return nil, fmt.Errorf("content is required")
	}
	if vBody.Language == nil {
		vBodyDefault.Language = "th"
	}
	if !vBodyDefault.Draft {
		if vBody.Title == nil {
			return nil, fmt.Errorf("title is required")
		}
		if vBody.Message == nil {
			return nil, fmt.Errorf("message is required")
		}
		if vBody.VoucherSrc == nil {
			return nil, fmt.Errorf("voucher_src is required")
		}
		if vBody.VoucherType == nil {
			return nil, fmt.Errorf("voucher_type is required")
		}
		// if vBody.CouponInitial == nil {
		// 	return nil, fmt.Errorf("coupon_initial is required")
		// }
		if vBody.Keys == nil {
			return nil, fmt.Errorf("keys is required")
		}
		if vBody.StartTime == nil {
			return nil, fmt.Errorf("start_time is required")
		}
		if vBody.CommentInboxMessage == nil {
			return nil, fmt.Errorf("comment_inbox_message is required")
		}
		if vBody.TotalVoucher == nil {
			return nil, fmt.Errorf("total_vouchers is required")
		}
		if vBody.CouponsList == nil {
			return nil, fmt.Errorf("coupon_list is required")
		}
		if vBody.Photos == nil {
			return nil, fmt.Errorf("photos is required")
		}
		if vBody.VoucherExpiredMessage == nil {
			return nil, fmt.Errorf("voucher_expired_message is required")
		}
		if vBody.InboxMessage == nil {
			return nil, fmt.Errorf("inbox_message is required")
		}
		if vBody.SecondReplyComment == nil {
			return nil, fmt.Errorf("second_reply_comment is required")
		}
		if vBody.ReplyCommentMessage == nil {
			return nil, fmt.Errorf("reply_comment_message is empty")
		}

		if len(vBodyDefault.Photos) == 0 {
			return nil, fmt.Errorf("aleast one image")
		}
		if vBodyDefault.StartTime < time.Now().Unix() {
			return nil, fmt.Errorf("start_time is invalid")
			// vBodyDefault.StartTime = time.Now().Add(1 * time.Minute).Unix()
		}
		if vBody.Title == nil {
			return nil, fmt.Errorf("empty title")
		}
		if vBodyDefault.Message == "" {
			return nil, fmt.Errorf("empty message")
		}
		if vBodyDefault.CommentInboxMessage == "" {
			return nil, fmt.Errorf("empty comment_inbox_message")
		}
		if len(vBodyDefault.Keys) == 0 {
			return nil, fmt.Errorf("atleast 1 key")
		}
		if vBodyDefault.VoucherSrc == "" {
			return nil, fmt.Errorf("empty voucher_src")
		}
		if vBodyDefault.VoucherType == "" {
			return nil, fmt.Errorf("empty voucher_type")
		}

		if vBodyDefault.VoucherSrc != "" {
			vBodyDefault.VoucherSrc = strings.ToLower(vBodyDefault.VoucherSrc)
			done := false
			for i := range config.SupportedVoucherSrc {
				if config.SupportedVoucherSrc[i] == vBodyDefault.VoucherSrc {
					done = true
				}
			}
			if !done {
				return nil, fmt.Errorf("unsupported voucher_src")
			}
		}

		if vBodyDefault.VoucherSrc == "embedded" {
			// if vBodyDefault.TotalVoucher > config.MaximumVouchers {
			// 	return nil, fmt.Errorf("Csv file contains %v, Maximum VoucherCodes allowed are %v", vBodyDefault.TotalVoucher, config.MaximumVouchers)
			// }
			if len(vBodyDefault.CouponsList) == 0 {
				return nil, fmt.Errorf("coupon_list cannot be empty")
			}
			if vBodyDefault.VoucherType != "" {
				vBodyDefault.VoucherType = strings.ToLower(vBodyDefault.VoucherType)
				done := false
				for i := range config.SupportedVoucherType {
					if config.SupportedVoucherType[i] == vBodyDefault.VoucherType {
						done = true
					}
				}
				if !done {
					return nil, fmt.Errorf("unsupported voucher_type")
				}
				if vBodyDefault.VoucherType == "instant" {
					vBodyDefault.TotalVoucher = len(vBodyDefault.CouponsList)
				} else {
					vBodyDefault.CouponsList = []string{}
				}
			}
		}
		if vBodyDefault.VoucherSrc == "single" {
			vBodyDefault.CouponsList = append([]string{}, vBodyDefault.CouponsList[0])
			// if vBodyDefault.TotalVoucher > config.MaximumVouchers {
			// 	return nil, fmt.Errorf("Maximum VoucherCodes allowed are", vBodyDefault.TotalVoucher, config.MaximumVouchers)
			// }
			if vBodyDefault.TotalVoucher == 0 {
				return nil, fmt.Errorf("Voucher Codes cannot be zero")
			}
			vBodyDefault.VoucherType = "instant"
		}

		if vBodyDefault.VoucherType == "web" {
			if vBodyDefault.CouponExpireTime < time.Unix(vBodyDefault.StartTime, 0).Add(30).Unix() {
				fmt.Println(vBodyDefault.CouponExpireTime, time.Unix(vBodyDefault.StartTime, 0).Add(30*time.Minute).Unix())
				return nil, fmt.Errorf("coupon_expire_time atleast 30 minutes more than start_time")
			}
			//generate codes
			vBodyDefault.CouponsList = generateUniueCouponCodes(vBodyDefault.TotalVoucher)
		}

		if vBodyDefault.Language != "th" {
			done := false
			vBodyDefault.Language = strings.TrimSpace(strings.ToLower(vBodyDefault.Language))
			for i := range config.SupportedLanguage {
				if config.SupportedLanguage[i] == vBodyDefault.Language {
					done = true
				}
			}
			if !done {
				return nil, fmt.Errorf("unsupported language")
			}
		}
		if vBodyDefault.VoucherExpiredMessage == "" {
			return nil, fmt.Errorf("voucher_expired_message is empty")
		}
		if vBodyDefault.InboxMessage == "" {
			return nil, fmt.Errorf("inbox_message is empty")
		}
		if vBodyDefault.SecondReplyComment == "" {
			return nil, fmt.Errorf("second_reply_comment is empty")
			// vBodyDefault.SecondReplyComment = config.VoucherResponse[strings.ToLower(vBodyDefault.Language)]["6"]
		}
		if vBodyDefault.ReplyCommentMessage == "" {
			return nil, fmt.Errorf("reply_comment_message is empty")
		}
		if vBodyDefault.TotalVoucher == 0 {
			return nil, fmt.Errorf("atleast one voucher")
		}
		if vBodyDefault.VoucherType == "instant" && len(vBodyDefault.CouponsList) == 0 {
			return nil, fmt.Errorf("%d coupon code are needed", vBodyDefault.TotalVoucher)
		}
		if vBodyDefault.StartTime > time.Now().Add(10*time.Minute).Unix() {
			vBodyDefault.Started = false
		} else {
			vBodyDefault.Started = true
		}

		if len(vBodyDefault.Survey) > 0 {
		}

		if vBodyDefault.VoucherTemplate.Type == "carousal" {
			if vBodyDefault.VoucherTemplate.Title == "" {
				return nil, fmt.Errorf("%v is missing", "carousal title ")
			}
			if vBodyDefault.VoucherTemplate.Subtitle == "" {
				return nil, fmt.Errorf("%v is missing", "carousal subtitle")
			}
			if len(vBodyDefault.VoucherTemplate.ImageURL) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal image_url")
			}
			if len(vBodyDefault.VoucherTemplate.Buttons) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal buttons")
			}
			for i := range vBodyDefault.VoucherTemplate.Buttons {
				if vBodyDefault.VoucherTemplate.Buttons[i].Title == "" {
					return nil, fmt.Errorf("%v is missing", "carousal button title")
				}
				if vBodyDefault.VoucherTemplate.Buttons[i].Url == "" {
					return nil, fmt.Errorf("%v is missing", "carousal button url")
				}
			}
			vBodyDefault.VoucherTemplate.InboxMessage = vBodyDefault.InboxMessage
		} else if vBodyDefault.VoucherTemplate.Type == "image_button" {
			if len(vBodyDefault.VoucherTemplate.ImageURL) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal image_url")
			}
		} else {
			vBodyDefault.VoucherTemplate = model.VoucherTemplate{
				Type:         "text",
				InboxMessage: vBodyDefault.InboxMessage,
			}
		}
	}
	vBodyDefault.EndTime = time.Now().Add(5 * 12 * 30 * 24 * time.Hour).Unix()

	return vBodyDefault, err
}

func removePost(p *ProcessVoucher, pageAccessToken, postID string) {
	res := p.FB.DeletePost(pageAccessToken, postID)
	p.logger.Println(res)
	if res.Err != nil {
		p.logger.Println(res.Err, "Remove post failed: ", postID)
	}
}

func removeVoucherFromDB(p *ProcessVoucher, id string) {

}

func generateUniueCouponCodes(l int) []string {
	list := []string{}
	for i := 0; i < l; i++ {
		list = append(list, uuid.New().String())
	}
	return list
}
