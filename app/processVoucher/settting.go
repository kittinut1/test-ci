package processVoucher

import (
	"botio-voucher/app/logger"
	"encoding/json"
	"fmt"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type pageConfigSetting struct {
	RedemptionPassword *map[string]string `json:"redemption_password" bson:"redeemed_password"`
	ConsentMessage     *string            `json:"consent_message" bson:"consent_message"`
}

func (p *ProcessVoucher) Setting(ctx *fasthttp.RequestCtx) {
	pageID, _ := ctx.UserValue("page-id").(string)
	updatebson, err := checkSetting(ctx.PostBody())
	if err != nil {
		ctx.SetBodyString(`{"code": 8500, "message": "` + err.Error() + `", "success": false}`)
		ctx.SetStatusCode(400)
		return
	}

	query := bson.M{
		"page_id": pageID,
	}
	update := bson.M{
		"$set": updatebson,
	}
	err = p.PageConfig.Update(query, update)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			ctx.SetBodyString(`{"code": 8501, "message": "Page config not found", "success": false}`)
			ctx.SetStatusCode(400)
			return
		}
		ctx.SetBodyString(`{"code": 8502, "message": "Page config not found", "success": false}`)
		ctx.SetStatusCode(500)
		return
	}
	ctx.SetBodyString(`{"code": 8503, "message": "Page config updated", "success": true}`)
	ctx.SetStatusCode(200)
	return
}

func checkSetting(data []byte) (b bson.M, err error) {
	p := new(pageConfigSetting)
	err = json.Unmarshal(data, p)
	if err != nil {
		logger.Error(err)
		return nil, fmt.Errorf("params not found")
	}

	b = bson.M{}
	if p.ConsentMessage != nil && *p.ConsentMessage != "" {
		b["consent_message"] = *p.ConsentMessage
	}
	if p.RedemptionPassword != nil {
		rp := *p.RedemptionPassword
		if len(rp) > 0 {
			for i := range rp {
				if rp[i] == "" {
					return nil, fmt.Errorf("label not found")
				}
			}
			b["redeemed_password"] = rp
		}
	}
	if len(b) == 0 {
		err = fmt.Errorf("params not found")
		logger.Error(err, string(data))
		return nil, err
	}
	return b, err
}
