package processVoucher

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type inventory struct {
	CouponCode string    `json:"coupon_code"`
	PSID       string    `json:"psid"`
	Name       string    `json:"name"`
	Time       time.Time `json:"time"`
	Log        string    `json:"log"`
	Duplicate  bool      `json:"duplicate"`
}
type pageconfigresponse struct {
	ID                       string                  `json:"_id" bson:"_id"`
	PageID                   string                  `json:"page_id" bson:"page_id"`
	RedeemedAt               map[string]string       `json:"redeemed_at" bson:"redeemed_at"`
	ConsentMessage           string                  `json:"consent_message" bson:"consent_message"`
	Payments                 model.PageConfigPayment `json:"payments" bson:"payments"`
	CampaignTotal            int                     `json:"campaign_total" bson:"campaign_total"`
	VouchersTotal            int                     `json:"vouchers_total" bson:"vouchers_total"`
	SubscriptionVoucherCount int                     `json:"subscription_voucher_count" bson:"subscription_voucher_count"`
	SubscriptionVoucherTotal int                     `json:"subscription_voucher_total" bson:"subscription_voucher_total"`
	SubscriptionType         string                  `json:"subscription_type" bson:"subscription_type"`
	SubscriptionStart        int64                   `json:"subscription_start" bson:"subscription_start"`
	SubscriptionEnd          int64                   `json:"subscription_end" bson:"subscription_end"`
}
type voucherresposne struct {
	ID              string          `json:"_id"  bson:"_id"`
	Title           string          `json:"title"  bson:"title"`
	PostID          string          `json:"post_id"  bson:"post_id"`
	CreatedOn       int64           `json:"created_on"  bson:"created_on"`
	Message         string          `json:"message"  bson:"message"`
	Language        string          `json:"language"  bson:"language"`
	Started         bool            `json:"started"  bson:"started"`
	Ended           bool            `json:"ended"  bson:"ended"`
	VoucherTotal    int             `json:"voucher_total"  bson:"voucher_total"`
	VoucherCount    int             `json:"voucher_count"  bson:"voucher_count"`
	VoucherType     string          `json:"voucher_type" bson:"voucher_type"`
	VoucherSrc      string          `json:"voucher_src" bson:"voucher_src"`
	VoucherTriggers []model.Trigger `json:"voucher_triggers" bson:"voucher_triggers"`
}

func (p *ProcessVoucher) ListCampaign(ctx *fasthttp.RequestCtx) {
	// userID := string(ctx.Request.Header.Peek("user-id"))
	pageID := ctx.UserValue("page-id").(string)
	query := map[string]interface{}{
		"page_id": pageID,
	}
	logger.Info("QUERY: ", query)
	selected := bson.M{
		"_id":              1,
		"title":            1,
		"post_id":          1,
		"message":          1,
		"language":         1,
		"voucher_total":    1,
		"voucher_template": 1,
		"voucher_count":    1,
		"started":          1,
		"ended":            1,
		"voucher_src":      1,
		"voucher_type":     1,
		"voucher_triggers": 1,
		"created_on":       1,
	}

	result := new([]voucherresposne)
	err := p.Voucher.GetAll(query, selected, result, 0, 0)
	if err != nil {
		if mongo.ErrNoDocuments == err {
			data := map[string]interface{}{
				"code":    2000,
				"message": "Getting All Vouchers Failed",
				"error":   "not found",
				"success": false,
			}
			bodyMessge, _ := json.Marshal(data)
			ctx.SetBody(bodyMessge)
			return
		}
		p.logger.Println(err, "pageid: ", pageID)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code": 2001, "message":"Getting All Auctions Failed", "error":"internal server error","success": false }`)
		return
	}
	logger.Info("RESULT", result, result == nil)
	data := map[string]interface{}{
		"code":    2002,
		"data":    result,
		"success": true,
	}
	if len(*result) == 0 {
		data["data"] = []map[string]interface{}{}
	}

	pageconfig := new(pageconfigresponse)
	err = p.PageConfig.GetOne(query, bson.M{}, pageconfig)
	if err != nil {
		logger.Info(err)
	}
	data["config"] = pageconfig
	bodyMessge, _ := json.Marshal(data)
	ctx.SetBody(bodyMessge)
}

func (p *ProcessVoucher) AllPageConfigs(ctx *fasthttp.RequestCtx) {
	userID := string(ctx.Request.Header.Peek("user-id"))

	res := p.AuthService.GetTokenFromAuthService(userID)
	accessToken, ok := res.Response["access_token"].(string)
	if !ok {
		logger.Info("AccessToken: ", res)
		data := map[string]interface{}{
			"code":    7001,
			"message": "Pages failed",
			"success": false,
			"error":   "token not found",
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody(bodyMessage)
	}
	// accessToken := string(ctx.Request.Header.Peek("access-token"))
	res = p.FB.GetPages2(userID, accessToken)
	if res.Err != nil {
		logger.Info(res.Response)
		data := map[string]interface{}{
			"code":    7001,
			"message": "Pages failed",
			"success": false,
			"error":   res.Response,
		}
		bodyMessage, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusForbidden)
		ctx.SetBody(bodyMessage)
		return
	}
	fbPages := res.Response["data"].([]interface{})
	list := []string{}
	for i := range fbPages {
		temp := fbPages[i].(map[string]interface{})
		list = append(list, temp["id"].(string))
	}

	query := map[string]interface{}{
		"page_id": bson.M{
			"$in": list,
		},
	}
	pageconfig := []pageconfigresponse{}
	err := p.PageConfig.GetAll(query, bson.M{}, &pageconfig, 0, 0)
	if err != nil {
		logger.Info(err)
	}
	// logger.Info("CONFIG: ", pageconfig)
	// res := p.FB.GetPages2(userID, accessToken)
	// if res.Err != nil {
	// 	logger.Info(res.Response)
	// 	data := map[string]interface{}{
	// 		"code":    7001,
	// 		"message": "Pages failed",
	// 		"success": false,
	// 		"error":   res.Response,
	// 	}
	// 	bodyMessage, _ := json.Marshal(data)
	// 	ctx.SetStatusCode(fasthttp.StatusForbidden)
	// 	ctx.SetBody(bodyMessage)
	// 	return
	// }

	resultPages := []map[string]interface{}{}
	for i := range fbPages {
		temp := fbPages[i].(map[string]interface{})
		err = p.Redis.Set(temp["id"].(string), temp["access_token"])
		logger.Info("REDIS SET ERROR: ", err)
		a := map[string]interface{}{
			"page_id":   temp["id"],
			"page_name": temp["name"],
			"picture":   temp["picture"],
		}
		for j := range pageconfig {
			id := a["page_id"].(string)
			if pageconfig[j].PageID == id {
				pageconfig[j].Payments.Ksher.PrivateKey = ""
				a["config"] = pageconfig[j]
			}
		}
		resultPages = append(resultPages, a)
	}

	data := map[string]interface{}{
		"code":    7002,
		"success": true,
		"pages":   resultPages,
	}
	body, _ := json.Marshal(data)

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(body)
	return

}

// page=&limit=&from=&name=&voucher_code=
// from = giventoday,givenyesterday,redeemtoday,redeemyesterday,all
// search =
func (p *ProcessVoucher) PublicList(ctx *fasthttp.RequestCtx) {
	pageID := ctx.UserValue("page-id").(string)
	// page := string(ctx.QueryArgs().Peek("page"))
	// limit := string(ctx.QueryArgs().Peek("limit"))
	from := string(ctx.QueryArgs().Peek("from"))
	h, _ := strconv.Atoi(from)
	t := int64(h)

	logger.Info("FROM := ", from, h, t)
	query := bson.M{
		"page_id": pageID,
		"edited_on": bson.M{
			"$gt": t,
		},
	}
	selectedfields := bson.M{
		"history":            1,
		"voucher_type":       1,
		"voucher_sell_price": 1,
		"photos":             1,
		"voucher_template":   1,
	}

	result := []model.Campaign{}
	err := p.Voucher.GetAll(query, selectedfields, &result, 0, 0)
	if err != nil {
		data := map[string]interface{}{
			"code":    2101,
			"message": "vouhcer list",
			"data":    []string{},
			"success": true,
		}
		body, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusOK)
		ctx.SetBody(body)
		return
	}
	list := []map[string]interface{}{}
	photosmap := []map[string]interface{}{}

	for i := range result {
		photosmap = []map[string]interface{}{}
		for k := range result[i].VoucherTemplate.ImageURL {

			photosmap = append(photosmap, map[string]interface{}{
				"src": result[i].VoucherTemplate.ImageURL[k],
			})
		}
		for j := range result[i].History {
			if result[i].History[j].RecievedOn > t || result[i].History[j].Timestamp > t {
				list = append(list, map[string]interface{}{
					"voucher_type":        result[i].VoucherType,
					"voucher_sell_price":  result[i].VoucherSellPrice,
					"photos":              photosmap,
					"id":                  result[i].History[j].VoucherID,
					"redeem_code":         result[i].History[j].RedeemCode,
					"name":                result[i].History[j].Name,
					"comment_id":          result[i].History[j].CommentID,
					"answers":             result[i].History[j].Answers,
					"coupon_code":         result[i].History[j].CouponCode,
					"redeemed":            result[i].History[j].Redeemed,
					"recieved_on":         result[i].History[j].RecievedOn,
					"used_on":             result[i].History[j].UsedOn,
					"psid":                result[i].History[j].PSID,
					"message":             result[i].History[j].Message,
					"timestamp":           result[i].History[j].Timestamp,
					"status":              result[i].History[j].Status,
					"expired_reservation": result[i].History[j].ExpiredReservation,
					"error": map[string]interface{}{
						"type": result[i].History[j].Error.Info,
						"info": result[i].History[j].Error.Type,
					},
				})
			}
		}
	}
	data := map[string]interface{}{
		"code":    2102,
		"message": "vouhcer list",
		"data":    list,
		"success": false,
	}
	body, _ := json.Marshal(data)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(body)
	return
}
