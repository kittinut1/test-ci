package processVoucher

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"
	"fmt"
	"time"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type redeemBody struct {
	ID   string `json:"id"`
	UID  string `json:"uid"`
	Code string `json:"code"`
}

func (p *ProcessVoucher) RedeemVoucher(ctx *fasthttp.RequestCtx) {
	redeemBody := new(redeemBody)
	err := json.Unmarshal(ctx.PostBody(), redeemBody)
	if err != nil || redeemBody.ID == "" || redeemBody.UID == "" {

		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":8000,"message":"Redeem Voucher Failed","success":false}`)
		return
	}
	query := bson.M{
		"post_id":                   redeemBody.ID,
		"history." + redeemBody.UID: bson.M{"$exists": true},
		// "voucher_password":          redeemBody.Code,
		"voucher_type": "web",
	}
	selectedfield := bson.M{
		"history." + redeemBody.UID: 1,
		"voucher_expired_on":        1,
		"page_id":                   1,
		"voucher_start_on":          1,
	}
	vchr := new(model.Campaign)
	err = p.Voucher.GetOne(query, selectedfield, vchr)
	if err != nil {
		logger.Info("Mongo GetOne Error:", err)
		ctx.SetBodyString(`{"code":8001,"message":"Redeem Voucher Failed","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	if vchr.History[redeemBody.UID].Redeemed {
		ctx.SetBodyString(`{"code":8002,"message":"Voucher already redeemed", "data":{"code": "` + vchr.History[redeemBody.UID].CouponCode + `"},"success":true}`)
		ctx.SetStatusCode(fasthttp.StatusOK)
		return
	}
	if vchr.RedemptionStartOn > time.Now().Unix() {
		ctx.SetBodyString(`{"code":9001,"message":"Redemption is not started yet","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	pc := new(model.PageConfig)
	err = p.PageConfig.GetOne(bson.M{"page_id": vchr.PageID}, bson.M{}, pc)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			ctx.SetBodyString(`{"code":9001,"message":"page config not found","success":false}`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		ctx.SetBodyString(`{"code":9001,"message":"internal server error","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	if len(pc.RedemptionPassword) > 0 {
		found := false
		for i := range pc.RedemptionPassword {
			if i == redeemBody.Code {
				found = true
			}
		}
		if !found {
			ctx.SetBodyString(`{"code":9001,"message":"password is wrong","success":false}`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
	}

	if vchr.RedemptionExpiredOn < time.Now().Unix() {
		ctx.SetBodyString(`{"code":8003,"message":"Voucher Expired","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusOK)
		return
	}
	update := bson.M{
		"$set": bson.M{
			"history." + redeemBody.UID + ".redeem_code": redeemBody.Code,
			"history." + redeemBody.UID + ".used_on":     time.Now().Unix(),
			"history." + redeemBody.UID + ".redeemed":    true,
			"edited_on": time.Now().Unix(),
		},
	}
	err = p.Voucher.Update(query, update)
	if err != nil {
		logger.Info("Mongo Update Error:", err)
		ctx.SetBodyString(`{"code":8004,"message":"Redeem Voucher Failed","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	ctx.SetBodyString(`{"code":8005,"message":"Redeem Voucher Successful","data":{"code": "` + vchr.History[redeemBody.UID].CouponCode + `"},"success":true}`)
	ctx.SetStatusCode(fasthttp.StatusOK)
	if vchr.VoucherRedeemedMessage == "" {
		vchr.VoucherRedeemedMessage = config.VoucherResponse[vchr.Language]["BC1-3"]
	}
	res := p.AuthService.GetTokenFromAuthService(vchr.PageID)
	pageAccessToken, ok := res.Response["access_token"].(string)
	if !ok {
		logger.Info(fmt.Errorf("Token not available for page: %v", vchr.PageID))
		return
	}
	m := map[string]interface{}{
		"messaging_type": "MESSAGE_TAG",
		"tag":            "POST_PURCHASE_UPDATE",
		"recipient": map[string]interface{}{
			"id": redeemBody.UID,
		},
		"message": p.FB.Text(vchr.VoucherRedeemedMessage),
	}
	res = p.FB.SendMessage(pageAccessToken, m)
	logger.Info(res)
	return
}

func (p *ProcessVoucher) Info(ctx *fasthttp.RequestCtx) {
	id := ctx.UserValue("campaign-id").(string)
	uid := ctx.UserValue("voucher-id").(string)
	query := bson.M{
		"post_id":      id,
		"voucher_type": "web",
	}
	selectedfield := bson.M{
		"_id":                1,
		"photos":             1,
		"page_id":            1,
		"title":              1,
		"message":            1,
		"content":            1,
		"inbox_message":      1,
		"voucher_expired_on": 1,
		"voucher_start_on":   1,
		"language":           1,
		"voucher_template":   1,
		"history." + uid:     1,
		"details":            1,
		"how_to_use":         1,
		"voucher_sell_price": 1,
	}
	vchr := new(model.Campaign)
	err := p.Voucher.GetOne(query, selectedfield, vchr)
	if err != nil {
		logger.Info("Mongo GetONe Error:", err)
		ctx.SetBodyString(`{"code":9001,"message":"Campaign not found","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	logger.Info("DOC", fmt.Sprintf("%+v", vchr))
	// res := p.AuthService.GetTokenFromAuthService(vchr.PageID)
	// if res.Err != nil {
	// 	logger.Info(res.Err, fmt.Sprintf("Token not available for page: %+v", b))
	// 	return "Token not available for page: " + b.PageID, 2
	// }
	// pageAccessToken, ok := res.Response["access_token"].(string)
	// if !ok {
	// 	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	// 	ctx.SetBodyString(`{"code":9001,"message":"Campaign not found","success":false}`)
	// 	return
	// }
	imagedata := []map[string]interface{}{}
	for i := range vchr.VoucherTemplate.ImageURL {
		if vchr.VoucherTemplate.ImageURL[i] == "1038212093245529" {
			imagedata = append(imagedata, map[string]interface{}{
				"src": "https://i.imgur.com/yOCZ1IB.jpg",
			})
			continue
		}
		// res := p.FB.GetImageFromPhotoID(pageAccessToken, vchr.VoucherTemplate.ImageURL[i])
		// if res.Err != nil {
		// 	ctx.SetStatusCode(fasthttp.StatusBadRequest)
		// 	ctx.SetBodyString(`{"code":9001,"message":"Campaign not found","success":false}`)
		// 	return
		// }
		imagedata = append(imagedata, map[string]interface{}{
			"src": vchr.VoucherTemplate.ImageURL[i],
		})
	}

	if vchr.VoucherSellPrice > 0 {
		query = bson.M{
			"user_id": uid,
			"post_id": id,
			"page_id": vchr.PageID,
		}
		t := new(model.Transaction)
		err = p.Transaction.GetOne(query, bson.M{}, t)
		if err != nil {
			ctx.SetBodyString(`{"code":9001,"message":"internal server error","success":false}`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		if t.Data.Status == "completed" {
			ctx.SetBodyString(`{"code":9001,"message":"paid","success":true}`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		response := map[string]interface{}{
			"code": 9002,
			"data": map[string]interface{}{
				"title":              vchr.Title,
				"how_to_use":         vchr.HowToUse,
				"details":            vchr.Details,
				"voucher_template":   vchr.VoucherTemplate,
				"language":           vchr.Language,
				"images":             imagedata,
				"redeemed":           vchr.History[uid].Redeemed,
				"voucher_sell_price": vchr.VoucherSellPrice,
				"qrcode":             t.Data.QrCode,
				"voucher_expired_on": vchr.RedemptionExpiredOn,
				"voucher_start_on":   vchr.RedemptionStartOn,
			},
			"message": "Campaign found",
			"success": true,
		}
		body, _ := json.Marshal(response)
		ctx.SetBody(body)
		ctx.SetStatusCode(fasthttp.StatusOK)
		return
	}

	pc := new(model.PageConfig)
	err = p.PageConfig.GetOne(bson.M{"page_id": vchr.PageID}, bson.M{}, pc)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			ctx.SetBodyString(`{"code":9001,"message":"page config not found","success":false}`)
			ctx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}
		ctx.SetBodyString(`{"code":9001,"message":"internal server error","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}
	logger.Info("Ninja: ", uid, "=> ", vchr.History[uid])
	cc := vchr.History[uid].CouponCode
	response := map[string]interface{}{
		"code": 9002,
		"data": map[string]interface{}{
			"title":              vchr.Title,
			"how_to_use":         vchr.HowToUse,
			"details":            vchr.Details,
			"voucher_start_on":   vchr.RedemptionStartOn,
			"voucher_expired_on": vchr.RedemptionExpiredOn,
			"voucher_template":   vchr.VoucherTemplate,
			"language":           vchr.Language,
			"images":             imagedata,
			"coupon_code":        cc,
			"redeemed":           vchr.History[uid].Redeemed,
			"has_password":       len(pc.RedemptionPassword) != 0,
		},
		"message": "Campaign found",
		"success": true,
	}
	body, _ := json.Marshal(response)
	ctx.SetBody(body)
	ctx.SetStatusCode(fasthttp.StatusOK)
	return
}
