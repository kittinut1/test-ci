package processVoucher

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (p *ProcessVoucher) GetCampaign(ctx *fasthttp.RequestCtx) {
	vid := ctx.UserValue("campaign-id").(string)
	objID, _ := primitive.ObjectIDFromHex(vid)
	query := bson.M{
		"_id": objID,
	}
	selectedfield := bson.M{
		"_id":              1,
		"title":            1,
		"post_id":          1,
		"message":          1,
		"language":         1,
		"voucher_total":    1,
		"voucher_template": 1,
		"voucher_count":    1,
		"started":          1,
		"ended":            1,
		"voucher_src":      1,
		"voucher_type":     1,
		"voucher_triggers": 1,
		"created_on":       1,
	}
	result := new(model.Campaign)
	err := p.Voucher.GetOne(query, selectedfield, result)
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"code":3301,"error":"campaign not found","success":false`)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	data := map[string]interface{}{
		"code":    3302,
		"data":    result,
		"success": true,
	}
	bodybytes, _ := json.Marshal(data)
	ctx.SetBody(bodybytes)
}

func (p *ProcessVoucher) GetPageConfig(ctx *fasthttp.RequestCtx) {
	pageID := ctx.UserValue("page-id").(string)
	query := bson.M{
		"page_id": pageID,
	}
	pc := new(model.PageConfig)
	err := p.PageConfig.GetOne(query, bson.M{}, pc)
	if err != nil {
		logger.Error(err)
		ctx.SetBodyString(`{"code":3501,"error":"page config not found","success":false`)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	pc.Payments.Ksher.PrivateKey = ""
	data := map[string]interface{}{
		"code":    3502,
		"data":    pc,
		"success": true,
	}
	bodybytes, _ := json.Marshal(data)
	ctx.SetBody(bodybytes)
}

func (p *ProcessVoucher) GetKsherInfo(ctx *fasthttp.RequestCtx) {
	pageID := ctx.UserValue("page-id").(string)
	query := bson.M{
		"post_id": pageID,
	}
	selectedfields := bson.M{
		"payments": 1,
	}
	result := new(model.PageConfig)
	err := p.PageConfig.GetOne(query, selectedfields, result)
	if err != nil {
		ctx.SetBodyString(`{"code": 4101,"error":"payment info not found","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	if result.Payments.Ksher.AppID == "" {
		ctx.SetBodyString(`{"code": 4102,"error":"payment info not found","success":false}`)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	data := map[string]interface{}{
		"data": map[string]interface{}{
			"ksher": map[string]interface{}{
				"app_id": result.Payments.Ksher.AppID,
			},
		},
		"code":    4103,
		"success": true,
	}
	bodybytes, _ := json.Marshal(data)
	ctx.SetBody(bodybytes)
}
