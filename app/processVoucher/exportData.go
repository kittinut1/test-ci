package processVoucher

import (
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/valyala/fasthttp"
)

type resfree struct {
	History map[string]freeresponse `json:"history"  bson:"history"`
}
type respremium struct {
	History map[string]premiumresponse `json:"history"  bson:"history"`
}
type freeresponse struct {
	Name       string             `json:"name"  bson:"name"`
	CouponCode string             `json:"coupon_code"  bson:"coupon_code"`
	RecievedOn int64              `json:"recieved_on"  bson:"recieved_on"`
	Error      model.VoucherError `json:"error"  bson:"error"`
}
type premiumresponse struct {
	Name       string             `json:"name"  bson:"name"`
	CouponCode string             `json:"coupon_code"  bson:"coupon_code"`
	RecievedOn int64              `json:"recieved_on"  bson:"recieved_on"`
	UsedOn     int64              `json:"used_on"  bson:"used_on"`
	Error      model.VoucherError `json:"error"  bson:"error"`
	CommentID  string             `json:"comment_id"  bson:"comment_id"`
	Answers    []string           `json:"answers" bson:"answers"`
	PSID       string             `json:"psid"  bson:"psid"`
	Message    string             `json:"message"  bson:"message"`
	Timestamp  int64              `json:"timestamp"  bson:"timestamp"`
	Status     string             `json:"status"  bson:"status"`
}

func (p *ProcessVoucher) ExportData(ctx *fasthttp.RequestCtx) {
	// userID := string(ctx.Request.Header.Peek("user-id"))
	pageID, _ := ctx.UserValue("campaign-id").(string)

	vchrID, ok := ctx.UserValue("campaign-id").(string)
	if !ok {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":2900, "message": "End Voucher Failed", "error":"voucher-id is missing in url","success": false }`)
		return
	}

	query := bson.M{
		"page_id": pageID,
	}
	selectedFields := bson.M{
		"subscription_type": 1,
	}
	pc := new(model.PageConfig)

	err := p.PageConfig.GetOne(query, selectedFields, pc)
	if err != nil {
		logger.Info(err)
		pc.SubscriptionType = "free"
	}

	selectedFields = bson.M{
		"history": 1,
	}
	logger.Info(vchrID)
	objID, _ := primitive.ObjectIDFromHex(vchrID)
	query = bson.M{
		"_id": objID,
	}

	rp := new(respremium)

	err = p.Voucher.GetOne(query, selectedFields, rp)
	if err != nil {
		logger.Info(err)
		if err == mongo.ErrNoDocuments {
			data := map[string]interface{}{
				"code":    "2901",
				"message": "export data failed",
				"error":   "not found",
				"success": false,
			}
			bodyMessge, _ := json.Marshal(data)
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)
			ctx.SetBody(bodyMessge)
			return
		}
		data := map[string]interface{}{
			"code":    "2901",
			"message": "export data failed",
			"error":   "internal server error",
			"success": false,
		}
		bodyMessge, _ := json.Marshal(data)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody(bodyMessge)
		return
	}
	data := map[string]interface{}{
		"code":    "2902",
		"success": true,
	}
	history := map[int]interface{}{}
	var k int = 0
	for i := range rp.History {
		history[k] = rp.History[i]
		k = k + 1
	}
	data["data"] = map[string]interface{}{
		"history": history,
	}
	bodyMessge, _ := json.Marshal(data)
	ctx.SetBody(bodyMessge)
}
