package processVoucher

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/voucher"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (p *ProcessVoucher) ProcessComment(b *bidRequest) (string, int) {
	timestamp := time.Now().Unix()
	vRedis := new(voucher.VoucherInfo)
	err := vRedis.Get(b.PostID, p.Redis)
	if err != nil {
		logger.Info("PostID not found in redis.", err)
		err = p.Voucher.GetOne(bson.M{"post_id": b.PostID}, bson.M{}, vRedis)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return "PostID not found in db", 1
			}
			return "db error retry", 2
		}
		vRedis.Set(b.PostID, p.Redis)
	}
	b.Message = strings.ToLower(b.Message)
	done := false
	strs := strings.SplitN(strings.ToLower(b.Message), " ", -1)
	for k := range strs {
		strs[0] = strings.TrimSpace(strs[k])
		for i := range vRedis.VoucherTriggers {
			if len(strs[k]) == len(vRedis.VoucherTriggers[i].Condition) && strings.ToLower(strs[k]) == strings.ToLower(vRedis.VoucherTriggers[i].Condition) {
				done = true
			}
		}
	}

	if !done {
		logger.Info(fmt.Sprintf("Voucher Commnet: %+v %v", b, "trigger key not found"))
		return "trigger key not found", 1
	}
	res := p.AuthService.GetTokenFromAuthService(b.PageID)
	pageAccessToken, ok := res.Response["access_token"].(string)
	if !ok {
		logger.Info(fmt.Errorf("Token not available for page: %v", b.PageID))
		return "Token not available for page: " + b.PageID, 2
	}
	if vRedis.Ended || vRedis.EndTime < time.Now().Unix() {
		if vRedis.VoucherExpiredMessage == "" {
			vRedis.VoucherExpiredMessage = config.VoucherResponse[vRedis.Language]["BC3-2-1"]
		}
		p.PostComment(b, replaceVoucherResponseValues(vRedis.VoucherExpiredMessage, "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
		err = p.Voucher.Update(bson.M{"post_id": b.PostID}, bson.M{"$set": bson.M{"ended": true}})
		if err != nil {
			logger.Error(err)
		}
		return "Voucher distribution is ended", 1
	}

	if vRedis.DistributionStartOn > time.Now().Unix() {
		if vRedis.EarlyDistributionMessage == "" {
			vRedis.EarlyDistributionMessage = config.VoucherResponse[vRedis.Language]["BC3-2-4"]
		}
		p.PostComment(b, vRedis.EarlyDistributionMessage, pageAccessToken, false)
		return "Voucher distribution is not started", 1
	}

	query := bson.M{
		"post_id":               b.PostID,
		"history." + b.BidderID: bson.M{"$exists": false},
		"voucher_count": bson.M{
			"$lt":  vRedis.VoucherTotal,
			"$gte": 0,
		},
		"ended": false,
		"voucher_expired_on": bson.M{
			"$gt": timestamp,
		},
		"end_time": bson.M{
			"$gt": timestamp,
		},
	}
	update := bson.M{
		"$set": bson.M{
			"edited_on": timestamp,
			"history." + b.BidderID: bson.M{
				"id":          uuid.New().String(),
				"name":        b.Name,
				"comment_id":  b.CommentID,
				"redeemed":    false,
				"recieved_on": timestamp,
				"used_on":     0,
				"psid":        b.BidderID,
				"message":     b.Message,
				"timestamp":   b.Timestamp,
				"status":      b.Action,
				"error": bson.M{
					"type": "not sent",
					"info": "",
				},
			},
		},
		"$push": bson.M{
			"index_data": bson.M{
				"$each":     []string{b.BidderID},
				"$position": 0,
			},
		},
		"$inc": bson.M{
			"voucher_count": 1,
		},
	}

	selectedFields := bson.M{
		"_id":           1,
		"voucher_count": 1,
		"voucher_list": bson.M{
			"$slice": []interface{}{0, 1},
		},
	}
	vchr := new(model.Campaign)
	err = p.Voucher.AtomicUpdate(query, update, selectedFields, vchr)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			logger.Info("NOT HERE")
			p.CommentFailed(vchr, vRedis, b, pageAccessToken)
			return "existing", 1
		}
		return "retry", 2
	}
	str, i := p.CommentPassed(vchr, vRedis, b, pageAccessToken)
	logger.Info("CommentPassed: ", str, i)
	return str, i
}
func (p *ProcessVoucher) CommentPassed(vchr *model.Campaign, vRedis *voucher.VoucherInfo, b *bidRequest, pageAccessToken string) (string, int) {
	timestamp := time.Now().Unix()
	codeIndex := vchr.VoucherCount
	objID, _ := primitive.ObjectIDFromHex(vchr.ID)
	query := bson.M{
		"_id": objID,
	}
	var err error
	if vRedis.VoucherSrc == "embedded" {
		selectedFields := bson.M{
			"_id": 1,
			"voucher_list": bson.M{
				"$slice": []interface{}{codeIndex, 1},
			},
		}
		err = p.Voucher.GetOne(query, selectedFields, vchr)
		if err != nil {
			logger.Info(err)
			return "retry", 2
		}
	}
	if len(vchr.VoucherList) == 0 {
		return "coupon code empty", 2
	}
	code := vchr.VoucherList[0]

	paymentOrderID := ""
	if vRedis.VoucherSellPrice > 0 {
		paymentOrderID, err = p.SellVoucher(vchr, vRedis, b, codeIndex, code, pageAccessToken)
		if err != nil {
			logger.Error(err)
			return "payment failed", 2
		}
	}

	m := map[string]interface{}{}

	pcr := new(model.PageConfig)
	q2 := bson.M{
		"page_id": b.PageID,
		"subscribers": bson.M{
			"$in": []string{b.BidderID},
		},
	}
	err = p.PageConfig.GetOne(q2, bson.M{}, pcr)
	if err != nil || len(vRedis.Survey) > 0 {
		err = p.PageConfig.GetOne(bson.M{"page_id": b.PageID}, bson.M{}, pcr)
		if err != nil {
			logger.Error(err)
			return "page-config not found", 1
		}
		uv := &voucher.UserVoucher{
			TransactionID:     paymentOrderID,
			VoucherType:       vRedis.VoucherType,
			Name:              b.Name,
			VoucherSellPrice:  vRedis.VoucherSellPrice,
			PostID:            b.PostID,
			PSID:              b.BidderID,
			State:             -1,
			Answers:           []string{},
			Survey:            vRedis.Survey,
			UserID:            b.BidderID,
			CouponCode:        code,
			InboxMessage:      vRedis.InboxMessage,
			Language:          vRedis.Language,
			ExpireReservation: time.Unix(timestamp, 0).Add(config.VoucherReservationTime).Unix(),
			VoucherTemplate:   vRedis.VoucherTemplate,
			Photos:            vRedis.Photos,
		}
		err := uv.Set(b.BidderID+"_"+b.PostID, p.Redis)
		if err != nil {
			logger.Error(err)
			return "redis save failed", 2
		}
		u := &voucher.UserState{
			UserID: b.BidderID,
			PostsInfo: map[string]string{
				b.PageID: b.PostID,
			},
		}
		err = u.Set(b.BidderID, p.Redis)
		if err != nil {
			logger.Error(err)
			return "redis save failed", 2
		}
		if pcr.ConsentMessage == "" {
			pcr.ConsentMessage = config.VoucherResponse[vRedis.Language]["BC1-2"]
		}
		buttons := []map[string]interface{}{
			p.FB.WebURLButton(config.VoucherResponse[vRedis.Language]["BC5-1"], config.VoucherResponse[vRedis.Language]["BC5-1-1"]),
		}
		qr := []map[string]interface{}{
			p.FB.QuickReply(config.VoucherResponse[vRedis.Language]["BC1-2-1"], fmt.Sprintf("%s_%s:%s", b.BidderID, b.PostID, "consentok")),
		}
		m = p.FB.TextButton(replaceVoucherResponseValues(pcr.ConsentMessage, "", "", "", "", "", "", "", "", vRedis.Title), buttons, qr)

	} else if paymentOrderID != "" {
		buttons := []map[string]interface{}{
			p.FB.WebURLButton("Pay "+fmt.Sprintf("%v", vRedis.VoucherSellPrice), "https://rocket-voucher.botio.io/"+"payment?id="+b.PostID+"&uid="+b.BidderID),
		}
		el := []map[string]interface{}{}
		for i := range vRedis.VoucherTemplate.ImageURL {
			el = append(el, p.FB.GenericElement(
				fmt.Sprintf("%s %v", vRedis.VoucherTemplate.Title, vRedis.VoucherSellPrice),
				vRedis.VoucherTemplate.ImageURL[i],
				"OrderID: "+paymentOrderID+"/npowered by Rocket Vouhcer",
				"https://facebook.com/"+b.PostID,
				buttons))
		}
		m = p.FB.GenericTemplate(el, []map[string]interface{}{})
	} else {
		switch vRedis.VoucherTemplate.Type {
		case "image_button":
			{
				buttons := []map[string]interface{}{}
				// if vRedis.VoucherType == "web" {

				// 	buttons = append(buttons, p.FB.WebURLButton("กดแลกรับสิทธิ์", "https://rocket-voucher.botio.io/voucher?id="+b.PostID+"&uid="+b.BidderID))
				// } else {
				for i := range vRedis.VoucherTemplate.Buttons {
					buttons = append(buttons, p.FB.WebURLButton(vRedis.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(vRedis.VoucherTemplate.Buttons[i].Url, vRedis.PostID, b.BidderID, b.PageID)))
				}
				// }
				m = p.FB.ImageButton(vRedis.VoucherTemplate.ImageURL, buttons)
			}
		case "carousal":
			{
				buttons := []map[string]interface{}{}
				// buttons = append(buttons, p.FB.WebURLButton("QrCode URL", config.QrCodeURL+"id="+b.PostID+"&uid="+b.BidderID+"&code="+code))
				for i := range vRedis.VoucherTemplate.Buttons {
					buttons = append(buttons, p.FB.WebURLButton(vRedis.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(vRedis.VoucherTemplate.Buttons[i].Url, vRedis.PostID, b.BidderID, b.PageID)))
				}

				el := []map[string]interface{}{}
				for i := range vRedis.VoucherTemplate.ImageURL {
					src := vRedis.VoucherTemplate.ImageURL[i]
					el = append(el, p.FB.GenericElement(
						replaceVoucherResponseValues(vRedis.VoucherTemplate.Title, code, "", vRedis.InboxMessage, "", "", "", "", "", ""),
						src,
						replaceVoucherResponseValues(vRedis.VoucherTemplate.Subtitle, code, "", vRedis.InboxMessage, "", "", "", "", "", ""),
						replaceIDanfUID(vRedis.VoucherTemplate.Buttons[i].Url, vRedis.PostID, b.BidderID, b.PageID),
						buttons))
				}
				m = p.FB.GenericTemplate(el, []map[string]interface{}{})
			}
		default:
			m = p.FB.Text(replaceVoucherResponseValues(config.VoucherResponse[vRedis.Language]["2"], code, "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title))
		}
	}
	res := p.FB.SendPrivateMessage(b.CommentID, "RESPONSE", m, pageAccessToken)
	if res.Err != nil {
		// still figure it out.
		st := "#" + strconv.Itoa(codeIndex+1)
		if vRedis.VoucherType == "web" {
			for i := range vRedis.VoucherTemplate.Buttons {
				st = st + "\n" + vRedis.VoucherTemplate.Buttons[i].Title + "\n" + replaceIDanfUID(vRedis.VoucherTemplate.Buttons[i].Url, vRedis.PostID, b.BidderID, b.PageID) + "/n"
			}
		}
		if vRedis.CommentInboxMessage == "" {
			vRedis.CommentInboxMessage = config.VoucherResponse[vRedis.Language]["BC4-1"]
		}
		st = replaceVoucherResponseValues(st+" "+vRedis.CommentInboxMessage, code, "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title)
		res = p.FB.ReplyToComment(b.CommentID, pageAccessToken, st)
		if res.Err != nil {
			logger.Info(res.Err)
			return "comment and message both failed", 2
		}
	} else {
		if vRedis.ReplyCommentMessage == "" {
			vRedis.ReplyCommentMessage = config.VoucherResponse[vRedis.Language]["BC1-1"]
		}
		p.PostComment(b, replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+vRedis.ReplyCommentMessage, "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
	}

	update := bson.M{
		"$set": bson.M{
			"edited_on":                              timestamp,
			"history." + b.BidderID + ".coupon_code": code,
			"history." + b.BidderID + ".error.type":  "sent",
			"history." + b.BidderID + ".error.info":  fmt.Sprintf("%+v", res),
		},
	}

	err = p.Voucher.Update(query, update)
	if err != nil {
		logger.Info(err)
	}
	return "done", 1
}

func (p *ProcessVoucher) CommentFailed(vchr *model.Campaign, vRedis *voucher.VoucherInfo, b *bidRequest, pageAccessToken string) {
	// timestamp := time.Now().Unix()
	query := bson.M{
		"post_id": b.PostID,
	}
	s2 := bson.M{
		"history." + b.BidderID: 1,
		"end_time":              1,
		"voucher_total":         1,
		"voucher_count":         1,
		"index_data":            1,
		"ended":                 1,
		"voucher_list":          1,
		"voucher_expired_on":    1,
	}
	err := p.Voucher.GetOne(query, s2, vchr)
	if err != nil {
		logger.Error(err)
		if err == mongo.ErrNoDocuments {
			return
		}
		//retry
		return
	}

	// if _, ok := vchr.History[b.BidderID]; ok {
	// 	if vchr.History[b.BidderID].Error.Type == "sent" && vchr.History[b.BidderID].Error.Info != "" || vchr.History[b.BidderID].Error.Type == "not sent" {
	// 		logger.Info("Here")
	// 		code := vchr.History[b.BidderID].CouponCode
	// 		codeIndex := 0
	// 		if vchr.History[b.BidderID].CouponCode == "" {
	// 			if len(vchr.VoucherList) == 0 {
	// 				return
	// 			}
	// 			code = vchr.VoucherList[0]
	// 			for i := range vchr.IndexData {
	// 				if b.BidderID == vchr.IndexData[i] {
	// 					codeIndex = len(vchr.IndexData) - 1 - i
	// 				}
	// 			}
	// 			if vRedis.VoucherSrc == "embedded" {
	// 				code = vchr.VoucherList[codeIndex]
	// 			}
	// 		}
	// 		if len(vRedis.Survey) > 0 {
	// 			p.SurveyVoucher(vchr, vRedis, b, codeIndex, code, pageAccessToken)
	// 			return
	// 		}
	// 		res := p.FB.SendPrivateMessage(b.CommentID, "RESPONSE", p.FB.Text(replaceVoucherResponseValues(config.VoucherResponse[vRedis.Language]["2"], code, "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title)), pageAccessToken)
	// 		if res.Err != nil {
	// 			msg := replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+config.VoucherResponse[vRedis.Language]["7"], code, "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title)
	// 			if vchr.CommentInboxMessage == "" {
	// 				msg = replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+config.VoucherResponse[vRedis.Language]["2"], code, "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title)
	// 			}
	// 			res = p.FB.ReplyToComment(b.CommentID, pageAccessToken, msg)
	// 			if res.Err != nil {
	// 				logger.Info(res.Err)
	// 			}
	// 		} else {
	// 			p.PostComment(b, replaceVoucherResponseValues("#"+strconv.Itoa(codeIndex+1)+" "+config.VoucherResponse[vRedis.Language]["1"], "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
	// 		}
	// 		query := bson.M{
	// 			"post_id":               b.PostID,
	// 			"history." + b.BidderID: bson.M{"$exists": true},
	// 		}
	// 		update := bson.M{
	// 			"$set": bson.M{
	// 				"edited_on":                              timestamp,
	// 				"history." + b.BidderID + ".coupon_code": code,
	// 				"history." + b.BidderID + ".error.type":  "sent",
	// 				"history." + b.BidderID + ".error.info":  fmt.Sprintf("%+v", res),
	// 			},
	// 		}
	// 		err = p.Voucher.Update(query, update)
	// 		if err != nil {
	// 			logger.Info(err)
	// 		}
	// 		return
	// 	}
	// 	if vRedis.SecondReplyComment == "" {
	// 		vRedis.SecondReplyComment = config.VoucherResponse[vRedis.Language]["BC3-1-2"]
	// 	}
	// 	p.PostComment(b, replaceVoucherResponseValues(vRedis.SecondReplyComment, "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
	// 	return
	// }

	if vchr.Ended || vchr.VoucherTotal == vchr.VoucherCount {
		if vRedis.VoucherExpiredMessage == "" {
			vRedis.VoucherExpiredMessage = config.VoucherResponse[vRedis.Language]["BC3-2-1"]
		}
		p.PostComment(b, replaceVoucherResponseValues(vRedis.VoucherExpiredMessage, "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
		return
	}
	if vchr.VoucherTotal == vchr.VoucherCount {
		if vRedis.VoucherRunOutOfMessage == "" {
			vRedis.VoucherRunOutOfMessage = config.VoucherResponse[vRedis.Language]["BC3-1-1"]
		}
		p.PostComment(b, replaceVoucherResponseValues(vRedis.VoucherRunOutOfMessage, "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
		return
	}

	p.PostComment(b, replaceVoucherResponseValues(config.VoucherResponse[vRedis.Language]["5"], "", "", vRedis.InboxMessage, vRedis.VoucherExpiredMessage, vRedis.ReplyCommentMessage, vRedis.SecondReplyComment, vRedis.CommentInboxMessage, b.Name, vRedis.Title), pageAccessToken, false)
	return
}

func (p *ProcessVoucher) SellVoucher(vchr *model.Campaign, vRedis *voucher.VoucherInfo, b *bidRequest, codeIndex int, code, pageAccessToken string) (orderID string, err error) {
	timestamp := time.Now().Unix()
	query := bson.M{
		"page_id": b.PageID,
	}
	selectedFields := bson.M{
		"payments": 1,
	}
	pageConfig := new(model.PageConfig)
	err = p.PageConfig.GetOne(query, selectedFields, pageConfig)
	if err != nil {
		logger.Error(err)
		return orderID, err
	}

	logger.Info("payment: ", fmt.Sprintf("%+v", pageConfig))
	merchantOrderID := uuid.New().String()
	currency := config.KsherFeeType
	channel := config.KsherUsedChannel
	kresult, err := p.PaymentGateway.PaymentRequest(merchantOrderID, channel, currency, vRedis.Title, pageConfig.Payments.Ksher.AppID, pageConfig.Payments.Ksher.PrivateKey, code+":"+b.PostID+":"+b.BidderID, vRedis.VoucherSellPrice*100)
	if err != nil {
		logger.Error(err)
		return orderID, err
	}
	t := &model.Transaction{
		UserID:    b.BidderID,
		PageID:    b.PageID,
		PostID:    b.PostID,
		CreatedOn: timestamp,
		EditedOn:  timestamp,
		Payment: model.TransactionPayment{
			Service:            "ksher",
			Currency:           currency,
			Channel:            channel,
			TotalFee:           fmt.Sprintf("%v", vRedis.VoucherSellPrice),
			Reference:          kresult["nonce_str"],
			OrderNumber:        merchantOrderID,
			ServiceOrderNumber: kresult["ksher_order_no"],
		},
		History: []model.TransactionStatus{
			{
				Status:    kresult["result"],
				Timestamp: kresult["time_stamp"],
			},
		},
		Data: model.TransactionData{
			Type:   "sell",
			QrCode: kresult["imgdat"],
		},
	}
	orderID, err = p.Transaction.Create(t)
	return

}

func replaceIDanfUID(src, id, uid, pageid string) string {
	src = strings.ReplaceAll(src, "{{id}}", id)
	src = strings.ReplaceAll(src, "{{pageid}}", pageid)
	return strings.ReplaceAll(src, "{{uid}}", uid)
}
