package processVoucher

import (
	"botio-voucher/app/fbapis"
	"botio-voucher/app/logger"
	"botio-voucher/app/model"
	"botio-voucher/app/voucher"
	"encoding/json"
	"fmt"
	"time"

	"github.com/valyala/fasthttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type editRequest struct {
	Title                 *string                `json:"title" bson:"title"`
	Message               *string                `json:"message" bson:"message"`
	Language              *string                `json:"language" bson:"language"`
	EndTime               *int64                 `json:"end_time"  bson:"end_time"`
	VoucherTotal          *int                   `json:"voucher_total"  bson:"voucher_total"`
	VoucherSellPrice      *float64               `json:"voucher_sell_price" bson:"voucher_sell_price"`
	DistributionStartOn   *int64                 `json:"distribution_start_on" bson:"distribution_start_on"`
	VoucherStartOn        *int64                 `json:"voucher_start_on" bson:"voucher_start_on"`
	VoucherExpiredOn      *int64                 `json:"voucher_expired_on" bson:"voucher_expired_on"`
	Survey                *[]model.Survey        `json:"survey" bson:"survey"`
	InboxMessage          *string                `json:"inbox_message" bson:"inbox_message"`
	ReplyCommentMessage   *string                `json:"reply_comment_message" bson:"reply_comment_message"`
	VoucherExpiredMessage *string                `json:"voucher_expired_message" bson:"voucher_expired_message"`
	SecondReplyComment    *string                `json:"second_reply_comment" bson:"second_reply_comment"`
	CommentInboxMessage   *string                `json:"comment_inbox_message" bson:"comment_inbox_message"`
	VoucherTemplate       *model.VoucherTemplate `json:"voucher_template" bson:"voucher_template"`
	Details               *string                `json:"details" bson:"details"`
	HowToUse              *string                `json:"how_to_use" bson:"how_to_use"`
	VoucherTriggers       *[]model.Trigger       `json:"voucher_triggers" bson:"voucher_triggers"`
	Content               *string                `json:"content"`              // requires fb update
	Photos                *[]string              `json:"photos" bson:"photos"` // requires fb update
	Ended                 *bool                  `json:"ended" bson:"ended"`
	// StartTime             int64                  `json:"start_time"  bson:"start_time"`
}

func (p *ProcessVoucher) EditCampaign(ctx *fasthttp.RequestCtx) {

	vid := ctx.UserValue("campaign-id").(string)
	objID, _ := primitive.ObjectIDFromHex(vid)
	query := bson.M{
		"_id": objID,
	}
	selectedField := bson.M{
		"title":                   1,
		"post_id":                 1,
		"message":                 1,
		"language":                1,
		"photos":                  1,
		"start_time":              1,
		"end_time":                1,
		"voucher_total":           1,
		"voucher_count":           1,
		"voucher_sell_price":      1,
		"distribution_start_on":   1,
		"voucher_start_on":        1,
		"voucher_expired_on":      1,
		"voucher_triggers":        1,
		"survey":                  1,
		"inbox_message":           1,
		"reply_comment_message":   1,
		"voucher_expired_message": 1,
		"second_reply_comment":    1,
		"comment_inbox_message":   1,
		"voucher_template":        1,
		"details":                 1,
		"how_to_use":              1,
		"ended":                   1,
	}
	vchr := new(model.Campaign)
	err := p.Voucher.GetOne(query, selectedField, vchr)
	if err != nil {
		logger.Error(err, "Mongo Error")
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":2301,"error":"Campaign not found","success":false}`)
		return
	}

	// er, err := checkEditRequest(ctx.PostBody())
	updatebson, err := checkEditRequestAndGiveBSON(ctx.PostBody(), vchr, p.FB)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":2302,"error":"` + err.Error() + `","success":false}`)
		return
	}
	vRedis := new(voucher.VoucherInfo)
	err = vRedis.Delete(vchr.PostID, p.Redis)
	if err != nil {
		logger.Error(err)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":2303,"error":"Campaign update failed","success":false}`)
		return
	}
	logger.Info("UPD: ", updatebson)
	timestamp := time.Now().Unix()
	updatebson["edited_on"] = timestamp
	update := bson.M{
		"$set": updatebson,
	}
	selectedField = bson.M{
		"voucher_total": 1,
		"voucher_count": 1,
		"page_id":       1,
		"ended":         1,
	}
	err = p.Voucher.AtomicUpdate(query, update, selectedField, vchr)
	if err != nil {
		logger.Error(err)
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBodyString(`{"code":2304,"error":"Campign update failed","success":false}`)
		return
	}
	if !vchr.Ended {
		ctx.SetStatusCode(fasthttp.StatusOK)
		ctx.SetBodyString(`{"code":2305,"message":"Campign updated","success":true}`)
		return
	}

	remainingquota := vchr.VoucherTotal - vchr.VoucherCount
	query = bson.M{
		"page_id": vchr.PageID,
	}
	update = bson.M{
		"$dec": bson.M{
			"subscription_voucher_count": remainingquota,
		},
		"$set": bson.M{
			"edited_on": timestamp,
		},
	}
	result2 := new(model.PageConfig)
	err = p.Voucher.AtomicUpdate(query, update, bson.M{}, result2)
	if err != nil {
		logger.Error(err)
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBodyString(`{"code":2306,"error":"internal server error","success":false}`)
		return
	}
	ctx.SetBodyString(`{"code":2307,"message":"Campaign Stopped successfully","success":true,}`)
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func checkEditRequestAndGiveBSON(data []byte, vchr *model.Campaign, fb fbapis.ExternalAPI) (b bson.M, err error) {
	er := new(editRequest)
	err = json.Unmarshal(data, er)
	if err != nil {
		logger.Error(err)
		err = fmt.Errorf("params not found")
		return nil, err
	}
	b = bson.M{}
	// if er.Content != nil {
	// 	// update post
	// }
	// if er.Photos != nil {

	// }
	if er.Ended != nil {
		if *er.Ended {
			b["ended"] = *er.Ended
		}
		return b, err
	}

	if er.Title != nil {
		b["title"] = *er.Title
	}
	if er.Message != nil && *er.Message != "" {
		b["message"] = *er.Message
	}
	if er.Language != nil {
		b["language"] = *er.Language
	}
	if er.InboxMessage != nil && *er.InboxMessage != "" {
		b["inbox_message"] = *er.InboxMessage
	}
	if er.ReplyCommentMessage != nil && *er.ReplyCommentMessage != "" {
		b["reply_comment_message"] = *er.ReplyCommentMessage
	}
	if er.VoucherExpiredMessage != nil && *er.VoucherExpiredMessage != "" {
		b["voucher_expired_message"] = *er.VoucherExpiredMessage
	}
	if er.SecondReplyComment != nil && *er.SecondReplyComment != "" {
		b["second_reply_comment"] = *er.SecondReplyComment
	}
	if er.CommentInboxMessage != nil && *er.CommentInboxMessage != "" {
		b["comment_inbox_message"] = *er.CommentInboxMessage
	}
	if er.Details != nil && *er.Details != "" {
		b["details"] = *er.Details
	}
	if er.HowToUse != nil && *er.HowToUse != "" {
		b["how_to_use"] = *er.HowToUse
	}
	if er.EndTime != nil {
		if *er.EndTime < time.Now().Unix() || *er.EndTime < time.Unix(vchr.StartTime, 0).Add(30*time.Minute).Unix() {
			err = fmt.Errorf("invalid end_time")
			return nil, err
		}
		b["end_time"] = *er.EndTime
	}
	if er.VoucherTotal != nil {
		logger.Info(*er.VoucherTotal, " ", vchr.VoucherCount+100, " ", vchr.VoucherTotal)
		if *er.VoucherTotal < (vchr.VoucherCount+100) || *er.VoucherTotal > vchr.VoucherTotal {
			err = fmt.Errorf("invalid voucher_total")
			return nil, err
		}
		b["voucher_total"] = *er.VoucherTotal
	}
	if er.VoucherSellPrice != nil {
		if vchr.VoucherCount == 0 || *er.VoucherSellPrice == 0 {
			err = fmt.Errorf("invalid voucher_sell_price")
			return nil, err
		}

		b["voucher_sell_price"] = *er.VoucherSellPrice
	}
	if er.DistributionStartOn != nil {
		if *er.DistributionStartOn < time.Now().Unix() {
			err = fmt.Errorf("invalid distribution_start_on")
			return nil, err
		}
		b["distribution_start_on"] = *er.DistributionStartOn
	}
	if er.VoucherStartOn != nil {
		if *er.VoucherStartOn < time.Now().Unix() {
			err = fmt.Errorf("invalid voucher_start_on")
			return nil, err
		}
		b["voucher_start_on"] = *er.VoucherStartOn
	}
	if er.VoucherExpiredOn != nil {
		if *er.VoucherExpiredOn < time.Now().Unix() {
			err = fmt.Errorf("invalid voucher_expired_on")
			return nil, err
		}
		b["voucher_expired_on"] = *er.VoucherExpiredOn

	}
	if er.Survey != nil {
		survey := *er.Survey
		if len(survey) > 0 {
			for i := range survey {
				if survey[i].Type == "quick_reply" {
					if len(survey[i].Options) == 0 {
						return nil, fmt.Errorf("option missing")
					}
					if len(survey[i].Message) == 0 {
						return nil, fmt.Errorf("message missing")
					}
					if survey[i].FieldName != "" && survey[i].Validation == "" {
						return nil, fmt.Errorf("validation missing")
					}
				} else if survey[i].Type == "question" {
					if len(survey[i].Message) == 0 {
						return nil, fmt.Errorf("message missing")
					}
					if survey[i].FieldName != "" && survey[i].Validation == "" {
						return nil, fmt.Errorf("validation missing")
					}
				} else if survey[i].Type == "message" {
					if len(survey[i].Message) == 0 {
						return nil, fmt.Errorf("message missing")
					}
				} else if survey[i].Type == "release_voucher" {
					if len(survey) != (i + 1) {
						return nil, fmt.Errorf("release_voucher should be last step")
					}
				} else {
					err = fmt.Errorf("Invalid type in survey")
					return nil, err
				}
			}
			b["survey"] = survey
		}
	}

	if er.VoucherTemplate != nil {
		voucherTemplate := *er.VoucherTemplate
		if voucherTemplate.Type == "carousal" {
			if voucherTemplate.Title == "" {
				return nil, fmt.Errorf("%v is missing", "carousal title ")
			}
			if voucherTemplate.Subtitle == "" {
				return nil, fmt.Errorf("%v is missing", "carousal subtitle")
			}
			if len(voucherTemplate.ImageURL) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal image_url")
			}
			if len(voucherTemplate.Buttons) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal buttons")
			}
			for i := range voucherTemplate.Buttons {
				if voucherTemplate.Buttons[i].Title == "" {
					return nil, fmt.Errorf("%v is missing", "carousal button title")
				}
				if voucherTemplate.Buttons[i].Url == "" {
					return nil, fmt.Errorf("%v is missing", "carousal button url")
				}
			}
			if voucherTemplate.InboxMessage == "" && (er.InboxMessage == nil || *er.InboxMessage == "") {
				return nil, fmt.Errorf("%v is missing", "inbox_message")
			}
		} else if voucherTemplate.Type == "image_button" {
			if voucherTemplate.InboxMessage == "" && (er.InboxMessage == nil || *er.InboxMessage == "") {
				return nil, fmt.Errorf("%v is missing", "inbox_message")
			}
			if len(voucherTemplate.ImageURL) == 0 {
				return nil, fmt.Errorf("%v is missing", "carousal image_url")
			}
		} else {
			if voucherTemplate.InboxMessage == "" && (er.InboxMessage == nil || *er.InboxMessage == "") {
				return nil, fmt.Errorf("%v is missing", "inbox_message")
			}
			voucherTemplate = model.VoucherTemplate{
				Type:         "text",
				InboxMessage: voucherTemplate.InboxMessage,
			}
		}
		b["voucher_template"] = voucherTemplate
	}
	if er.VoucherTriggers != nil {
		vt := *er.VoucherTriggers
		if len(vt) > 0 {
			b["voucher_triggers"] = vt
		}
	}

	if len(b) == 0 {
		logger.Info("empty Object")
		err = fmt.Errorf("params not found")
	}
	return b, err
}

// func checkEditRequest(data []byte) (*editRequest, error) {
// 	logger.Info(string(data))
// 	er := new(editRequest)
// 	err := json.Unmarshal(data, er)
// 	if err != nil {
// 		logger.Error(err)
// 		err = fmt.Errorf("params not found")
// 		return nil, err
// 	}

// 	timestamp := time.Now().Add(1 * time.Minute).Unix()
// 	if er.EndTime < timestamp {
// 		err = fmt.Errorf("invalid end_time")
// 		return nil, err
// 	}
// 	if er.DistributionStartOn < timestamp {
// 		err = fmt.Errorf("invalid distribution_start_on")
// 		return nil, err
// 	}
// 	if er.VoucherStartOn < timestamp {
// 		err = fmt.Errorf("invalid voucher_start_on")
// 		return nil, err
// 	}
// 	if er.VoucherExpiredOn < timestamp {
// 		err = fmt.Errorf("invalid voucher_expired_on")
// 		return nil, err
// 	}
// 	if len(er.Keys) == 0 {
// 		err = fmt.Errorf("keys not found")
// 		return nil, err
// 	}
// 	return er, err
// }
