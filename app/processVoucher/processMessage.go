package processVoucher

import (
	"botio-voucher/app/config"
	"botio-voucher/app/logger"
	"botio-voucher/app/voucher"
	"fmt"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

func (p *ProcessVoucher) ProcessMessage(b *bidRequest) (string, int) { // 0: error 1: handled 2: retry
	defer func() {
		if err := recover(); err != nil {
			logger.Error(fmt.Errorf("%s %+v", err, "voucher handler crashed"))
		}
	}()
	logger.Info(b.Message)
	u := new(voucher.UserState)
	err := u.Get(b.BidderID, p.Redis)
	if err != nil {
		logger.Error(err)
		return "redis get 'userstate' failed:25", 2
	}
	if u.UserID == "" {
		return "bot 'userstate' doesnot exist:28", 1
	}
	logger.Info(b.PageID, u)
	if postID, ok := u.PostsInfo[b.PageID]; ok {
		uv := new(voucher.UserVoucher)
		err = uv.Get(b.BidderID+"_"+postID, p.Redis)
		logger.Info("1", err, uv)
		if err != nil {
			logger.Error(err)
			return "redis get 'uservoucher' failed:37", 2
		}
		logger.Info("1")
		if uv.PostID == "" {
			return "bot 'uservoucher' doesnot exist:41", 1
		}

		if uv.State > len(uv.Survey) {
			delete(u.PostsInfo, b.PageID)
			err = u.Set(b.BidderID, p.Redis)
			return "already completed:47", 1
		}

		if uv.State == -1 || !uv.Recieve || (uv.State+1) > len(uv.Survey) {
			return "not in recieving mode:51", 1
		}
		logger.Info(uv)
		if uv.ExpireReservation < time.Now().Unix() {
			res := p.AuthService.GetTokenFromAuthService(b.PageID)
			if pageAccessToken, ok := res.Response["access_token"].(string); ok {
				logger.Info(config.VoucherResponse[uv.Language]["10"], uv.Language)
				err = uv.Delete(b.BidderID+"_"+postID, p.Redis)
				logger.Info(err)
				if err == nil {
					body := map[string]interface{}{
						"recipient": map[string]interface{}{
							"id": b.BidderID,
						},
						"message": p.FB.Text(config.VoucherResponse[uv.Language]["10"]),
					}
					res = p.FB.SendMessage(pageAccessToken, body)
					if res.Err != nil {
						logger.Info(res.Err)
					}
					return "reservation expired:71", 1
				}
			}
			return "token failure:74", 2
		}

		if uv.LastSentTimestamp > b.Timestamp {
			logger.Info("Extra replies")
			return "multiple replies to one question:79", 1
		}
		res := p.AuthService.GetTokenFromAuthService(b.PageID)
		logger.Info(res)
		pageAccessToken, ok := res.Response["access_token"].(string)
		if !ok {
			logger.Info(fmt.Errorf("Token not available for page: %v", b.PageID))
			return "token failure:86", 2
		}

		// saving answer and monving to next question
		uv.State = uv.State + 1
		uv.Recieve = false
		uv.Send = true
		uv.Answers = append(uv.Answers, b.Message)
		err = uv.Set(b.BidderID+"_"+postID, p.Redis)
		if err != nil {
			logger.Error(err)
			return "redis set 'uservoucher' failed:97", 2
		}

		if uv.Survey[uv.State].Type == "quick_reply" {
			qr := []map[string]interface{}{}
			for k := range uv.Survey[uv.State].Options {
				qr = append(qr, p.FB.QuickReply(uv.Survey[uv.State].Options[k][uv.Language], fmt.Sprintf("%s_%s:%s", b.BidderID, uv.PostID, uv.Survey[uv.State].Options[k][uv.Language])))
			}
			body := map[string]interface{}{
				"recipient": map[string]interface{}{
					"id": b.BidderID,
				},
				"message": p.FB.TextQuickReply(uv.Survey[uv.State].Message[uv.Language], qr),
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			if res.Err != nil {
				logger.Error(fmt.Errorf("%+v", res))
				return "send message failed:114", 2
			}
			uv.Recieve = true
			uv.Send = false
			uv.LastSentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
			err = uv.Set(b.BidderID+"_"+postID, p.Redis)
			if err != nil {
				logger.Error(err)
			}
			return "quick_reply sent:123", 1
		} else if uv.Survey[uv.State].Type == "question" {
			body := map[string]interface{}{
				"recipient": map[string]interface{}{
					"id": b.BidderID,
				},
				"message": p.FB.Text(uv.Survey[uv.State].Message[uv.Language]),
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			if res.Err != nil {
				logger.Error(fmt.Errorf("%+v", res))
				return "send message failed:134", 2
			}
			uv.Recieve = true
			uv.Send = false
			uv.LastSentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
			err = uv.Set(b.BidderID+"_"+postID, p.Redis)
			if err != nil {
				logger.Info(err)
			}
			return "question sent:117", 1
		} else if uv.Survey[uv.State].Type == "message" {
			for state := uv.State; state < len(uv.Survey); state++ {
				if uv.Survey[state].Type == "message" {
					body := map[string]interface{}{
						"recipient": map[string]interface{}{
							"id": b.BidderID,
						},
						"message": p.FB.Text(uv.Survey[state].Message[uv.Language]),
					}
					res = p.FB.SendMessage(pageAccessToken, body)
					if res.Err != nil {
						logger.Error(fmt.Errorf("%+v", res))
						return "send message failed:154", 2
					}
					uv.State = uv.State + 1
				}
				if uv.Survey[state].Type == "question" {
					body := map[string]interface{}{
						"recipient": map[string]interface{}{
							"id": b.BidderID,
						},
						"message": p.FB.Text(uv.Survey[state].Message[uv.Language]),
					}
					res = p.FB.SendMessage(pageAccessToken, body)
					if res.Err != nil {
						logger.Error(fmt.Errorf("%+v", res))
						return "send message failed:154", 2
					}
					logger.Info("let check it")
					break
				}
				if uv.Survey[state].Type == "release_voucher" {
					uv.State = uv.State + 1
					uv.Recieve = false
					uv.Send = true
					uv.Answers = append(uv.Answers, b.Message)
					err = uv.Set(b.BidderID+"_"+postID, p.Redis)
					if err != nil {
						logger.Error(err)
						return "redis set 'uservoucher' failed:172", 2
					}
					qr1 := p.FB.QuickReply(config.VoucherResponse[uv.Language]["9_qr_1"], fmt.Sprintf("%s_%s:%s", b.BidderID, postID, "confirm"))
					qr2 := p.FB.QuickReply(config.VoucherResponse[uv.Language]["9_qr_2"], fmt.Sprintf("%s_%s:%s", b.BidderID, postID, "edit"))
					body := map[string]interface{}{
						"recipient": map[string]interface{}{
							"id": b.BidderID,
						},
						"message": p.FB.TextQuickReply(config.VoucherResponse[uv.Language]["9"], []map[string]interface{}{qr1, qr2}),
					}
					res = p.FB.SendMessage(pageAccessToken, body)
					if res.Err != nil {
						logger.Error(fmt.Errorf("%+v", res))
					}
					uv.Recieve = false
					uv.Send = false
					err = uv.Set(b.BidderID+"_"+postID, p.Redis)
					if err != nil {
						logger.Info(err)
					}
					// send coupon code
					return "release_vouhcer:193", 1
				}
			}
			// body := map[string]interface{}{
			// 	"recipient": map[string]interface{}{
			// 		"id": b.BidderID,
			// 	},
			// 	"message": p.FB.Text(uv.Survey[uv.State].Message[uv.Language]),
			// }
			// res = p.FB.SendMessage(pageAccessToken, body)
			// if res.Err != nil {
			// 	logger.Error(fmt.Errorf("%+v", res))
			// 	return "send message failed:154", 2
			// }
			uv.Recieve = true
			uv.Send = false
			uv.LastSentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
			err = uv.Set(b.BidderID+"_"+postID, p.Redis)
			if err != nil {
				logger.Info(err)
			}
			return "message sent:130", 1
		} else if uv.Survey[uv.State].Type == "release_voucher" {
			uv.State = uv.State + 1
			uv.Recieve = false
			uv.Send = true
			uv.Answers = append(uv.Answers, b.Message)
			err = uv.Set(b.BidderID+"_"+postID, p.Redis)
			if err != nil {
				logger.Error(err)
				return "redis set 'uservoucher' failed:172", 2
			}
			qr1 := p.FB.QuickReply(config.VoucherResponse[uv.Language]["9_qr_1"], fmt.Sprintf("%s_%s:%s", b.BidderID, postID, "confirm"))
			qr2 := p.FB.QuickReply(config.VoucherResponse[uv.Language]["9_qr_2"], fmt.Sprintf("%s_%s:%s", b.BidderID, postID, "edit"))
			body := map[string]interface{}{
				"recipient": map[string]interface{}{
					"id": b.BidderID,
				},
				"message": p.FB.TextQuickReply(config.VoucherResponse[uv.Language]["9"], []map[string]interface{}{qr1, qr2}),
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			if res.Err != nil {
				logger.Error(fmt.Errorf("%+v", res))
			}
			uv.Recieve = false
			uv.Send = false
			err = uv.Set(b.BidderID+"_"+postID, p.Redis)
			if err != nil {
				logger.Info(err)
			}
			// send coupon code
			return "release_vouhcer:193", 1
		}
	}
	return "postInfo doesnot exist:196", 0
}

func (p *ProcessVoucher) ProcessPostback(b *bidRequest) int {
	strs := strings.SplitN(b.Payload, ":", 2)
	uv := new(voucher.UserVoucher)
	err := uv.Get(strs[0], p.Redis)
	if err != nil {
		logger.Info(err)
		return 2
	}
	if uv == nil || uv.InboxMessage == "" {
		return 0
	}
	logger.Info(uv)
	if strings.ToLower(strs[1]) == "confirm" {
		logger.Info("CONFIRM")
		if uv.State > len(uv.Survey) {
			return 1
		}
		uv.State = len(uv.Survey) + 1
		uv.Recieve = false
		uv.Send = false
		err = uv.Set(strs[0], p.Redis)
		if err != nil {
			logger.Info(err)
			return 2
		}

		res := p.AuthService.GetTokenFromAuthService(b.PageID)
		logger.Info(res)
		if pageAccessToken, ok := res.Response["access_token"].(string); ok {
			// send carpusal
			m := map[string]interface{}{}

			if uv.TransactionID != "" {
				logger.Info("paid code")
				buttons := []map[string]interface{}{
					p.FB.WebURLButton("Pay "+fmt.Sprintf("%v", uv.VoucherSellPrice), "https://rocket-voucher.botio.io/"+"payment?id="+uv.PostID+"&uid="+uv.PSID),
				}
				el := []map[string]interface{}{}
				for i := range uv.VoucherTemplate.ImageURL {
					el = append(el, p.FB.GenericElement(
						fmt.Sprintf("%s %v", uv.VoucherTemplate.Title, uv.VoucherSellPrice),
						uv.VoucherTemplate.ImageURL[i],
						"OrderID: "+uv.TransactionID+"/npowered by Rocket Vouhcer",
						"https://facebook.com/"+uv.PostID,
						buttons))
				}
				m = p.FB.GenericTemplate(el, []map[string]interface{}{})

			} else {
				logger.Info("normal")
				switch uv.VoucherTemplate.Type {
				case "image_button":
					{
						buttons := []map[string]interface{}{}
						// buttons = append(buttons, p.FB.WebURLButton(config.VoucherResponse[uv.Language]["11"], config.QrCodeURL+"id="+strs[1]+"&uid="+strs[0]))
						for i := range uv.VoucherTemplate.Buttons {
							buttons = append(buttons, p.FB.WebURLButton(uv.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(uv.VoucherTemplate.Buttons[i].Url, uv.PostID, b.BidderID, b.PageID)))
						}
						m = p.FB.ImageButton(uv.VoucherTemplate.ImageURL, buttons)
					}
				case "carousal":
					{
						buttons := []map[string]interface{}{}
						for i := range uv.VoucherTemplate.Buttons {
							buttons = append(buttons, p.FB.WebURLButton(uv.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(uv.VoucherTemplate.Buttons[i].Url, uv.PostID, b.BidderID, b.PageID)))
						}
						el := []map[string]interface{}{}
						for i := range uv.VoucherTemplate.ImageURL {
							el = append(el, p.FB.GenericElement(
								replaceVoucherResponseValues(uv.VoucherTemplate.Title, uv.CouponCode, "", uv.InboxMessage, "", "", "", "", "", ""),
								uv.VoucherTemplate.ImageURL[i],
								replaceVoucherResponseValues(uv.VoucherTemplate.Subtitle, uv.CouponCode, "", uv.InboxMessage, "", "", "", "", "", ""),
								"https://facebook.com/"+b.PostID,
								buttons))
						}

						m = p.FB.GenericTemplate(el, []map[string]interface{}{})
					}
				default:
					m = p.FB.Text(replaceVoucherResponseValues(config.VoucherResponse[uv.Language]["2"], uv.CouponCode, "", uv.InboxMessage, "", "", "", "", uv.Name, ""))
				}
			}
			body := map[string]interface{}{
				"messaging_type": "MESSAGE_TAG",
				"tag":            "POST_PURCHASE_UPDATE",
				"recipient": map[string]interface{}{
					"id": b.BidderID,
				},
				"message": m,
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			logger.Info(res)
			if res.Err == nil {
				data := strings.Split(strs[0], "_")
				logger.Info(data, " ", strs)
				query := bson.M{
					"post_id": data[1] + "_" + data[2],
				}
				update := bson.M{
					"$set": bson.M{
						"history." + data[0] + ".answers":     uv.Answers,
						"history." + data[0] + ".coupon_code": uv.CouponCode,
						"history." + data[0] + ".error.type":  "sent",
						"history." + data[0] + ".error.info":  fmt.Sprintf("%+v", res),
						"edited_on":                           time.Now().Unix(),
					},
				}
				err = p.Voucher.Update(query, update)
				if err != nil {
					logger.Info(err)
					uv.State = len(uv.Survey)
					uv.Recieve = true
					uv.Send = false
					err = uv.Set(strs[0], p.Redis)
					if err != nil {
						logger.Info(err)
					}
					return 2
				}
				x := new(voucher.UserState)
				err := x.Get(b.BidderID, p.Redis)
				if err != nil || x == nil {
					logger.Info(err)
				}
				delete(x.PostsInfo, b.PageID)
				err = x.Set(data[0], p.Redis)
				if err != nil {
					logger.Info(err)
				}
				return 1
			}
			return 2

		}
		return 2
	}
	if strings.ToLower(strs[1]) == "edit" {
		uv.State = 0
		uv.Answers = []string{}
		uv.Send = true
		uv.Recieve = false
		err = uv.Set(strs[0], p.Redis)
		if err != nil {
			logger.Info(err)
			return 2
		}
		res := p.AuthService.GetTokenFromAuthService(b.PageID)
		logger.Info(res, fmt.Sprintf("Token not available for page: %+v", b))
		if pageAccessToken, ok := res.Response["access_token"].(string); ok {
			body := map[string]interface{}{
				"messaging_type": "MESSAGE_TAG",
				"tag":            "POST_PURCHASE_UPDATE",
				"recipient": map[string]interface{}{
					"id": b.BidderID,
				},
				"message": p.FB.Text(uv.Survey[uv.State].Message[uv.Language]),
			}
			res = p.FB.SendMessage(pageAccessToken, body)
			if res.Err == nil {
				uv.Send = false
				uv.Recieve = true
				uv.LastSentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
				err = uv.Set(strs[0], p.Redis)
				if err != nil {
					logger.Info(err)
				}
				return 1
			}
			return 2
		}
		return 2
	}
	if strings.ToLower(strs[1]) == "consentok" {
		logger.Info("consent OK")
		query := bson.M{
			"page_id": b.PageID,
		}
		update := bson.M{
			"$addToSet": bson.M{
				"subscribers": b.BidderID,
			},
		}
		err = p.PageConfig.Update(query, update)
		if err != nil {
			logger.Error(err)
			return 2
		}
		res := p.AuthService.GetTokenFromAuthService(b.PageID)
		pageAccessToken, ok := res.Response["access_token"].(string)
		if !ok {
			logger.Info(res, fmt.Sprintf("Token not available for page: %+v", b))
			return 2
		}
		m := map[string]interface{}{}
		if len(uv.Survey) > 0 {
			logger.Info("Survey")
			//send survey question
			if uv.State != -1 {
				logger.Info("HERE", uv.State)
				return 1
			}
			uv.State = 0
			uv.Send = true
			uv.Recieve = false
			uv.Answers = []string{}
			err = uv.Set(strs[0], p.Redis)
			if err != nil {
				logger.Info(err)
				return 2
			}

			body := map[string]interface{}{
				"batch": []map[string]interface{}{
					/*	{
						"name":         "req1",
						"method":       "POST",
						"relative_url": "me/messages",
						"body":         "recipient={\"id\": \"" + b.BidderID + "\"}&message={\"attachment\":{\"type\":\"template\",\"payload\":{\"template_type\":\"media\",\"elements\": [{\"media_type\":\"image\",\"url\":\"https://www.facebook.com/photo.php?fbid=" + uv.Photos[0] + "\"}]}}}",
					},*/
					{
						"name": "req2",
						// "depends_on":   "req1",
						"method":       "POST",
						"relative_url": "me/messages",
						"body":         "recipient={\"id\": \"" + b.BidderID + "\"}&message={\"text\": \"" + replaceVoucherResponseValues(config.VoucherResponse[uv.Language]["8"], "", "", "", "", "", "", "", uv.Name, "") + "\"}",
					},
					{
						"name":         "req3",
						"depends_on":   "req2",
						"method":       "POST",
						"relative_url": "me/messages",
						"body":         "recipient={\"id\": \"" + b.BidderID + "\"}&message={\"text\": \"" + uv.Survey[uv.State].Message[uv.Language] + "\"}",
					},
				},
			}

			res = p.FB.BatchRequest(body, pageAccessToken)
			logger.Info(res)
			if res.Err != nil {
				uv.State = -1
				err = uv.Set(strs[0], p.Redis)
				if err != nil {
					logger.Error(err)
				}
				return 2
			}
			uv.Send = false
			uv.Recieve = true
			uv.LastSentTimestamp = time.Now().UnixNano() / int64(time.Millisecond)
			err = uv.Set(strs[0], p.Redis)
			if err != nil {
				logger.Info(err)
			}
			return 1
		} else if uv.TransactionID != "" {
			logger.Info("paid code")
			// send paid coupon code link
			buttons := []map[string]interface{}{
				p.FB.WebURLButton("Pay "+fmt.Sprintf("%v", uv.VoucherSellPrice), "https://rocket-voucher.botio.io/"+"payment?id="+uv.PostID+"&uid="+uv.PSID),
			}
			el := []map[string]interface{}{}
			for i := range uv.VoucherTemplate.ImageURL {
				el = append(el, p.FB.GenericElement(
					fmt.Sprintf("%s %v", uv.VoucherTemplate.Title, uv.VoucherSellPrice),
					uv.VoucherTemplate.ImageURL[i],
					"OrderID: "+uv.TransactionID+"/npowered by Rocket Vouhcer",
					"https://facebook.com/"+uv.PostID,
					buttons))
			}
			m = p.FB.GenericTemplate(el, []map[string]interface{}{})
		} else {
			// send whatever you have
			logger.Info("normal")

			switch uv.VoucherTemplate.Type {
			case "image_button":
				{
					buttons := []map[string]interface{}{}
					// buttons = append(buttons, p.FB.WebURLButton(config.VoucherResponse[vRedis.Language]["11"], config.QrCodeURL+"id="+b.PostID+"&uid="+b.BidderID))
					if uv.VoucherType == "web" {

						buttons = append(buttons, p.FB.WebURLButton("กดแลกรับสิทธิ์", "https://rocket-voucher.botio.io/voucher?id="+uv.PostID+"&uid="+b.BidderID))
					} else {
						for i := range uv.VoucherTemplate.Buttons {
							buttons = append(buttons, p.FB.WebURLButton(uv.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(uv.VoucherTemplate.Buttons[i].Url, uv.PostID, b.BidderID, b.PageID)))
						}
					}
					m = p.FB.ImageButton(uv.VoucherTemplate.ImageURL, buttons)
				}
			case "carousal":
				{
					buttons := []map[string]interface{}{}
					for i := range uv.VoucherTemplate.Buttons {
						buttons = append(buttons, p.FB.WebURLButton(uv.VoucherTemplate.Buttons[i].Title, replaceIDanfUID(uv.VoucherTemplate.Buttons[i].Url, uv.PostID, b.BidderID, b.PageID)))
					}

					el := []map[string]interface{}{}
					for i := range uv.VoucherTemplate.ImageURL {
						el = append(el, p.FB.GenericElement(
							replaceVoucherResponseValues(uv.VoucherTemplate.Title, uv.CouponCode, "", uv.InboxMessage, "", "", "", "", "", ""),
							uv.VoucherTemplate.ImageURL[i],
							replaceVoucherResponseValues(uv.VoucherTemplate.Subtitle, uv.CouponCode, "", uv.InboxMessage, "", "", "", "", "", ""),
							replaceIDanfUID(uv.VoucherTemplate.Buttons[i].Url, uv.PostID, b.BidderID, b.PageID),
							buttons))
					}

					m = p.FB.GenericTemplate(el, []map[string]interface{}{})
				}
			default:
				m = p.FB.Text(replaceVoucherResponseValues(config.VoucherResponse[uv.Language]["2"], uv.CouponCode, "", uv.InboxMessage, "", "", "", "", uv.Name, uv.VoucherTemplate.Title))
			}
		}
		body := map[string]interface{}{
			"messaging_type": "MESSAGE_TAG",
			"tag":            "POST_PURCHASE_UPDATE",
			"recipient": map[string]interface{}{
				"id": uv.PSID,
			},
			"message": m,
		}

		res = p.FB.SendMessage(pageAccessToken, body)
		if res.Err != nil {
		}
		update = bson.M{
			"$set": bson.M{
				"edited_on":                              time.Now().Unix(),
				"history." + b.BidderID + ".coupon_code": uv.CouponCode,
				"history." + b.BidderID + ".error.type":  "sent",
				"history." + b.BidderID + ".error.info":  fmt.Sprintf("%+v", res),
			},
		}
		err = p.Voucher.Update(bson.M{"post_id": uv.PostID}, update)
		if err != nil {
			logger.Error(err)
		}
		return 1
	}
	return 0
}
