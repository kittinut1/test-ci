package logger

import (
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"
)

var (
	DEFAULT_LOGGER = Logger{}
)

const (
	DEBUG LogLevel = 0
	INFO  LogLevel = 1
	ERROR LogLevel = 2
)

type Loggable interface {
	Log(message string, params ...interface{})
}

type Logger struct{}

type LogLevel int

func Debug(message ...interface{}) {
	DEFAULT_LOGGER.Debug(message...)
}

func Info(message ...interface{}) {
	DEFAULT_LOGGER.Info(message...)
}

func Error(err error, message ...interface{}) {
	DEFAULT_LOGGER.Error(err, message...)
}

func (level LogLevel) String() string {
	levels := []string{
		"DEBUG",
		"INFO",
		"ERROR",
	}
	return levels[level]
}

func (l *Logger) Log(message string, params ...interface{}) {
	fmt.Println(message, params)
}

func (l *Logger) Debug(message ...interface{}) {
	printDebug(message...)
}

func (l *Logger) Info(message ...interface{}) {
	printInfo(message...)
}

func (l *Logger) Error(err error, message ...interface{}) {
	printError(err, message...)
}

func defaultLogFormat(logLevel LogLevel, t time.Time) string {
	fn, file, line, _ := runtime.Caller(5)

	time := t.Format("2006-01-02 15:04:05.000")
	path := fmt.Sprintf("%s(%s:%d)", runtime.FuncForPC(fn).Name(), chopPath(file), line)
	level := splitN(logLevel.String(), 5)
	level = fmt.Sprintf("[%s]", fmt.Sprintf("%-5v", strings.ToUpper(level)))
	return fmt.Sprintf("%s %s %s", time, level, path)
}

func chopPath(str string) string {
	i := strings.LastIndex(str, "/")
	if i == -1 {
		return str
	}
	return str[i+1:]
}

func splitN(str string, n int) string {
	if len(str) > n {
		return str[:n]
	}
	return str
}

func printDebug(message ...interface{}) {
	defer func(t time.Time) {
		if os.Getenv("APP_ENV") == "development" {
			fmt.Println(defaultLogFormat(DEBUG, t), ":", fmt.Sprint(message...))
		}
	}(time.Now())
}

func printInfo(message ...interface{}) {
	defer func(t time.Time) {
		fmt.Println(defaultLogFormat(INFO, t), ":", fmt.Sprint(message...))
	}(time.Now())
}

func printError(err error, message ...interface{}) {
	defer func(t time.Time) {
		if len(message) > 0 {
			fmt.Println(defaultLogFormat(ERROR, t), ":", err)
		} else {
			fmt.Println(defaultLogFormat(ERROR, t), ":", err, ":", fmt.Sprint(message...))
		}
	}(time.Now())
}
