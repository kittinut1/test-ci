package authService

import (
	"botio-voucher/app/config"
	"botio-voucher/app/network"
)

type AuthService struct {
	client network.Client
}

func New(c network.Client) *AuthService {
	return &AuthService{
		client: c,
	}
}

func (a *AuthService) GetTokenFromAuthService(id string) network.Response {
	if config.AppEnvironment == "test" {
		res := network.Response{Response: map[string]interface{}{"id": id}, Err: nil}
		res.Response["access_token"] = "nsdjkxnewiudbcejhdbsfcjsdbxcsaewsahjmsdxjc"
		return res
	}
	uri := config.AuthService + "tokens?ids=" + id
	res := a.client.Get(uri)
	return res
}

func (a *AuthService) CheckCompletedBidCron() network.Response {
	uri := "http://localhost:8084/ujung/posts/status"
	res := a.client.Get(uri)
	return res
}
