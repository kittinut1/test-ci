FROM golang:1.13.4 as stage1

RUN set -eux; \
    apt-get update -qqy; \
    apt-get install -y --no-install-recommends \
        libsasl2-dev \
        libsasl2-modules \
        libssl-dev \  
        upx \
        git; \
   rm -rf /var/cache/apk/*; \
    rm -rf /var/lib/apt/lists/*;
RUN git clone https://github.com/edenhill/librdkafka.git; \
    cd librdkafka; \
    ./configure --prefix /usr; \
    make; \
    make install; 
ENV VOUCHER_PATH /go/src/botio-voucher

RUN  mkdir -p ${VOUCHER_PATH}/app

WORKDIR ${VOUCHER_PATH}/app

COPY ./go.mod ./go.sum ${VOUCHER_PATH}/

RUN go mod download

COPY app ${VOUCHER_PATH}/app

# RUN GOOS=linux go build -tags static -a -ldflags="-s -w" -o /go/bin/botio-voucher
RUN go build -o /go/bin/botio-voucher
RUN ldd /go/bin/botio-voucher
FROM ubuntu:19.04

RUN set -eux; \
    apt update -qqy; \
    apt install -y --no-install-recommends \
	ca-certificates \
        libsasl2-dev \
        libsasl2-modules \
        libssl-dev; \
        apt autoremove
COPY --from=stage1 /usr/lib/pkgconfig /usr/lib/pkgconfig
COPY --from=stage1 /usr/lib/librdkafka* /usr/lib/
COPY --from=stage1 /go/bin/botio-voucher /home/
WORKDIR /home
EXPOSE 8084
ENV APP_ENV=prod PORT=8084 KAFKA_ADDRESS= KAFKA_APIKEY= KAFKA_APISECRET= SERVER_URL= REDIS_HOST=redis-master REDIS_PORT=6379 REDIS_PASSWORD= MONGO_HOST=mongo-voucher MONGO_PORT=27017 MONGO_USERNAME= MONGO_PASSWORD= MONGODB_URL= ISSUER=botioPlatform JWT_SECRET= MONGO_INITDB_ROOT_USERNAME= MONGO_INITDB_ROOT_PASSWORD=
CMD ["./botio-voucher"]
