module botio-voucher

go 1.13

require (
	github.com/AubSs/fasthttplogger v0.0.0-20170531123222-7dab642e7dab
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/confluentinc/confluent-kafka-go v1.3.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/disintegration/imaging v1.6.2
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/google/pprof v0.0.0-20191218002539-d4f498aebedc
	github.com/google/uuid v1.1.1
	github.com/klauspost/compress v1.9.5
	github.com/pkg/profile v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasthttp v0.0.0-20171207120941-e5f51c11919d
	go.mongodb.org/mongo-driver v1.3.0
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	gopkg.in/alexcesaro/statsd.v2 v2.0.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.1.0
)
