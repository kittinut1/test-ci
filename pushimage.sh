
# if [[ $# -eq 0 ]] ; then
#     echo 'You must provide commit message'
#     echo
#     exit 1
# fi

set -eux; \
docker build -f build/app/DockerfileProd -t voucher:latest .; 



# set -eux; \
# git add .; \
# git commit -m "'"${1}"'"; \
# git push origin staging; 

shortcommit=$(git rev-parse --short HEAD)
timestamp=$(date +%s)

set -eux;\
docker tag voucher:latest asia.gcr.io/botio-website/voucher:$shortcommit$timestamp; \
docker push asia.gcr.io/botio-website/voucher:$shortcommit$timestamp; 
# docker tag voucher:latest asia.gcr.io/botio-website/voucher:latest; \
# docker push asia.gcr.io/botio-website/voucher:latest; 


# set -eux;\
# sed -e "s/\${commit}/${shortcommit}/" -e  "s/\${timestamp}/${timestamp}/" ./build/app/kube-deployment.yaml;\
# kubectl apply -f build/app/kube-deployment.yaml
set -eux;\
kubectl set image deployment/voucher voucher=asia.gcr.io/botio-website/voucher:$shortcommit$timestamp --record;\
kubectl rollout status deployment.v1.apps/voucher
